<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockController
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class StockController extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        //$this->load->library('form_validation');
        //$this->load->helper("security");
        $this->load->library('session');
        $this->load->model('StockModel');
    }

    // SNT 14th Feb 2020 to get counterstock  with json respone
    function FetchCounterStock() {

        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $ApplianceName = $this->input->post('ApplianceName');
            $Username = $this->input->post('Username'); // counter id 
            if ($Username == '') {
                $Username = $this->session->userdata('Username'); //counterid
            }
            $stockType = $this->input->post('stockType');
            //$ApplianceName = "xyz";
            //$Username = "ctr0001";
            $result = $this->StockModel->FetchCounterStock($ApplianceName, $Username, $stockType);
            //var_dump($result);

            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // SNT 27th Feb 2020 to get counterstock  with json respone
    function FetchWareHouseStock() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $ApplianceName = $this->input->post('ApplianceName');
            $WarehouseName = $this->input->post('Username'); // counter id 
            $stockType = $this->input->post('stockType');
            $result = $this->StockModel->FetchWareHouseStock($WarehouseName, $ApplianceName, $stockType);

            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // SNT 14th Feb 2020 to save records of stockdistribtion
    function SaveCounterMISEntry() {
        
        if ($this->session->userdata('Username') != "") {
        try {

            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username');
            $ApplianceName = $this->input->post('ApplianceName');
            $QtyDistributed = $this->input->post('QtyDistributed');
            if ($QtyDistributed == null || $QtyDistributed == "") {
                $QtyDistributed = 0;
            }
            $QtyReplaced = $this->input->post('QtyReplaced');
            if ($QtyReplaced == null || $QtyReplaced == "") {
                $QtyReplaced = 0;
            }

            $QtyBD = $this->input->post('QtyBD');
            if ($QtyBD == null || $QtyBD == "") {
                $QtyBD = 0;
            }

            if ($this->input->post('CashReceived') == "") {
                $CashReceived = 0;
            } else {
                $CashReceived = $this->input->post('CashReceived');
            }
            $Comments = $this->input->post('Comments');
            $TrnDate = $this->input->post('TrnDate');
            $createdBy = $this->session->userdata('Username');
           
            $result = $this->StockModel->SaveCounterMISEntry($Username, $ApplianceName, $QtyDistributed, $QtyReplaced, $QtyBD, $Comments, $CashReceived, $createdBy,$TrnDate);
            // print_r($result);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            //echo"hii";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
   
    } else {
            redirect('/');
        }
    }

    // SNT 14th Feb 2020 to display records of stockdistribtion 
    function FetchCounterMISList() {
        
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            //$Username = $this->session->userdata('Username'); //counterid
            $Username = $this->input->post('Username'); 
            $ApplianceName = $this->input->post('ApplianceName'); 
            $From = $this->input->post('From'); 
             $To = $this->input->post('To'); 
             
            $result = $this->StockModel->FetchCounterMISList($Username, $ApplianceName, $From, $To);

            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // SNT 15th Feb 2020 to save stock transfer
    function SaveStockTransfer() {
        
        if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $Username_From = $this->input->post('Username'); //counterid
                //$Username = $this->input->post('Username'); // counter id 
                $StockType = $this->input->post('StockType');
                $TrnDate = $this->input->post('TrnDate');
                $TrnType = $this->input->post('TrnType');
                $BrandName = $this->input->post('Name_');
                $ApplianceName = $this->input->post('ApplianceName');
                $TrnQty = $this->input->post('Qty');
                $RecipientPartyType = $this->input->post('RecipientPartyType'); //RecipientPartyType from common 
                $Username_To = $this->input->post('Username_To'); //From user List
                $Comments = $this->input->post('Comments');
                $ModifiedBy = $Username_From;
                $result = $this->StockModel->SaveStockTransfer($TrnType, $ApplianceName, $StockType, $BrandName, $TrnDate, $TrnQty, $Username_From, $Username_To, $Comments, $ModifiedBy);

                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }

    // SNT 15th Feb 2020 List to display stock transfer
    function FetchStockTransferList() {
        try {

            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username'); // counter id 
            $State = $this->input->post('State', TRUE);
            $District = $this->input->post('District', TRUE);
            $WarehouseName = $this->input->post('Wname', TRUE);
            $Status = $this->input->post('Status', TRUE);
            $Appliance = $this->input->post('Appliance', TRUE);
            $StockType = $this->input->post('StockType', TRUE);
            $Brand = $this->input->post('Brand', TRUE);
            $From = $this->input->post('From'); 
            $To = $this->input->post('To'); 
            $result = $this->StockModel->FetchStockTransferList($Username, $State, $District, $WarehouseName, $Appliance, $StockType, $Brand, $Status, $From, $To);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
            
             
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    //SNT 17th Feb 2020 Function use to get pending List of stock Approval for specific user
    function stockApprovedRejectList() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username');
            $Status = $this->input->post('Status');
            $RoleType = $this->input->post('RoleType');
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $result = $this->StockModel->stockApprovedRejectList($Username, $Status, $RoleType, $State, $District);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    //SNT 17th Feb 2020 Function use to approve/rej stock transfer
    function ApproveRejectStockTransfer() {
        
        if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $Id = $this->input->post('Id');
                $Status = $this->input->post('Status');
                $Username_Approver = $this->session->userdata('Username');
                $ActionTakenOn = date('Y-m-d H:i:s');
                $ApproverRemarks = $this->input->post('ApproverRemarks');
                $ModifiedBy = $this->session->userdata('Username');
                $Username_From = $this->input->post('Username_From');
                $TrnType = $this->input->post('TrnType');
                $Username_To = $this->input->post('Username_To');
                $TrnQty = $this->input->post('TrnQty');
                $ApplianceName = $this->input->post('ApplianceName');
                $StockType = $this->input->post('StockType');

                $result = $this->StockModel->ApproveRejectStockTransfer($Id, $Status, $Username_Approver, $ActionTakenOn, $ApproverRemarks, $ModifiedBy, $Username_From, $TrnType, $Username_To, $TrnQty, $ApplianceName, $StockType);
                //echo $result["status"];
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }
    
    //SNT 03-02-2020 Load  page for mis-entry
    public function LoadStockDistributionView() {
        
         if ($this->session->userdata('Username') != "") {
        $data['Title'] = "MIS Entry";
        $data['Heading'] = AppName;
        $this->load->view('Layouts/_LayoutMainHeader.php', $data);
        $this->load->view('Distribution.php');
        $this->load->view('Layouts/_LayoutMainFooter.php');
         }else {
            redirect('/');
        }
    }
    
    //SNT 05-02-2020 Load  page for stock transfer
    public function LoadStockTransfer() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Stock Transfer";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Stock/StockTransfer.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }

    //KCJ 05 Macrh 2020 Load Approval page for stock transfer
    public function LoadStockTransferApproval() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Approvals";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Stock/StockApproval.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
    //SNT 23 Macrh 2020 Load Approval page for stock transfer
    public function LoadCashTransferApproval() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "CashApproval";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Cash/CashApproval.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }

}
