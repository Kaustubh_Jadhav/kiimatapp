<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CashController
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class CashController extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        //$this->load->helper('form');
        //$this->load->library('form_validation');
        //$this->load->helper("security");
        $this->load->library('session');
        $this->load->model('CashModel');
        $this->load->helper("url");
    }

// SNT 14th Feb 2020 Function use to to get cash with json response
    function FetchCounterCash() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username'); // counter id 
            //$Username = $this->input->post('Username'); // counter id 
            $result = $this->CashModel->FetchCounterCash($Username);
            //var_dump($result);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

// SNT 14th Feb 2020 Function use to to save cash transfer
    function SaveCashTransfer() {
        if ($this->session->userdata('Username') != "") {
         try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $TrnDate = $this->input->post('TrnDate');
                $Username_From = $this->input->post('Username');
                $RecipientAccountType = $this->input->post('RecipientAccountType'); //RecipientAccountType from constant
                $Username_To = $this->input->post('Username_To'); //from user list
                $TrnAmount = $this->input->post('TrnAmount');
                $Comments = $this->input->post('Comments');
                $CreatedBy = $this->session->userdata('Username');
                $result = $this->CashModel->SaveCashTransfer($Username_From, $Username_To, $TrnDate, $TrnAmount, $RecipientAccountType, $Comments, $CreatedBy);

                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }

// SNT 14th Feb 2020 Function use to to display cash transfer 
    function FetchCashTransferList() {

        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username'); // counter id 
            //$Username = $this->session->userdata('Username'); //counterid
            $State = $this->input->post('State', TRUE);
            $District = $this->input->post('District', TRUE);
            $RecipientType = $this->input->post('RecipientType', TRUE);
            $Status = $this->input->post('Status', TRUE);
            $TransferTo = $this->input->post('TransferTo', TRUE);
            $From = $this->input->post('From'); 
            $To = $this->input->post('To'); 
            $result = $this->CashModel->FetchCashTransferList($Username, $State, $District, $RecipientType, $Status, $TransferTo, $From, $To);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
           //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

//KCJ 20th Feb 2020 Use to load expense view with details
    public function LoadExpenseView() {

        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Expenses";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Cash/Expense.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }

//  // SNT 12th Feb 2020 Function use to get expense for counter user
    function FetchExpenseList() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $State = $this->input->post('State', TRUE);
            $District = $this->input->post('District', TRUE);
            $User = $this->input->post('User', TRUE);
            $Status = $this->input->post('Status', TRUE);
            $ExpType = $this->input->post('ExpType', TRUE);
            $From = $this->input->post('From'); 
            $To = $this->input->post('To'); 
            $result = $this->CashModel->FetchExpenseList( $State, $District, $User, $Status, $ExpType, $From, $To );
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => failmessage, 'data' => ''))));
        }
    }

    // SNT 12th Feb 2020 Function use to store expense for counter user
    function SaveExpenseDetails() {
         if ($this->session->userdata('Username') != "") {
             try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $TrnDate = $this->input->post('TrnDate', TRUE);
                $ExpenseType = $this->input->post('ExpenseType', TRUE);
                $AmountSpent = $this->input->post('AmountSpent', TRUE);
                $Comments = $this->input->post('Comments', TRUE);
                $Username = $this->session->userdata('Username');
                $CreatedBy = $this->session->userdata('Username');
              
                    $result = $this->CashModel->SaveExpenseEntry($Username, $TrnDate, $ExpenseType, $AmountSpent, $Comments, $CreatedBy);
                    if ($result) {
                        $this->output->set_content_type('application/json');
                        $this->output->set_output(json_encode(array($result)));
                    } else {
                        throw new Exception(MsgException);
                    }
            
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => failmessage, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
       
    }

    // SNT 17th Feb 2020 Function use to get pending List of cash Approval for specific user
    function FetchCashApproveRejectList() {
        try {
            
            $_POST = json_decode(file_get_contents('php://input'), true);
            $RoleType = $this->input->post('RoleType');
            $Username = $this->input->post('Username'); //counterid
            $Status = $this->input->post('Status');
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $result = $this->CashModel->FetchCashApproveRejectList($RoleType, $Username, $Status, $State, $District);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // SNT 17th Feb 2020 Function use to approve/rej cash transfer
    function ApproveRejectCashTransfer() {
        if ($this->session->userdata('Username') != "") {
            date_default_timezone_set('Asia/Kolkata');
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Id = $this->input->post('Id');
            $Username_From = $this->input->post('Username_From');
            $Username_To = $this->input->post('Username_To');
            $TrnAmount = $this->input->post('TrnAmount');
            $Status = $this->input->post('Status');
            $CreatedBy = $this->session->userdata('Username');
            $ApproverRemarks = $this->input->post('ApproverRemarks');
            /* $Id = "75";
              $Username_From ="ctr0001";
              $Username_To = 'cxb';
              $TrnAmount = "100";
              $Status =  'APPROVED';
              $CreatedBy = "cxb"; */
            try {
                $data = array(
                    "Status" => $this->input->post('Status'),
                    "Username_Approver" => $this->session->userdata('Username'),
                    "ActionTakenOn" => date('Y-m-d H:i:s'),
                    "ApproverRemarks" => $ApproverRemarks,
                    "ModifiedOn" => date('Y-m-d H:i:s'),
                    "ModifiedBy" => $this->session->userdata('Username')

                        /* "Status" => 'APPROVED',
                          "Username_Approver" => 'cxb',
                          "ActionTakenOn" => date('Y-m-d H:i:s'),
                          "ApproverRemarks" => 'test',
                          "ModifiedOn" => date('Y-m-d H:i:s'),
                          "ModifiedBy" => 'cxb' */
                );

                $result = $this->CashModel->ApproveRejectCashTransfer($data, $Id, $Username_From, $Username_To, $TrnAmount, $Status, $CreatedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
        
    }

    // SNT 20th Feb 2020 Function use to load cash transfer view
    public function LoadCashTransferView() {

        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Cash Transfer";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Cash/Transfer.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }

    // SNT 17th Feb 2020 Function use to approve/rej cash transfer
    function FetchCashForUser() {
        try {
            $Username = $this->input->post('Username');
            if ($Username == "" || $Username == null) {
                $Username = $this->session->userdata('Username');
            }
            $result = $this->CashModel->FetchCashForUser($Username);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    //SNT 17th Feb 2020 Function use to get pending List of expense Approval for specific user
     function FetchExpenseApproveRejectList() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Username = $this->input->post('Username');
            $Status = $this->input->post('Status');
            $RoleType = $this->input->post('RoleType');
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $result = $this->CashModel->FetchExpenseApproveRejectList($RoleType, $Username, $Status, $State, $District);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
     //SNT 17th Feb 2020 Function use to get  expense Approval for specific user
     function ApproveRejectExpense() {
        if ($this->session->userdata('Username') != "") {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $Id = $this->input->post('Id');
            $Username = $this->input->post('Username'); // person who submitted the expense entry
            //$CreatedBy = $this->session->userdata('Username');
            $AmountSpent = $this->input->post('AmountSpent');
            //$Status = $this->input->post('Status');
            try {
                $data = array(
                    "Status" => $this->input->post('Status'),
                    "Username_Approver" => $this->session->userdata('Username'),
                    "ActionTakenOn" => date('Y-m-d H:i:s'),
                    "ApproverRemarks" => $this->input->post('ApproverRemarks'),
                    "ModifiedOn" => date('Y-m-d H:i:s'),
                    "ModifiedBy" => $this->session->userdata('Username')
                );

                // $result = $this->CashModel->ApproveRejectExpense($data, $Id, $username, $AmountSpent, $CreatedBy, $Data[0]->Status);
                $result = $this->CashModel->ApproveRejectExpense($data, $Id, $Username, $AmountSpent);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }
}
