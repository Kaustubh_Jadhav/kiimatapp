<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommonController
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        //$this->load->library('form_validation');
        //$this->load->helper("security");
        $this->load->library('session');
        $this->load->model('CommonModel');
    }

    // SNT 14th Feb 2020 Function use to to get applianceList with json response
    function FetchApplianceList() {
        try {
            $result = $this->CommonModel->FetchApplianceList();
            //var_dump($result);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // SNT 14th Feb 2020 Function use to to get list of users
//    function FetchUserList() {
//        try {
//            $_POST = json_decode(file_get_contents('php://input'), true);
//            $Username = $this->input->post('Username');
//            $RoleType = $this->input->post('RoleType');
//            $result = $this->CommonModel->FetchUserList($RoleType, $Username);
//            if ($result) {
//                $this->output->set_content_type('application/json');
//                $this->output->set_output(json_encode(array($result)));
//            } else {
//                throw new Exception(MsgException);
//            }
//        } catch (Exception $e) {
//            $this->output->set_content_type('application/json');
//            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
//        }
//    }
    

    // SNT 14th Feb 2020 Function use to to get list of brands
    function FetchBrandList() {

        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $ApplianceName = $this->input->post('ApplianceName');
            $result = $this->CommonModel->FetchBrandList($ApplianceName);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // KCJ 02th March 2020 Function use to to get State List 
    function FetchStateList() {

        try {
            $result = $this->CommonModel->FetchStateList();
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

    // KCJ 02th March 2020 Function use to to get District List 
    function FetchDistrictList() {

        try {
            $result = $this->CommonModel->FetchDistrictList();
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    
    // SNT 05-03-2020 Function use to add/modify appliances
    function SaveApplianceDetails(){
        if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $Id = $this->input->post('Id');
                $Name_ = $this->input->post('Name_');
                $RatePerPc = $this->input->post('RatePerPc');
                $ModifiedBy = $this->session->userdata('Username');

                /* $Id = "";
                  $Name_ = "LEDBULB";
                  $RatePerPc = "20";
                  $ModifiedBy = "null"; */
                $result = $this->CommonModel->SaveApplianceDetails($Id, $Name_, $RatePerPc, $ModifiedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }
    
    
     // SNT 06-03-2020 Function use to Fetch Warehouse List
    function FetchWarehouseList() {

        try {
            
            $_POST = json_decode(file_get_contents('php://input'), true);
            $State = $this->input->post('State', TRUE);
            $District = $this->input->post('District', TRUE);
            $result = $this->CommonModel->FetchWarehouseList($State, $District);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    // SNT 06-03-2020 Function use to add/modify warehouse
     function SaveWarehouseDetails(){
        if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $Id = $this->input->post('Id');
                $DistrictName = $this->input->post('DistrictName');
                $Name_ = $this->input->post('Name_');
                $Description = $this->input->post('Description');
                $ModifiedBy = $this->session->userdata('Username');
                $result = $this->CommonModel->SaveWarehouseDetails($Id, strtoupper($DistrictName), $Name_, $Description, $ModifiedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }
    
    
     // SNT 06-03-2020 Function use to add/modify Brand
     function UpdateBrandDetails(){
        if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $Id = $this->input->post('Id');
                $ApplianceName = $this->input->post('ApplianceName');
                $Name_ = $this->input->post('Name_');
                $ModifiedBy = $this->session->userdata('Username');
                $result = $this->CommonModel->UpdateBrandDetails($Id, $ApplianceName, $Name_, $ModifiedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
            }
        } else {
            redirect('/');
        }
    }
    
    //  SNT 12-03-2020 function to load Appliance view
      public function ApplianceView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Appliances";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Users/Appliance.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
    //  SNT 12-03-2020 function to fetch whouse data
    function fetchwhdata() {

        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $refname = $this->input->post('name');
            $result = $this->CommonModel->fetchwhdata($refname);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    //  SNT 13-03-2020 function to load Appliance view
      public function BrandView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Brands";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Users/Brands.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
    //  SNT 14-03-2020 function to load warehouse view
      public function WarehouseView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Warehouses";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Users/Warehouses.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
}
