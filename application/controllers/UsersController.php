<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersController
 *
 * @author CxB012
 */
class UsersController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
//$this->load->library('form_validation');
        $this->load->model('UserModel');
        $this->load->helper("security");
        $this->load->library('session');
        /*if(! $this->db->conn_id){
           header('Location: redirect-to-customepage');
            exit();
        }  */
    }

//KCJ 17th Feb 2020 load login view it will be treated as default view
    public function LoadLoginView() {

        $this->session->sess_destroy();
        $data['Title'] = "Login";
        $data['Heading'] = AppName;
        $this->load->view('Layouts/_LayoutLoginHeader.php', $data);
        $this->load->view('Users/Login.php');
        $this->load->view('Layouts/_LayoutLoginFooter.php');
    }

//KCJ 17th Feb 2020 action to login user with username and password with provided credential and set session data
    public function AuthenticateUser() {

        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $LoginObject = $this->input->post('student', TRUE);
            $Username = $LoginObject["UserName"];
            $Password = $LoginObject["Password"];
            $data = $this->UserModel->AuthenticateUser($Username, $Password);
            if ($data != -1 && $data != 0) {
                $UserData = array(
                    'Username' => $data[0]->Username,
                    'RoleType' => $data[0]->RoleType,
                    'Id' => $data[0]->Id,
                    'Name_' => $data[0]->Name,
                    'StateName' => $data[0]->StateName !=''? $data[0]->StateName : "0",
                    'DistrictName' => $data[0]->DistrictName !=''? $data[0]->DistrictName : "0",
                    'WarehouseName' => $data[0]->WarehouseName !=''? $data[0]->WarehouseName : "0",
                );
                $this->session->set_userdata($UserData);
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusSuccess, 'message' => "Login Successfully", 'data' => $data)));
            } else if ($data == 0) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusFailed, 'message' => "Invalid credential", 'data' => '')));
            } else {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusFailed, 'message' => failmessage, 'data' => '')));
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array('status' => StatusFailed, 'message' => failmessage, 'data' => '')));
        }
    }
    
    
      // SNT 5th martch 2020 function to create User
    public function CreateUser(){
        if ($this->session->userdata('Username') != "") {
            try {

                $_POST = json_decode(file_get_contents('php://input'), true);
                $Name_ = $this->input->post('Name_');
                $Username = $this->input->post('Username');
                $Password_ = password_hash(DefaultPassword, PASSWORD_BCRYPT);
                $StateName = $this->input->post('StateName');
                $DistrictName = $this->input->post('DistrictName');
                $WarehouseName = $this->input->post('WarehouseName');
                $RoleType = $this->input->post('RoleType');
                $CreatedBy = $this->session->userdata('Username');
                $result = $this->UserModel->CreateUser($Name_, $Username, $Password_, $StateName, $DistrictName, $WarehouseName, $RoleType, $CreatedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusFailed, 'message' => failmessage, 'data' => '')));
            }
        } else {
            redirect('/');
        }
    }
    
    // SNT 5th martch 2020 function to Fetch Roles
    
      public function FetchRoles(){
          
          try {
            $result = $this->UserModel->FetchRoles();
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
      }
        
     //  SNT 5th martch 2020 function to Reset Password  
        public function ResetUserPassword(){
         if ($this->session->userdata('Username') != "") {
            try {
                $_POST = json_decode(file_get_contents('php://input'), true);
                $User = $this->input->post('Username');
                $pass = $this->input->post('pass');
                $Password_ = password_hash($pass, PASSWORD_BCRYPT);
                $ModifiedBy = $this->session->userdata('Username'); //counterid
                $result = $this->UserModel->ResetUserPassword($User, $Password_, $ModifiedBy);
                if ($result) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(array($result)));
                } else {
                    throw new Exception(MsgException);
                }
            } catch (Exception $e) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusFailed, 'message' => failmessage, 'data' => '')));
            }
        } else {
            redirect('/');
        }
    }
    
     //  SNT 09-03-2020 function to load Register view
      public function LoadUserRegisterView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Users";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('Users/Register.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
      //  SNT 12-03-2020 Function use to to get list of users
    function FetchUserList() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $RoleType = $this->input->post('RoleType');
            $StateName = $this->input->post('StateName', TRUE);
            $DistrictName = $this->input->post('DistrictName', TRUE);
            $WarehouseName = $this->input->post('WarehouseName', TRUE);
//            $Parameter = $this->input->post('Para');
            $result = $this->UserModel->FetchUserList($RoleType, $StateName, $DistrictName, $WarehouseName);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    //  SNT 08-04-2020 function to load Change Password
      public function LoadUserChangePasswordView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Change Password";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('SharedViews/ChangePassword.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
    
function fileupload() {
        $target_dir = "uploads/";
        // print_r($_FILES);
         $file_type = $_FILES['file']['type']; //returns the mimetype
         $file_size = $_FILES['file']['size'];

        $allowed = array("image/jpeg", "image/png", "application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        if (!in_array($file_type, $allowed) || $file_size > 5242880) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array('status' => StatusFailed,'data' => '')));
        }
        else{
            $name = time() . '.' . $_FILES['file']['name'];
            $target_path = $target_dir . basename($name);

            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusSuccess,'data' => $name)));
            } else {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array('status' => StatusFailed,'data' => '')));
            }
        }
     
//                $config['upload_path']  =  'uploads/';
//                $config['allowed_types']  = 'gif|jpg|png|jpeg|pdf|txt';
//                $config['overwrite'] =  TRUE;
//                $config['max_size'] = 2048000;
//               
//      
//      		$this->load->library('upload', $config);
//
//      			if ( ! $this->upload->do_upload('file'))
//                {
//                        echo"not uploaded";
//                }
//                else
//                {
//                         $error = $this->upload->display_errors();
//                         print_r($error);
//                }
        }
        
        
    //  SNT 18-04-2020 Function use to to get list of users
    function FetchInternalUserList() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $RoleType = $this->input->post('RoleType');
            $StateName = $this->input->post('StateName', TRUE);
            $DistrictName = $this->input->post('DistrictName', TRUE);
            $Username = $this->input->post('Username', TRUE);
            $result = $this->UserModel->FetchInternalUserList($Username, $RoleType, $StateName, $DistrictName);
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

}
