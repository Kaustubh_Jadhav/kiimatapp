<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CashController
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MISReportsController extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        //$this->load->helper('form');
        //$this->load->library('form_validation');
        //$this->load->helper("security");
        $this->load->library('session');
        $this->load->model('MISReports');
        $this->load->helper("url");
    }
    
    
    //SNT 19-03-2020 Load  page for reports
    public function LoadMISView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Stock Position";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('MISReports/StockPosition.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
     function FetchStock() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $Appliance = $this->input->post('Appliance');
             $Username = $this->input->post('Username');
            $result = $this->MISReports->FetchStock($Appliance, $State, $District, $Username );
           
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    //SNT 27-03-2020 Load  page for reports
    public function LoadSALEView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Daily Sales";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('MISReports/SaleSummary.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
     function FetchSaleSummary() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $Appliance = $this->input->post('Appliance');
            $From = $this->input->post('From'); 
            $To = $this->input->post('To'); 
            $result = $this->MISReports->FetchSaleSummary($Appliance, $State, $District,$From, $To );
           
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }

     //SNT 31-03-2020 Load  page for reports
    public function LoadCashPendencyview() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Cash Pendency";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('MISReports/CashPendency.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
     function FetchCash() {
        try {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $State = $this->input->post('State');
            $District = $this->input->post('District');
            $Username = $this->input->post('Username');
            $result = $this->MISReports->FetchCash($State, $District, $Username);
           
            if ($result) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode(array($result)));
            } else {
                throw new Exception(MsgException);
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
        }
    }
    
    //chart
    public function ChartView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Expense Chart";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('MISReports/chart.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
//    function FetchGraph() {
//        try {
//            $result = $this->MISReports->FetchGraph();
//            //var_dump($result);
//            if ($result) {
//                $this->output->set_content_type('application/json');
//                $this->output->set_output(json_encode(array($result)));
//            } else {
//                throw new Exception(MsgException);
//            }
//        } catch (Exception $e) {
//            //var_dump($e->getMessage());
//            $this->output->set_content_type('application/json');
//            $this->output->set_output(json_encode(array(array('status' => StatusFailed, 'message' => MsgError, 'data' => ''))));
//        }
//    }

}

