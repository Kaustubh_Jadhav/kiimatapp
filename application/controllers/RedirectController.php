<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CashController
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class RedirectController extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper("url");
    }
    
    
    //SNT 19-03-2020 Load  page for reports
    public function LoadRedirectView() {
        if ($this->session->userdata('Username') != "") {
            $data['Title'] = "Stock Position";
            $data['Heading'] = AppName;
            $this->load->view('Layouts/_LayoutMainHeader.php', $data);
            $this->load->view('User/Redirect.php');
            $this->load->view('Layouts/_LayoutMainFooter.php');
        } else {
            redirect('/');
        }
    }
    
}

