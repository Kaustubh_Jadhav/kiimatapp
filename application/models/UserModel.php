<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserModel
 *
 * @author CxB012
 */
class UserModel extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->model('CommonModel');
    }

    public function AuthenticateUser($Username, $Password) {

        $data = array(
            'UsernameEntered' => $Username,
            'Comments' => $Password
        );
        $this->db->trans_begin();
        $this->db->insert('ki_user_login_track', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            $this->db->select('usr.Id as Id, usr.RoleType as RoleType, usr.Username as Username, usr.Name_ as Name, usr.StateName as StateName, usr.DistrictName as DistrictName, usr.WarehouseName as WarehouseName, usr.Password_ as Password', FALSE);
            $this->db->from('ki_users as usr');
            $this->db->where('Username', $Username);
            $this->db->where('IsActive', 1);
            $query = $this->db->get();
            $data = $query->result();
            if (!$query) {
                return -1;
            } else if (count($data) == 0) {
                return 0;
            } else {
                $DbPassword = $data[0]->Password;
                $IsPasswordVerfy = password_verify($Password, $DbPassword);
                if ($IsPasswordVerfy == 1 && strtolower($data[0]->Username) == strtolower($Username)) {
                    return $data;
                } else {
                    return 0;
                }
            }
        }
    }

    // SNT 5th martch 2020 function to create User

    function CreateUser($Name_, $Username, $Password_, $StateName, $DistrictName, $WarehouseName, $RoleType, $CreatedBy) {
        $data = array(
            'Name_' => $Name_,
            'Username' => strtolower($Username),
            'Password_' => $Password_,
            'StateName' => $StateName,
            'DistrictName' => $DistrictName,
            'WarehouseName' => $WarehouseName,
            'RoleType' => $RoleType,
            'CreatedBy' => $CreatedBy
        );
        $query = $this->db->insert('ki_users', $data);
        $error = $this->db->error();

        if (!$query) {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $result = array(
                'status' => StatusSuccess,
                'message' => 'User Created Successfully.',
                'data' => $data
            );
        }
        return $result;
    }

    // SNT 5th martch 2020 function to Fetch Roles

    public function FetchRoles() {
        $this->db->select('RoleType');
        $this->db->order_by("Id", "asc");
        $query = $this->db->get_where('ki_roles', array('IsActive' => 1));
        $error = $this->db->error();
        if (!$query) {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
            return $result;
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
                return $result;
            } else {
                return false;
            }
        }
    }

    //  SNT 5th martch 2020 function to Reset Password  

    public function ResetUserPassword($User, $Password_, $ModifiedBy) {
        $this->CommonModel->CurrentTime();
        if ($Password_ != '') {
            $data = array(
                "Password_" => $Password_,
                "ModifiedOn" => date('Y-m-d H:i:s'),
                'ModifiedBy' => $ModifiedBy,
            );
            $this->db->trans_begin();
            $query = $this->db->update('ki_users', $data, array('Username' => $User, 'IsActive' => 1));
            // var_dump($query);
            $error = $this->db->error();
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
              
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            } else {
                $this->db->trans_commit();
                if (!$query) {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                } else {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => 'Password change successfully.',
                        'data' => $data
                    );
                }
            }
        } else {
            $result = array(
                'status' => StatusFailed,
                'message' => 'Password is blank',
                'data' => $data
            );
        }
        return $result;
    }
    
     //  SNT 12-03-2020 Function use to to get user List 
    function FetchUserList($RoleType, $StateName, $DistrictName, $WarehouseName) {

        $this->db->select("usr.Id, usr.RoleType, usr.Name_, UCASE(usr.Username) as Username, usr.StateName, usr.DistrictName, usr.WarehouseName, usr.CreatedBy, usr.ModifiedBy,
        CASE WHEN usr.ModifiedOn IS NULL THEN usr.CreatedOn else usr.ModifiedOn END as m_dt,
        CASE WHEN usr.ModifiedBy IS NULL THEN usr.CreatedBy else usr.ModifiedBy END as m_md,
        ", FALSE);
        $this->db->from('ki_users as usr');
        $this->db->join('ki_roles as kr', 'kr.RoleType = usr.RoleType');
        
        if ($StateName != '0') {
            $this->db->where('usr.StateName', $StateName);
        }
        if ($DistrictName != '0') {
            $this->db->where('usr.DistrictName', $DistrictName);
        }
        if ($WarehouseName != '0') {
           $this->db->where('usr.WarehouseName', $WarehouseName);
        } 
        if ($RoleType != '' && $RoleType != '0') {
           // echo $RoleType;
            $this->db->where('usr.RoleType', $RoleType);
        }
        
//        if($Parameter == 0){
//            $this->db->order_by("usr.RoleType = '" . ROL_CTR . "'", 'DESC');
//            $this->db->order_by("usr.RoleType = '" . ROL_WA . "'", 'DESC');
//            $this->db->order_by("usr.RoleType = '" . ROL_DT . "'", 'DESC');
//            $this->db->order_by("usr.RoleType = '" . ROL_ST . "'", 'DESC');
//            $this->db->order_by("usr.RoleType = '" . ROL_SA . "'", 'DESC');
            $this->db->order_by("kr.Id","asc");
            $this->db->order_by("usr.Username","asc");
//        }
//        else{
             //$this->db->order_by("m_dt", "desc");
//        }
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }
    
      //  SNT 12-03-2020 Function use to to get user List 
    function FetchInternalUserList($Username,$RoleType, $StateName, $DistrictName) {

        $this->db->select("usr.Id, usr.RoleType, usr.Name_, usr.Username, usr.StateName, usr.DistrictName, usr.WarehouseName, usr.CreatedBy, usr.ModifiedBy,
        CASE WHEN usr.ModifiedOn IS NULL THEN usr.CreatedOn else usr.ModifiedOn END as m_dt,
        CASE WHEN usr.ModifiedBy IS NULL THEN usr.CreatedBy else usr.ModifiedBy END as m_md,
        ", FALSE);
        $this->db->from('ki_users as usr');
        $this->db->join('ki_roles as kr', 'kr.RoleType = usr.RoleType');
//        if ($StateName != '0') {
//            $this->db->where('usr.StateName', $StateName);
//        }if ($DistrictName != '0') {
//            $this->db->where('usr.DistrictName', $DistrictName);
//        }
//        if ($RoleType != '' && $RoleType != '0') {
//            $this->db->where('usr.RoleType', $RoleType);
//        }
        
        if ($DistrictName != '0') {
            $this->db->where('usr.DistrictName', $DistrictName);
          }
        if ($StateName != '0') {
            $this->db->where('usr.StateName', $StateName);
         }
        
        if($RoleType == ROL_CTR){
            $roles = array(ROL_WA, ROL_DT);
            $this->db->where_in('usr.RoleType',$roles);
            $this->db->or_where('usr.RoleType', ROL_ST );
            $this->db->where('usr.StateName', $StateName);
            $this->db->or_where('usr.RoleType',ROL_SA);
           
        }
        if($RoleType == ROL_WA){
            $roles = array( ROL_DT);
            $this->db->where_in('usr.RoleType',$roles);
            $this->db->or_where('usr.RoleType', ROL_ST );
            $this->db->where('usr.StateName', $StateName);
            $this->db->or_where('usr.RoleType',ROL_SA);
//            $this->db->or_where('usr.Username', $Username);
        }
        if($RoleType == ROL_DT){
            $roles = array( ROL_WA, ROL_DT);
            $this->db->where_not_in('usr.RoleType',$roles);
            $this->db->or_where('usr.RoleType', ROL_ST );
            $this->db->where('usr.StateName', $StateName);
            $this->db->or_where('usr.RoleType',ROL_SA);
//          $this->db->or_where('usr.Username', $Username);
        }
        if($RoleType == ROL_ST){
            $roles = array( ROL_WA, ROL_DT, ROL_ST);
            $this->db->where_not_in('usr.RoleType',$roles);
            $this->db->or_where('usr.RoleType',ROL_SA);
//            $this->db->or_where('usr.Username', $Username);
        }
        if($RoleType == ROL_SA){
//            $this->db->where('usr.Username', $Username);
        }
       
        
        $this->db->order_by("kr.Id","asc");
        $this->db->order_by("usr.Username","asc"); 
        
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

}
