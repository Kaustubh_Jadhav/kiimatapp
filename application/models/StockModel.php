<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockModel
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//SNT 14th Feb 2020  model func to  save counterstock  

class StockModel extends CI_Model {

    //put your code here
    public function __construct() {

        parent::__construct();
        $this->load->model('CashModel');
        $this->load->model('CommonModel');
    }

    // SNT 14th Feb 2020 function to ftech counter stock
    function FetchCounterStock($ApplianceName, $Username, $stockType) {
        //$this->db->select('Qty, QtyUnApproved, Qty-QtyUnApproved');
        $this->db->select('cs.Qty, cs.QtyUnApproved, aplc.RatePerPc, (cs.Qty -IFNULL(cs.QtyUnApproved,0)) As QtyAvailable', FALSE);
        $this->db->from('ki_appliances as aplc');
        $this->db->join('ki_counter_stock as cs', 'cs.ApplianceName=aplc.Name_');
        $this->db->where('ApplianceName', $ApplianceName);
        $this->db->where('Username', $Username);
        $this->db->where('StockType', $stockType);
        $this->db->where('cs.IsActive', 1);
        $query = $this->db->get();
        $error = $this->db->error();
        // var_dump($error);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {

            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                //var_dump($query);
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    // SNT 27th Feb 2020 function to ftech warehouse stock
    function FetchWareHouseStock($WarehouseName, $ApplianceName, $stockType) {

        $this->db->select('ws.Qty, ws.QtyUnApproved, aplc.RatePerPc, (ws.Qty -IFNULL(ws.QtyUnApproved,0)) As QtyAvailable', FALSE);
        $this->db->from('ki_appliances as aplc');
        $this->db->join('ki_warehouse_stock as ws', 'ws.ApplianceName=aplc.Name_');
        $this->db->where('ApplianceName', $ApplianceName);
        $this->db->where('WarehouseName', $WarehouseName);
        $this->db->where('StockType', $stockType);
        $this->db->where('ws.IsActive', 1);
        $query = $this->db->get();
        $error = $this->db->error();
        // var_dump($error);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {

            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                // var_dump($data);
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    // SNT 14th Feb 2020 model func to save records of stockdistribtion 
    function SaveCounterMISEntry($Username, $ApplianceName, $QtyDistributed, $QtyReplaced, $QtyBD, $Comments, $CashReceived, $ModifiedBy, $TrnDate) {
        $this->CommonModel->CurrentTime();
        $data = array(
            'Username' => $Username,
            'ApplianceName' => $ApplianceName,
            'QtyDistributed' => $QtyDistributed,
            'QtyReplaced' => $QtyReplaced,
            'QtyBD' => $QtyBD,
            'Comments' => $Comments,
            'CashReceived' => $CashReceived,
            'CreatedBy' => $ModifiedBy,
            'EntryDate' => $TrnDate
        );

       // print_r(date("Y-m-d H:i:s", strtotime($TrnDate)));
        //$this->db->where('CounterId', $UserId);
        $query = $this->db->insert('ki_stock_distribution', $data);
        $this->db->trans_begin();
        $error = $this->db->error();
        if (!$query) {
            // echo"bd";
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {

            $QtyToAdd = $QtyDistributed + $QtyReplaced + $QtyBD;
            //$QtyToUnapproved = 0;
            $StockType = unserialize(StockType);
            //$StockType = $StockType1[0];
            //print_r($StockType);
            $TrnTypeStatus = unserialize(TrnType);
            $StockTransferStatus = unserialize(StockTransferStatus);

            //MIS_ENTRY", "MIS_ENTRY_D"

            $this->UpdateCounterStock($Username, $ApplianceName, $StockType[0], $StockTransferStatus[0], $QtyToAdd, $ModifiedBy);
            if ($CashReceived != 0) {
                $CashUpdateStatus = unserialize(CashUpdateStatus);
                $ActionType = $CashUpdateStatus[0];
                $this->CashModel->UpdateUserCash($Username, $CashReceived, $ActionType, $ModifiedBy);
            }

            if ($QtyReplaced > 0) {
                //$StockType = $StockType1[1];
                // $this->UpdateCounterStock($Username, $ApplianceName, $QtyToAdd, $QtyToUnapproved, $StockType);
                $this->UpdateCounterStock($Username, $ApplianceName, $StockType[1], $StockTransferStatus[1], $QtyReplaced, $ModifiedBy);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //echo"hwl";
                $this->db->trans_rollback();
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            } else {
                $this->db->trans_commit();
                //echo"hii";
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgstockDist,
                    'data' => $data
                );
            }
            // select query to detch MISCounter STock
        }
        return $result;
    }

    //SNT 14th Feb 2020  model func to  save counterstock  
    function FetchCounterMISList($Username, $ApplianceName, $From, $To) {
        //$this->CommonModel->CurrentTime();
        $this->db->select("Id, Username, ApplianceName, QtyDistributed, QtyReplaced, QtyBD, round(CashReceived) as CashReceived, Comments, CASE WHEN EntryDate IS NULL THEN '' else DATE_FORMAT(EntryDate,'%d-%b-%Y') END as EntryDate, CreatedOn , CreatedBy");
        $this->db->from('ki_stock_distribution');
        $this->db->order_by('CreatedOn', 'desc');
        if ($From != '' ) {
           $this->db->where('CreatedOn >=', $From);
        } if ($To != '') {
            $this->db->where('CreatedOn <=', $To);
        }
        if ($ApplianceName != '0') {
            $this->db->where('ApplianceName', $ApplianceName);
        } if ($Username != '0') {
            $this->db->where('Username', $Username);
        }
        
        
        /*if ($Username != '') {
            $query = $this->db->get_where('ki_stock_distribution', array('Username' => $Username));
        } else {
            $query = $this->db->get('ki_stock_distribution');
        }*/
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            //var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
    }

    //SNT 10th apr 2020 func to find available stock during stock transfer
    function fetchStockdata($ApplianceName, $Username_From, $StockType,$TrnQty, $TrnType){
        
        $TrnTypeStatus = unserialize(TrnType);
        
        if ($TrnType == $TrnTypeStatus[0] || $TrnType == $TrnTypeStatus[1]) {
            $counterStock = $this->FetchCounterStock($ApplianceName, $Username_From, $StockType);
            $Status = $counterStock["status"];
            $Data = $counterStock["data"];
            if ($Status == 1) {
                $available = $Data[0]->QtyAvailable;
                if ($available >= $TrnQty && $TrnQty > 0) {
                    return  true;
                }
                else{
                    return false;
                }
            }else{
                return false;
            }
        }
        
         else if ($TrnType == $TrnTypeStatus[2] || $TrnType == $TrnTypeStatus[3] || $TrnType == $TrnTypeStatus[4] || $TrnType == $TrnTypeStatus[5] || $TrnType == $TrnTypeStatus[6]) {
         
            $warehouseStock = $this->FetchWareHouseStock($Username_From, $ApplianceName , $StockType);
            $Status = $warehouseStock["status"];
            $Data = $warehouseStock["data"];
           // echo $available = $Data[0]->QtyAvailable;
            if ($Status == 1) {
                $available = $Data[0]->QtyAvailable;
                if ($available >= $TrnQty && $TrnQty > 0) {
                    return  true;
                }
                else{
                    return false;
                }
            }else{
                return false;
            }
         }
         else{
             return true;
         }
         
    }
    // SNT 27th Feb 2020  func to save stock transfer
    function SaveStockTransfer($TrnType, $ApplianceName, $StockType, $BrandName, $TrnDate, $TrnQty, $Username_From, $Username_To, $Comments, $ModifiedBy) {

//        
//        $counterStock  = $this->FetchCounterStock($ApplianceName, $Username_From, $StockType);
//        $Status = $counterStock["status"];
//        $Data = $counterStock["data"];
//        if ($Status == 1){
//         echo $available = $Data[0]->QtyAvailable;
//         if($available >= $TrnQty){
//             echo"1";
//         }
//         else{
//             echo"0";
//         }
//          
//        } 
       // $check = $this->fetchStockdata($ApplianceName, $Username_From, $StockType, $TrnQty, $TrnType);
        //print_r($check);
        if ($this->fetchStockdata($ApplianceName, $Username_From, $StockType, $TrnQty, $TrnType)) {

            $this->CommonModel->CurrentTime();
            $data = array(
                'TrnType' => $TrnType,
                'StockType' => $StockType,
                'TrnDate' => $TrnDate,
                'BrandName' => $BrandName,
                'ApplianceName' => $ApplianceName,
                'Username_From' => $Username_From,
                'Username_To' => $Username_To,
                'Comments' => $Comments,
                'Qty' => $TrnQty,
                'CreatedBy' => $ModifiedBy
            );

            $this->db->trans_begin();
            $query = $this->db->insert('ki_stock_transfer', $data);

            $TrnTypeStatus = unserialize(TrnType);
            $StockTransferStatus = unserialize(StockTransferStatus);

            //CTR_WH_F,CTR_WH_D=>Trn type
            //"MIS_ENTRY", "MIS_ENTRY_D", "TRNOUT", "TRNQTYAPPROVED", "TRNOUTREJECT", "TRNINAPPROVED"
            if ($TrnType == $TrnTypeStatus[0] || $TrnType == $TrnTypeStatus[1])
               $StockReceivedData=  $this->UpdateCounterStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[2], $TrnQty, $ModifiedBy);
            else if ($TrnType == $TrnTypeStatus[2] || $TrnType == $TrnTypeStatus[3] || $TrnType == $TrnTypeStatus[4] || $TrnType == $TrnTypeStatus[5] || $TrnType == $TrnTypeStatus[6]) {
                $StockReceivedData = $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[2], $TrnQty, $ModifiedBy);
            } else if ($TrnType == $TrnTypeStatus[7]){
                $StockReceivedData = true;
            }
            if($StockReceivedData == true){
            
                $this->db->trans_complete();
                $error = $this->db->error();
                if ($this->db->trans_status() === FALSE) {
                    //echo"hii";
                    $this->db->trans_rollback();
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => ''
                    );
                } else {

                    $this->db->trans_commit();
                    if (!$query) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => MsgError,
                            'data' => $error
                        );
                    } else {
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => MsgstockDist,
                            'data' => $data
                        );
                    }
                }
        } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }else {

            $result = array(
                'status' => StatusFailed,
                'message' => 'Stock Transfer failed. Kindly refresh the page and try again.',
                'data' => ''
            );
        }

        return $result;
    }

    // SNT 15th Feb 2020 method func to display stock transfer List
    function FetchStockTransferList($Username, $State, $District, $WarehouseName, $Appliance, $StockType, $Brand, $Status, $From, $To) {
        $this->db->select("str.StockType  as Stock Type,  DATE_FORMAT(str.TrnDate ,'%d-%b-%Y')as `Transaction Date`, str.BrandName as Brand Name, str.ApplianceName as  Appliance Name,  str.Username_From as Transfer From, str.Username_To as Transfer To, str.Qty, str.Comments, str.Status, str.Username_Approver as Username Approver, str.ApproverRemarks as Approver Remarks,  CASE WHEN str.ActionTakenOn IS NULL THEN DATE_FORMAT(str.CreatedOn,'%d-%b-%Y %T') else DATE_FORMAT(str.ActionTakenOn,'%d-%b-%Y %T') END as  `Action TakenOn`");
        $this->db->from('ki_stock_transfer as str');
        $this->db->join('ki_users as usr', 'str.Username_From = usr.Username  or str.Username_From = usr.WarehouseName');
        if ($From != '' ) {
           $this->db->where('str.TrnDate >=', $From);
        } if ($To != '') {
            $this->db->where('str.TrnDate <=', $To);
        }
        if ($Username != '0') {
            //$this->db->join('ki_users as usr', 'str.Username_From = usr.Username');
            $this->db->where('usr.Username', $Username);
        }if ($WarehouseName != '0') {
             //$this->db->distinct();
            //echo $WarehouseName;
            // $this->db->join('ki_users as usr', 'usr.WarehouseName  = str.Username_From');
             $this->db->where('str.Username_From', $WarehouseName);
             $this->db->group_by("str.Id");
        } 
        if ($State != '0') {
           // echo $State;
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
           //  echo $District;
            $this->db->where('usr.DistrictName', $District);
        }if ($Status != '0') {
            //echo $Status;
            $this->db->where('str.Status', $Status);
        }
         if ($Appliance != '0') {
           // echo $State;
            $this->db->where('str.ApplianceName', $Appliance);
        } if ($StockType != '0') {
           //  echo $District;
            $this->db->where('str.StockType', $StockType);
        }if ($Brand != '0') {
            //echo $Status;
            $this->db->where('str.BrandName', $Brand);
        }
        
          $this->db->order_by('str.Id', 'desc');
         /* $this->db->order_by('CreatedOn', 'desc');
          $this->db->order_by('Id', 'desc');
        
         if ($Username != '') {
            $query = $this->db->get_where('ki_stock_transfer', array('Username_From' => $Username));
        } else {
            $query = $this->db->get('ki_stock_transfer');
        }*/
        $query = $this->db->get();
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
    }

    // SNT 17th Feb 2020 method func to  fetch pending stock transfer List

    function stockApprovedRejectList($Username, $Status, $RoleType, $State, $District) {
        
        $TrnTypeStatus = unserialize(TrnType);
        $this->db->select('str.Id, str.StockType,usrs.DistrictName, usrs.RoleType, usrs.StateName ,str.TrnDate, str.BrandName, str.ApplianceName, str.Username_From, str.TrnType, str.Username_To, str.Qty, str.Comments, str.Status, str.Username_Approver, str.ApproverRemarks, str.ActionTakenOn, str.CreatedOn, str.CreatedBy, str.ModifiedOn, str.ModifiedBy');
        $this->db->from('ki_stock_transfer as str');
        if ($RoleType == ROL_CTR || $RoleType == ROL_WA) { 
            
            $this->db->join('ki_users as usrs', ' usrs.WarehouseName = str.Username_From OR usrs.Username = str.Username_From');
            $this->db->where('str.Username_To', $Username);
            $this->db->where('usrs.IsActive', 1);
            $this->db->where('str.TrnType!=', $TrnTypeStatus[7]);
           
        } 
        else if ($RoleType == ROL_ST)
        {
            
            //$this->db->select("usr.StateName, usr.DistrictName, usr.WarehouseName");
            //$this->db->from('ki_users as usr');
            //$this->db->where('usr.Username', $Username);
           // $this->db->where('usr.IsActive', 1);
           // $query = $this->db->get();
           // var_dump($query);
            //if ($this->db->affected_rows() > 0) {
                    //$data = $query->result();
                    // $StateName = $data[0]->StateName;
                    // $DistrictName = $data[0]->DistrictName;
                   
                     
                     //$this->db->select('str.Id, str.StockType,usrs.DistrictName, usrs.RoleType, usrs.StateName ,str.TrnDate, str.BrandName, str.ApplianceName, str.Username_From, str.TrnType, str.Username_To, str.Qty, str.Comments, str.Status, str.Username_Approver, str.ApproverRemarks, str.ActionTakenOn, str.CreatedOn, str.CreatedBy, str.ModifiedOn, str.ModifiedBy');
                     //$this->db->from('ki_stock_transfer as str');
                     $this->db->join('ki_users as usrs', 'usrs.WarehouseName = str.Username_From OR usrs.WarehouseName= str.Username_To');
                     $this->db->where('usrs.IsActive', 1);
                          
                         $TrnType = array($TrnTypeStatus[5], $TrnTypeStatus[6], $TrnTypeStatus[7]);
                         $this->db->where_in('str.TrnType',$TrnType);
                      
//                          if($StateName != null || $StateName !=''){
//                             // echo"hii";
//                                 $this->db->where('usrs.StateName', $StateName);
//                          }
//                          if($DistrictName != null || $DistrictName !='' ){
//                               $this->db->where('usrs.DistrictName', $DistrictName);
////                          }
//                         
                         if($State !='0'){
                                 $this->db->where('usrs.StateName', $State);
                          }
                          if($District !='0'){
                               $this->db->where('usrs.DistrictName', $District);
                         }
                          
//                        $this->db->where('str.Status', $Status);  
//                        $this->db->group_by("str.Id");
//                        $this->db->order_by('str.TrnDate', 'desc');
                       
            // }
        }
        else
        {
            //echo"a";
            // do nothing
        }
       
        $this->db->where('str.Status', $Status);
        $this->db->group_by("str.Id");
        $this->db->order_by('str.CreatedOn', 'desc');
        $query1 = $this->db->get();
        $error = $this->db->error();
        if (!$query1) {
          //  var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query1->result();
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
}

    //SNT 17th Feb 2020 Function use to update  pending stock transfer List
    function ApproveRejectStockTransfer($Id, $Status, $Username_Approver, $ActionTakenOn, $ApproverRemarks, $ModifiedBy, $Username_From, $TrnType, $Username_To, $TrnQty, $ApplianceName, $StockType) {
        $this->CommonModel->CurrentTime();
        $data = array(
            "Status" => $Status,
            "Username_Approver" => $Username_Approver,
            "ActionTakenOn" => $ActionTakenOn,
            "ApproverRemarks" => $ApproverRemarks,
            "ModifiedOn" => date('Y-m-d H:i:s'),
            "ModifiedBy" => $ModifiedBy
        );
        $this->db->trans_begin();
        $query = $this->db->update('ki_stock_transfer', $data, array('Id' => $Id, 'Status'=>strtolower(TRN_STATUS_PENDING)));
        $error = $this->db->error();
        $TrnTypeStatus = unserialize(TrnType);
        $StockTransferStatus = unserialize(StockTransferStatus);
        
        //StockTransferStatus ="MIS_ENTRY", "MIS_ENTRY_D", "TRNOUT", TRNQTYAPPROVED", "TRNOUTREJECT", "TRNINAPPROVED"
        //Trntype = "CTR_WH_F", "CTR_WH_D", "WH_CTR_F", "WH_WH_F", "WH_WH_D", "WH_VEN_F", "WH_VEN_D", "VEN_WH"
        //Trntype = "WH_VEN_F", "WH_VEN_D", 

        if ($Status == TRN_STATUS_APPROVED) {
            //counter to warehoue app
            //Trntype = "CTR_WH_F", "CTR_WH_D", 
            if (($TrnType == $TrnTypeStatus[0] || $TrnType == $TrnTypeStatus[1])) {
                 $this->UpdateCounterStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[3], $TrnQty, $ModifiedBy);
                 $this->UpdateWarehouseStock($Username_To, $ApplianceName, $StockType, $StockTransferStatus[5], $TrnQty, $ModifiedBy);
             }
            //warehouse to warehoue app  //Trntype = "WH_WH_D", "WH_WH_F",
            else if (($TrnType == $TrnTypeStatus[3] || $TrnType == $TrnTypeStatus[4])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[3], $TrnQty, $ModifiedBy);
                $this->UpdateWarehouseStock($Username_To, $ApplianceName, $StockType, $StockTransferStatus[5], $TrnQty, $ModifiedBy);
            }
            //ven to warehouse app "VEN_WH"
            else if (($TrnType == $TrnTypeStatus[7])) {
                $this->UpdateWarehouseStock($Username_To, $ApplianceName, $StockType, $StockTransferStatus[5], $TrnQty, $ModifiedBy);
            }
            //w to ctr app "WH_CTR_F",                
            else if (($TrnType == $TrnTypeStatus[2])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[3], $TrnQty, $ModifiedBy);
                $this->UpdateCounterStock($Username_To, $ApplianceName, $StockType, $StockTransferStatus[5], $TrnQty, $ModifiedBy);
            }
            //warehose to vendor app "WH_VEN_F", "WH_VEN_D"
            else if (($TrnType == $TrnTypeStatus[5] || $TrnType == $TrnTypeStatus[6])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[3], $TrnQty, $ModifiedBy);
            }
        } else if ($Status == TRN_STATUS_REJECTED) {
            //counter to warehoue reject "CTR_WH_F", "CTR_WH_D"
            if (($TrnType == $TrnTypeStatus[0] || $TrnType == $TrnTypeStatus[1])) {
                $this->UpdateCounterStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[4], $TrnQty, $ModifiedBy);
            }
            //warehouse to warehoue rej "WH_WH_F", "WH_WH_D"
            else if (($TrnType == $TrnTypeStatus[3] || $TrnType == $TrnTypeStatus[4])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[4], $TrnQty, $ModifiedBy);
            }
            //w to ctr reject "WH_CTR_F"
            else if (($TrnType == $TrnTypeStatus[2])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[4], $TrnQty, $ModifiedBy);
            }
            //warehose to vendor rej "WH_VEN_F", "WH_VEN_D"
            else if (($TrnType == $TrnTypeStatus[5] || $TrnType == $TrnTypeStatus[6])) {
                $this->UpdateWarehouseStock($Username_From, $ApplianceName, $StockType, $StockTransferStatus[4], $TrnQty, $ModifiedBy);
            }
        }
       
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //echo"hwl";
                $this->db->trans_rollback();
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            } else {
                $this->db->trans_commit();
                //echo"hii";
                //var_dump($query);
                if (!$query) {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                } else {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => MsgCommon,
                        'data' => $data
                    );
                }
            }
      
        return $result;
    }

    //SNT 20th Feb 2020 update counter stock after stock transfer/distribution
    function UpdateCounterStock($Ctr_Username, $ApplianceName, $StockType, $ActionType, $TrnQuantity, $ModifiedBy) {
        $this->CommonModel->CurrentTime();
        $StockTypes = unserialize(StockType);
        //$StockType1 = $StockTypes[0];
        $IsDataModified = 0;
        $StockTransferStatus = unserialize(StockTransferStatus);
        $this->db->select('Id, Qty, QtyUnApproved');
        //$this->db->limit(1);
        $query = $this->db->get_where('ki_counter_stock', array('Username' => $Ctr_Username, 'ApplianceName' => $ApplianceName, 'StockType' => $StockType, 'IsActive' => 1));
        //var_dump($query);
        if ($query) {
            if ($this->db->affected_rows() > 0) {
                // echo"hii";
                foreach ($query->result() as $row) {
                    $Qty = $row->Qty;
                    $QtyUnApproved = $row->QtyUnApproved;
                }
                //echo $Qty;
                //echo $QtyUnApproved;
                //echo $TrnQuantity;
                //MIS_ENTRY", "MIS_ENTRY_D", "TRNOUT", "TRNQTYAPPROVED", "TRNOUTREJECT", "TRNINAPPROVED"
                if ($ActionType == $StockTransferStatus[2]) {

                    $QtyUnApproved = $QtyUnApproved + $TrnQuantity;
                     $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[3]) {

                    $Qty = $Qty - $TrnQuantity;
                    $QtyUnApproved = $QtyUnApproved - $TrnQuantity;
                    $IsDataModified = 1;
                     
                } else if ($ActionType == $StockTransferStatus[4]) {
                    $QtyUnApproved = $QtyUnApproved - $TrnQuantity;
                    $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[5]) {

                    $Qty = $Qty + $TrnQuantity;
                    $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[0]) {

                    $Qty -= $TrnQuantity;
                     $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[1]) {

                    $Qty = $Qty + $TrnQuantity;
                     $IsDataModified = 1;
                }
                
                
                 if ($IsDataModified == 1) {
                    $data = array(
                        'IsActive' => 0,
                        "ModifiedOn" => date('Y-m-d H:i:s'),
                        'ModifiedBy' => $ModifiedBy,
                    );

                    $sql = $this->db->update('ki_counter_stock', $data, array('Username' => $Ctr_Username, 'ApplianceName' => $ApplianceName, 'StockType' => $StockType, 'IsActive' => 1));

                    $db = array(
                        'Username' => $Ctr_Username,
                        'ApplianceName' => $ApplianceName,
                        'StockType' => $StockType,
                        'Qty' => $Qty,
                        'QtyUnApproved' => $QtyUnApproved,
                        'CreatedBy' => $Ctr_Username
                    );

                    $sqli = $this->db->insert('ki_counter_stock', $db);
                    $error = $this->db->error();
                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => MsgCommon,
                            'data' => $db
                        );
                        return $result;
                    } else {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => MsgError,
                            'data' => $error
                        );
                        return $result;
                    }
                } else {
                    return false;
                }
            }else {

                $db = array(
                    'Username' => $Ctr_Username,
                    'ApplianceName' => $ApplianceName,
                    'StockType' => $StockType,
                    'Qty' => $TrnQuantity,
                    'CreatedBy' => $Ctr_Username
                );


                $sqli = $this->db->insert('ki_counter_stock', $db);
                if ($this->db->affected_rows() > 0) {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => MsgCommon,
                        'data' => $db
                    );
                    return $result;
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => ''
                    );
                    return $result;
                }
            }
        } else {
            return false;
        }
    }

    //SNT 20th Feb 2020 update warehouse stock after stock transfer
    function UpdateWarehouseStock($WarehouseName, $ApplianceName, $StockType, $ActionType, $TrnQuantity, $ModifiedBy) {

        $this->CommonModel->CurrentTime();
        $IsDataModified = 0;
        $StockTransferStatus = unserialize(StockTransferStatus);
        $this->db->select('Id, Qty, QtyUnApproved');
        // $this->db->limit(1);
        $query = $this->db->get_where('ki_warehouse_stock', array('WarehouseName' => $WarehouseName, 'ApplianceName' => $ApplianceName, 'StockType' => $StockType, 'IsActive' => 1));
        //print_r($query);
        if ($query) {
            if ($this->db->affected_rows() > 0) {
                // echo"hii";
                foreach ($query->result() as $row) {
                    $Qty = $row->Qty;
                    $QtyUnApproved = $row->QtyUnApproved;
                }

                //  "TRNOUT", "TRNQTYAPPROVED", "TRNOUTREJECT", "TRNINAPPROVED"
                if ($ActionType == $StockTransferStatus[2]) {
                    $QtyUnApproved = $QtyUnApproved + $TrnQuantity;
                     $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[3]) {
                    $Qty = $Qty - $TrnQuantity;
                    $IsDataModified = 1;
                    $QtyUnApproved = $QtyUnApproved - $TrnQuantity;
                } else if ($ActionType == $StockTransferStatus[4]) {
                    $QtyUnApproved = $QtyUnApproved - $TrnQuantity;
                    $IsDataModified = 1;
                } else if ($ActionType == $StockTransferStatus[5]) {
                    $Qty = $Qty + $TrnQuantity;
                     $IsDataModified = 1;
                }
                
                if ($IsDataModified = 1) {

                    $data = array(
                        'IsActive' => 0,
                        "ModifiedOn" => date('Y-m-d H:i:s'),
                        'ModifiedBy' => $ModifiedBy,
                    );

                    $sql = $this->db->update('ki_warehouse_stock', $data, array('warehouseName' => $WarehouseName, 'ApplianceName' => $ApplianceName, 'StockType' => $StockType, 'IsActive' => 1));
                    if($sql){
                        $db = array(
                            'warehouseName' => $WarehouseName,
                            'ApplianceName' => $ApplianceName,
                            'StockType' => $StockType,
                            'Qty' => $Qty,
                            'QtyUnApproved' => $QtyUnApproved,
                            'CreatedBy' => $ModifiedBy
                        );

                        $sqli = $this->db->insert('ki_warehouse_stock', $db);
                        $error = $this->db->error();

                        if ($this->db->affected_rows() > 0) {
                            $result = array(
                                'status' => StatusSuccess,
                                'message' => MsgCommon,
                                'data' => $db
                            );
                            return $result;
                        } else {
                            $result = array(
                                'status' => StatusFailed,
                                'message' => MsgError,
                                'data' => $error
                            );
                            return $result;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                //return false;
                $db = array(
                    'warehouseName' => $WarehouseName,
                    'ApplianceName' => $ApplianceName,
                    'StockType' => $StockType,
                    'Qty' => $TrnQuantity,
                    'CreatedBy' => $ModifiedBy
                );

                $sqli = $this->db->insert('ki_warehouse_stock', $db);
                $error = $this->db->error();

                if ($this->db->affected_rows() > 0) {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => MsgCommon,
                        'data' => $db
                    );
                    return $result;
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                    return $result;
                }
            }
        } else {
            return false;
        }
    }

}
