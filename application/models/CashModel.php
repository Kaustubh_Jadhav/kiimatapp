<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CashModel
 *
 * @author CxB012
 */
class CashModel extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->model('CashModel');
        $this->load->model('CommonModel');
    }

// SNT 14th Feb 2020 Function use to to get counter cash
    function FetchCounterCash($Username) {

        $this->db->select('CashInHand, CashUnApproved, (CashInHand - IFNULL(CashUnApproved,0)) As CashAvailable', FALSE);
        $this->db->from('ki_user_cash');
        $this->db->where('Username', $Username);
        $this->db->where('IsActive', 1);
        $query = $this->db->get();
        $error = $this->db->error();

        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
            
        } else {

            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                // var_dump($data);
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
                return $result;
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }
    
    //SNT 10th Apr 2020 func to check available cash while cash transfer/Expense
    function fetchCashdata($Username_From, $TrnAmount){
        $fetchUserCash = $this->FetchCounterCash($Username_From);
        $Status = $fetchUserCash["status"];
        $Data = $fetchUserCash["data"];
         if ($Status == 1) {
                $availableCash = $Data[0]->CashAvailable;
                if ($availableCash >= $TrnAmount && $TrnAmount > 0) {
                    return  true;
                }
                else{
                    return false;
                }
            }else{
                return false;
            }
    }

// SNT 14th Feb 2020 Function use to save cash transfer
    function SaveCashTransfer($Username_From, $Username_To, $TrnDate, $TrnAmount, $RecipientAccountType, $Comments, $CreatedBy) {
        if ($this->fetchCashdata($Username_From, $TrnAmount)) {

            $this->CommonModel->CurrentTime();
            $CashUpdateStatus = unserialize(CashUpdateStatus);
            $ActionType = $CashUpdateStatus[3];

            $data = array(
                'Username_From' => $Username_From,
                'Username_To' => $Username_To,
                'TrnDate' => $TrnDate,
                'TrnAmount' => $TrnAmount,
                'RecipientAccountType' => $RecipientAccountType,
                'Comments' => $Comments,
                'CreatedBy' => $CreatedBy,
            );
            $this->db->trans_begin();
            $query = $this->db->insert('ki_cash_transfers', $data);
            $CashReceivedData = $this->UpdateUserCash($Username_From, $TrnAmount, $ActionType, $CreatedBy);
           
            if ($CashReceivedData == true) {
                $this->db->trans_complete();
                $error = $this->db->error();
                if($this->db->trans_status() === FALSE) {
                    // echo"hii";
                    $this->db->trans_rollback();
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => ''
                    );
                } else {
                    $this->db->trans_commit();
                    if (!$query) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => MsgError,
                            'data' => $error
                        );
                    } else {

                        $result = array(
                            'status' => StatusSuccess,
                            'message' => "Cash Transferred submitted for approval.",
                            'data' => $data
                        );
                    }
                }
            } else {
                //echo"by";
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        } else {

            $result = array(
                'status' => StatusFailed,
                'message' => 'Cash Transfer failed. Kindly refresh the page and try again.',
                'data' => ''
            );
        }
        return $result;
    }

// SNT 14th Feb 2020 Function use to get cash transfer history
    function FetchCashTransferList($Username, $State, $District, $RecipientType, $Status, $TransferTo,$From, $To) {
        $this->db->select("DATE_FORMAT(ctr.TrnDate ,'%d-%b-%Y')as `Transaction Date`, ctr.Username_From as From, ctr.RecipientAccountType as Recipient Account Type, ctr.Username_To as To, round(ctr.TrnAmount)as AmountTransferred, ctr.Status, ctr.Comments, ctr.Username_Approver as Username Approver,  CASE WHEN ctr.ActionTakenOn IS NULL THEN '' else DATE_FORMAT(ctr.ActionTakenOn,'%d-%b-%Y %T') END as ActionTakenOn , ctr.ApproverRemarks as  Approver Remarks, ctr.CreatedOn as Created On");
        $this->db->from('ki_cash_transfers as ctr');
        $this->db->join('ki_users as usr', 'usr.Username = ctr.Username_From ');
        if ($From != '' ) {
           $this->db->where('ctr.TrnDate >=', $From);
        } if ($To != '') {
           $this->db->where('ctr.TrnDate <=', $To);
        }
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        } if ($Username != '0') {
            $this->db->where('usr.Username', $Username);
        }
        if ($Status != '0') {
            $this->db->where('ctr.Status', $Status);
        }
        if ($RecipientType != '0') {
            $this->db->where('ctr.RecipientAccountType', $RecipientType);
        }
        if ($TransferTo != '0') {
            $this->db->where('ctr.Username_To', $TransferTo);
        }
        $this->db->order_by('ctr.Id', 'desc');
        
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
            
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
    }

    // SNT 12th Feb 2020 Function use to get save Expense data
    function SaveExpenseEntry($Username, $TrnDate, $ExpenseType, $AmountSpent, $Comments, $CreatedBy) {

        if ($this->fetchCashdata($Username, $AmountSpent)) {
            $this->CommonModel->CurrentTime();
            $data = array(
                'TrnDate' => $TrnDate,
                'ExpenseType' => $ExpenseType,
                'AmountSpent' => $AmountSpent,
                'Comments' => $Comments,
                'Username' => $Username,
                'CreatedBy' => $CreatedBy
            );
            $this->db->trans_begin();
            $query = $this->db->insert('ki_cash_expenses', $data);
            $CashUpdateStatus = unserialize(CashUpdateStatus);
            $CashReceivedData = $this->CashModel->UpdateUserCash($Username, $AmountSpent, $CashUpdateStatus[2], $CreatedBy);
           
            if ($CashReceivedData == true) {
                $this->db->trans_complete();
                $error = $this->db->error();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => ''
                    );
                } else {
                    $this->db->trans_commit();
                    if (!$query) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => MsgError,
                            'data' => $error
                        );
                    } else {
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => 'Expense submitted for  approval.',
                            'data' => $data
                        );
                    }
                }
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        } else {

            $result = array(
                'status' => StatusFailed,
                'message' => 'Add Expense  failed. Kindly refresh the page and try again.',
                'data' => ''
            );
        }
        return $result;
    }

    // SNT 12th Feb 2020 Function use to get  Expense data
       function FetchExpenseList($State, $District, $User, $Status, $ExpType, $From, $To ) {
        $this->db->select("cep.Username , DATE_FORMAT(cep.TrnDate ,'%d-%b-%Y')as `Transaction Date`, cep.ExpenseType as Expense Type ,round(cep.AmountSpent) as `Amount Spent`, cep.Comments, cep.Status,cep.Username_Approver as Username Approver , CASE WHEN cep.ActionTakenOn IS NULL THEN '' else DATE_FORMAT(cep.ActionTakenOn,'%d-%b-%Y %T') END as `Action TakenOn`, cep.ApproverRemarks as Approver Remarks,cep.CreatedOn as Created On");
        $this->db->from('ki_cash_expenses as cep');
        $this->db->join('ki_users as usr', 'usr.Username = cep.Username ');
        if ($From != '' ) {
           $this->db->where('cep.TrnDate >=', $From);
        } if ($To != '') {
            $this->db->where('cep.TrnDate <=', $To);
        }
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        } if ($User != '0') {
            $this->db->where('usr.Username', $User);
        }
        if ($ExpType != '0') {
            $this->db->where('cep.ExpenseType', $ExpType);
        }
        if ($Status != '0') {
            $this->db->where('cep.Status', $Status);
        }
        $this->db->order_by("cep.Id", "desc");
        $query = $this->db->get();
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
           
        } else {
           
                $data = $query->result();
                // var_dump($data);
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
        }
         return $result;
    }

    // SNT 12th Feb 2020 Function use to fetch pending cash List
    function FetchCashApproveRejectList($RoleType, $Username, $Status, $State, $District) {
        /*$this->db->select("usr.StateName, usr.DistrictName, usr.WarehouseName");
        $this->db->from('ki_users as usr');
        $this->db->where('usr.Username', $Username);
        $this->db->where('usr.IsActive', 1);
        $query = $this->db->get();
        if ($this->db->affected_rows() > 0) {
            //echo"hii";
            $data = $query->result();
           // var_dump($data);
           $StateName = $data[0]->StateName;
           $DistrictName = $data[0]->DistrictName;
            
          // echo $StateName;
          // echo $DistrictName;
           
            if($RoleType == ROL_WA || $RoleType == ROL_CTR ){
            $this->db->select('ctr.Id, ctr.TrnDate,,ctr.Username_From, ctr.RecipientAccountType, ctr.Username_To,  round(ctr.TrnAmount) as TrnAmount, ctr.Status, ctr.Comments, ctr.Username_Approver, ctr.ActionTakenOn, ctr.ApproverRemarks, ctr.CreatedOn, ctr.CreatedBy, ctr.ModifiedOn, ctr.ModifiedBy');
            $this->db->from('ki_cash_transfers as ctr');
                 $this->db->where('ctr.Username_To', $Username);
                 $this->db->where('ctr.Status', $Status);
                 //$this->db->where('ctr.Status!=','');
                 $this->db->order_by("ctr.TrnDate", "desc");
                
            }
            else{
              
            $this->db->select('ctr.Id, ctr.TrnDate,usrs.DistrictName, usrs.RoleType, usrs.StateName ,ctr.Username_From, ctr.RecipientAccountType, ctr.Username_To,  round(ctr.TrnAmount) as TrnAmount, ctr.Status, ctr.Comments, ctr.Username_Approver, ctr.ActionTakenOn, ctr.ApproverRemarks, ctr.CreatedOn, ctr.CreatedBy, ctr.ModifiedOn, ctr.ModifiedBy');
            $this->db->from('ki_cash_transfers as ctr');
            $this->db->join('ki_users as usrs', 'usrs.Username = ctr.Username_From');
               
                if($RoleType == ROL_DT)
                {
                    //$this->db->where('usrs.RoleType = 4,5');
                    $roles = array(ROL_WA, ROL_CTR);
                    $this->db->where_in('usrs.RoleType',$roles);
                    //$this->db->or_where('usrs.RoleType', ROL_CTR);
                }
                else if($RoleType == ROL_ST)
                {
                    $this->db->where('usrs.RoleType', ROL_DT);
                }
                else if($RoleType == ROL_SA)
                {
                    $this->db->where('usrs.RoleType', ROL_ST);
                }
                
                /*if($StateName != null || $StateName !=''){
                  //  echo $StateName;
                   // if('usrs.StateName' == $StateName){
                        $this->db->where('usrs.StateName', $StateName);
                    //}
                }
                if($DistrictName != null || $DistrictName !='' ){
                   //echo $DistrictName;
                    $this->db->where('usrs.DistrictName', $DistrictName);
                }*/
              /*  $this->db->where('ctr.RecipientAccountType!=', 'INTERNAL_USERS');
                $this->db->where('ctr.Status', $Status);
                $this->db->or_where('ctr.Username_To', $Username);
                $this->db->order_by("ctr.TrnDate", "desc");
                $this->db->where('ctr.Status', $Status);
                
                //$this->db->where()
            }
            
            */
         
           
        $this->db->select('ctr.Id, ctr.TrnDate,ctr.Username_From, ctr.RecipientAccountType, ctr.Username_To,  round(ctr.TrnAmount) as TrnAmount, ctr.Status, ctr.Comments, ctr.Username_Approver, ctr.ActionTakenOn, ctr.ApproverRemarks, ctr.CreatedOn, ctr.CreatedBy, ctr.ModifiedOn, ctr.ModifiedBy');
        $this->db->from('ki_cash_transfers as ctr');
        $this->db->join('ki_users as usrs', 'usrs.Username = ctr.Username_From');
        $this->db->where('usrs.IsActive', 1);
        if ($RoleType !=ROL_SA) {
            $this->db->where('ctr.Username_To', $Username);
        
        } else {

            // $this->db->join('ki_users as usrs', 'usrs.Username = ctr.Username_From');
            $this->db->where('ctr.Status', $Status);
            if ($State != '0') {
                $this->db->where('usrs.StateName', $State);
            }
            if ($District != '0') {
                $this->db->where('usrs.DistrictName', $District);
            }
            $this->db->where('ctr.RecipientAccountType!=', 'INTERNAL_USERS');
            $this->db->where('ctr.Status', $Status);
            $this->db->or_where('ctr.Username_To', $Username);
            $this->db->where('ctr.Status', $Status);
            if ($State != '0') {
                $this->db->where('usrs.StateName', $State);
            }
            if ($District != '0') {
                $this->db->where('usrs.DistrictName', $District);
            }
        }


            $this->db->order_by("ctr.CreatedOn", "desc");
            $this->db->where('ctr.Status', $Status);
            $query1 = $this->db->get();
            $error = $this->db->error();
            if (!$query1) {
                //var_dump($query);
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => $error
                );
            } else {
                $data = $query1->result();
                // var_dump($data);
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            }
            return $result;
       
    }
    
    //SNT 17th Feb 2020 Function use to update  pending cash transfer List
    function ApproveRejectCashTransfer($data, $Id, $Username_From, $Username_To, $TrnAmount, $Status, $CreatedBy) {
        $this->CommonModel->CurrentTime();
        $this->db->where('Id', $Id);
        $this->db->where('Status',strtolower(TRN_STATUS_PENDING));  //add status pending frm const
        $this->db->trans_begin();
        $query = $this->db->update('ki_cash_transfers', $data);
       
        //MIS_ENTRY", "TRANSFER_APPOROVE_TO", "EXPENSE","TRANSFER","TRANSFER_APPOROVE","EXPENSE_APPOROVE","TRANSFER_REJECTED_TO","EXPENSE_REJECTED_TO"
        $CashUpdateStatus = unserialize(CashUpdateStatus);
        if ($Status == TRN_STATUS_APPROVED) {
            
            if($Username_To == '' || $Username_To==null ){
               $CashReceivedData=  $this->UpdateUserCash($Username_From, $TrnAmount, $CashUpdateStatus[4], $CreatedBy);
            }
            else{
               $CashReceivedData =   $this->UpdateUserCash($Username_From, $TrnAmount, $CashUpdateStatus[4], $CreatedBy);
               if($CashReceivedData == true){
                   $this->UpdateUserCash($Username_To, $TrnAmount, $CashUpdateStatus[1], $CreatedBy);
               }
                 else{
                     ///echo"3";
                     $result = array(
                        'status' => StatusFailed,
                        'message' => 'MsgError',
                        'data' => ''
                     );
                 }
               
            }
            
        } else {
            $CashReceivedData = $this->UpdateUserCash($Username_From, $TrnAmount, $CashUpdateStatus[6], $CreatedBy);
        }
        if ($CashReceivedData == true) {
            $this->db->trans_complete();
            $error = $this->db->error();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            } else {
                $this->db->trans_commit();
                if (!$query) {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                } else {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => MsgCommon,
                        'data' => $data
                    );
                }
            }
        }else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
        }
        return $result;
    }

    //KCJ 21 th Feb 2020 Function use to fetch data for user cash approve and cash unapprove
    function FetchCashForUser($Username) {
        $this->db->where('Username', $Username);
        $this->db->where('IsActive', 1);
        $this->db->select('ifnull(CashInHand,0) as Approvecash, ifnull(CashUnApproved,0) as Unapprovecash');
        $this->db->from('ki_user_cash');
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                //echo"hii";
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    //KCJ 21th Feb 2020 Function use to calculate cash of user and update within table (User_cash)
    //ActionType : "MIS_ENTRY", "TRANSFER_APPOROVE_TO", "EXPENSE","TRANSFER","TRANSFER_APPOROVE","EXPENSE_APPOROVE","TRANSFER_REJECTED_TO","EXPENSE_REJECTED_TO"
    function UpdateUserCash($Username, $Cash_ToAdd, $ActionType, $ModifiedBy) {
        $this->CommonModel->CurrentTime();
        try {
            $IsDataModified = 0;
           // var_dump($Username) ;
            $CashUpdateStatus = unserialize(CashUpdateStatus);
            //Get cash data for existing user       
            $CashReceivedData = $this->CashModel->FetchCashForUser($Username);
            $Status = $CashReceivedData["status"];
            $Data = $CashReceivedData["data"];
            //print_r($Data);
            //cash update status => "MIS_ENTRY", "TRANSFER_APPOROVE_TO", "EXPENSE","TRANSFER","TRANSFER_APPOROVE","EXPENSE_APPOROVE","TRANSFER_REJECTED_TO","EXPENSE_REJECTED_TO"
            if ($Status == 1) {
                $CashInHand = $Data[0]->Approvecash;
                $CashUnApprove = $Data[0]->Unapprovecash;
                
                if ($ActionType != "") {
                    if ($ActionType == $CashUpdateStatus[0] || $ActionType == $CashUpdateStatus[1]) {

                        $CashInHand = $CashInHand + $Cash_ToAdd;
                        $IsDataModified = 1;
                    } else if ($ActionType == $CashUpdateStatus[2] || $ActionType == $CashUpdateStatus[3]) {

                        $CashUnApprove = $CashUnApprove + $Cash_ToAdd;
                        $IsDataModified = 1;
                    } else if ($ActionType == $CashUpdateStatus[4] || $ActionType == $CashUpdateStatus[5]) {

                        $CashInHand = $CashInHand - $Cash_ToAdd;
                        $CashUnApprove = $CashUnApprove - $Cash_ToAdd;
                        $IsDataModified = 1;
                    } else if ($ActionType == $CashUpdateStatus[6] || $ActionType == $CashUpdateStatus[7]) {

                        $CashUnApprove = $CashUnApprove - $Cash_ToAdd;
                        $IsDataModified = 1;
                    }

                    if ($IsDataModified == 1) {

                        $data_update = array(
                            "IsActive" => 0,
                            "ModifiedOn" => date('Y-m-d H:i:s'),
                            "ModifiedBy" => $ModifiedBy
                        );
                        $this->db->where('Username', $Username);
                        $this->db->where('IsActive', 1);
                        $query = $this->db->update('ki_user_cash', $data_update);

                        $data = array(
                            "Username" => $Username,
                            "CashInHand" => $CashInHand,
                            "CashUnApproved" => $CashUnApprove,
                            "CreatedBy" => $Username,
                        );

                        $query1 = $this->db->insert('ki_user_cash', $data);
                        $error = $this->db->error();
                        if (!$query) {
                            $result = array(
                                'status' => StatusFailed,
                                'message' => MsgError,
                                'data' => $error
                            );
                            return $result;
                        } else {
                            //echo"hii";
                            $result = array(
                                'status' => StatusSuccess,
                                'message' => MsgCommon,
                                'data' => $data
                            );
                            return $result;
                        }
                    }
                    else{
                        return false;
                    }
                } else {
                    //echo"hii";
                    return false;
                }
            } else {
                
                //echo"hiii";
                $db = array(
                    'Username' => $Username,
                    'CashInHand' => $Cash_ToAdd,
                    'CreatedBy' => $ModifiedBy
                );

                $sqli = $this->db->insert('ki_user_cash', $db);
                if ($sqli) {
                    
                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => MsgCommon,
                            'data' => $db
                        );
                    } else {
                        //echo"hiii";
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => MsgCommon,
                            'data' => ''
                        );
                    }
                }
                else{
                    //echo"hii";
                     $result = array(
                            'status' => StatusSuccess,
                            'message' => MsgCommon,
                            'data' => ''
                    );
                }
                
                 return $result;
            }
        } catch (Exception $e) {   //echo"hii"; 
            return false;
        }
    }

    
    //SNT 17th Feb 2020 Function use to get pending List of expense Approval for specific user
    function FetchExpenseApproveRejectList($RoleType, $Username, $Status,$State, $District) {
//        $this->db->select("usr.StateName, usr.DistrictName, usr.WarehouseName");
//        $this->db->from('ki_users as usr');
//        $this->db->where('usr.Username', $Username);
//        $this->db->where('usr.IsActive', 1);
//        
//        //$where = "usr.Username= $Username AND usr.IsActive = '1'";
//        //$this->db->where($where);
//        $query = $this->db->get();
//        if ($this->db->affected_rows() > 0) {
//            //echo"hii";
//            $data = $query->result();
//           // var_dump($data);
//            $StateName = $data[0]->StateName;
//            $DistrictName = $data[0]->DistrictName;
            
            $this->db->select('cep.Id, usr.DistrictName, usr.RoleType, usr.StateName, cep.Username, cep.TrnDate, cep.ExpenseType ,round(cep.AmountSpent) as AmountSpent, cep.Comments, cep.Status, cep.ActionTakenOn, cep.ApproverRemarks, cep.CreatedBy, cep.CreatedOn');
            $this->db->from('ki_cash_expenses as cep');
            $this->db->join('ki_users as usr', 'usr.Username = cep.Username');

           /* if ($RoleType == ROL_SA) {
                $this->db->where('cep.Status', $Status);
                $this->db->where('usr.RoleType', ROL_ST);
                $this->db->or_where('usr.RoleType', ROL_SA);
            } else if ($RoleType == ROL_ST) {
                $this->db->where('usr.RoleType', ROL_DT);
                $this->db->where('usr.StateName', $StateName);
                //$where = "usr.RoleType = ROL_DT  AND usr.StateName = $StateName";
                //$this->db->where($where);
            } else if ($RoleType == ROL_DT) {
                $this->db->where('usr.RoleType', ROL_WA);
                $this->db->or_where('usr.RoleType', ROL_CTR);
                $this->db->where('usr.StateName', $StateName);
                $this->db->where('usr.DistrictName',  $DistrictName);
                ////$where = "(usr.RoleType = 'ROL_WA' OR usr.RoleType = 'ROL_CTR')  AND usr.StateName = $StateName AND usr.DistrictName = $DistrictName";
               // $this->db->where($where);
            }*/
            
            if ($RoleType == ROL_SA) {
                $this->db->where('cep.Status', $Status);
                $roles = array(ROL_WA, ROL_CTR, ROL_ST, ROL_DT, ROL_SA);
                $this->db->where_in('usr.RoleType',$roles);
            }
            else{}
            
             if($State !='0' && $State!=''){
               $this->db->where('usr.StateName', $State);
              }
            if($District !='0'){
               $this->db->where('usr.DistrictName', $District);
            }
            $this->db->where('cep.Status', $Status);
            $this->db->order_by("cep.CreatedOn", "desc");
            
            $query1 = $this->db->get();
            $error = $this->db->error();
            if (!$query1) {
                var_dump($query1);
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => $error
                );
            } else {
                $data = $query1->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            }
            return $result;
//        }
//        else{
//            return false;
//        }

    }
    
    //SNT 17-03-2020 Function use to update  pending expense List
    function ApproveRejectExpense($data, $Id, $Username, $AmountSpent) { 
        $this->CommonModel->CurrentTime();
        $this->db->where('Id', $Id);
        $this->db->where('Status',strtolower(TRN_STATUS_PENDING));
        $query = $this->db->update('ki_cash_expenses', $data);
        $this->db->trans_begin();
         //"EXPENSE_APPOROVE","EXPENSE_REJECTED_TO"
        $CashUpdateStatus = unserialize(CashUpdateStatus);
        
       // var_dump($data);
        //var_dump($data[0]['Status']) ;
       // var_dump($data["Status"]);
        if ($data['Status'] == TRN_STATUS_APPROVED) {
            $CashReceivedData = $this->CashModel->UpdateUserCash($Username, $AmountSpent, $CashUpdateStatus[5], $data['ModifiedBy']);
        } else {
            $CashReceivedData = $this->CashModel->UpdateUserCash($Username, $AmountSpent, $CashUpdateStatus[7], $data['ModifiedBy']);
        }
       
        if ($CashReceivedData == true) {
             $this->db->trans_complete();
             $error = $this->db->error();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            } else {
                $this->db->trans_commit();
                if (!$query) {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                } else {
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => MsgCommon,
                        'data' => $data
                    );
                }
            }
        } else {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => ''
            );
        }
        return $result;
    }

}
