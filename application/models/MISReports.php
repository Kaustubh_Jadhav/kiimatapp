<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MISReports
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class MISReports extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->model('CommonModel');
    }
    
    
   /* function FetchStock($State, $District){
        $this->db->select('usr.StateName, usr.DistrictName, cs.Username,  cs.StockType, (cs.Qty -IFNULL(cs.QtyUnApproved,0)) As Total', FALSE);
        $this->db->from('ki_counter_stock as cs');
        $this->db->join('ki_users as usr', 'usr.Username = cs.Username');
        $this->db->where('cs.IsActive',1);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }
        $query1 =  $this->db->get_compiled_select(); 
        
        
        $this->db->select('usr.StateName, usr.DistrictName, ws.WarehouseName, ws.StockType, (ws.Qty -IFNULL(ws.QtyUnApproved,0)) As Total', FALSE);
        $this->db->from('ki_warehouse_stock as ws');
        $this->db->join('ki_users as usr', 'usr.WarehouseName = ws.WarehouseName');
        $this->db->where('ws.IsActive',1);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }
        $query2 = $this->db->get_compiled_select();
        $query = $this->db->query($query1.' UNION '.$query2);
        
        //$query = $this->db->get();
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
        
    }
    
     function FetchStock( $Appliance, $State, $District){
        $this->db->select('usr.StateName, usr.DistrictName, cs.Username,  cs.StockType, sum(DISTINCT
        case when  cs.StockType = "FRESH" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As FRESH,
        sum(DISTINCT 
        case when  cs.StockType = "DEFECTIVE" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
        $this->db->from('ki_counter_stock as cs');
        $this->db->join('ki_users as usr', 'usr.Username = cs.Username');
        $this->db->where('cs.IsActive',1);
        $this->db->where('usr.IsActive',1);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }if ($Appliance != '0') {
            $this->db->where('cs.ApplianceName', $Appliance);
        }
        $this->db->group_by("usr.Username");
        $query1 =  $this->db->get_compiled_select(); 
        
        
        $this->db->select('usr.StateName, usr.DistrictName, ws.WarehouseName, ws.StockType,  sum(DISTINCT
        case when  ws.StockType = "FRESH" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As FRESH,
        sum(DISTINCT
        case when  ws.StockType = "DEFECTIVE" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
        $this->db->from('ki_warehouse_stock as ws');
        $this->db->join('ki_users as usr', 'usr.WarehouseName = ws.WarehouseName');
        $this->db->where('ws.IsActive',1);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }if ($Appliance != '0') {
            $this->db->where('ws.ApplianceName', $Appliance);
        }
        $this->db->group_by("usr.WarehouseName");
        $query2 = $this->db->get_compiled_select();
        $query = $this->db->query($query1.' UNION '.$query2);
        
        //$query = $this->db->get();
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
        
    }*/
    
    
//     function FetchStock($Appliance, $State, $District, $Username){
//        $this->db->select('cs.ApplianceName, usr.StateName, usr.DistrictName, cs.Username,  cs.StockType, sum(DISTINCT
//        case when  cs.StockType = "FRESH" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As FRESH,
//        sum(DISTINCT 
//        case when  cs.StockType = "DEFECTIVE" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
//        $this->db->from('ki_counter_stock as cs');
//        $this->db->join('ki_users as usr', 'usr.Username = cs.Username and cs.IsActive = 1','left');
////        $this->db->where('cs.IsActive',1);
//        $this->db->where('usr.IsActive',1);
//        if ($State != '0') {
//            $this->db->where('usr.StateName', $State);
//        } if ($District != '0') {
//            $this->db->where('usr.DistrictName', $District);
//        }if ($Appliance != '0') {
//            $this->db->where('cs.ApplianceName', $Appliance);
//        }
//        if ($Username != '0') {
//            $this->db->where('cs.Username', $Username);
//        }
////        $this->db->group_by("usr.Username");
//        $this->db->group_by( array('cs.ApplianceName','usr.Username'));
//        $query1 =  $this->db->get_compiled_select(); 
//        
//        
//        $this->db->select('ws.ApplianceName,dr.StateName, dr.Name_ as DistrictName, w.Name_ as Username, ws.StockType,  sum(DISTINCT
//        case when  ws.StockType = "FRESH" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As FRESH,
//        sum(DISTINCT
//        case when  ws.StockType = "DEFECTIVE" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
//        $this->db->from('ki_warehouse_stock as ws');
//        $this->db->join(' ki_warehouses as w', 'w.Name_ = ws.WarehouseName');
//        $this->db->join(' ki_districts as dr', 'dr.Name_ = w.DistrictName');
//        $this->db->where('w.IsActive',1);
//        $this->db->where('ws.IsActive',1);
//        if ($State != '0') {
//            $this->db->where('dr.StateName', $State);
//        } if ($District != '0') {
//            $this->db->where('dr.Name_', $District);
//        }if ($Appliance != '0') {
//            $this->db->where('ws.ApplianceName', $Appliance);
//        }
//        if ($Username != '0') {
//            $this->db->where('ws.WarehouseName', $Username);
//        }
////        $this->db->group_by("w.Name_");
//        $this->db->group_by( array('ws.ApplianceName','w.Name_'));
//        $query2 = $this->db->get_compiled_select();
//        
//        // logic to display data 
//        
////        if($_SESSION['RoleType']== ROL_CTR){
////            $query = $this->db->query($query1);
////        }
////        else if($_SESSION['RoleType']== ROL_WA){
////             $query = $this->db->query($query2);
////        }else{
////            
////            echo"hii";
//             $query = $this->db->query($query1.' UNION '.$query2);
////        }
//       
//        
//        //$query = $this->db->get();
//        $error = $this->db->error();
//        //var_dump($query);
//        if (!$query) {
//            //var_dump($query);
//            $result = array(
//                'status' => StatusFailed,
//                'message' => MsgError,
//                'data' => $error
//            );
//        } else {
//            $data = $query->result();
//            // var_dump($data);
//            $result = array(
//                'status' => StatusSuccess,
//                'message' => MsgCommon,
//                'data' => $data
//            );
//        }
//        return $result;
//        
//    }
//    
    
    
    
    function FetchStock($Appliance, $State, $District, $Username){
        $this->db->select('app.Name_, usr.StateName, usr.DistrictName, usr.Username,  cs.StockType, sum(DISTINCT
        case when  cs.StockType = "FRESH" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As FRESH,
        sum(DISTINCT 
        case when  cs.StockType = "DEFECTIVE" then (cs.Qty -IFNULL(cs.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
        $this->db->from('ki_appliances as app');
        $this->db->join('ki_users as usr', '1=1 and app.IsActive=1 and usr.IsActive=1');
        $this->db->join('ki_counter_stock cs', ' app.Name_= cs.ApplianceName and usr.Username= ifnull(cs.Username,usr.Username)  and cs.IsActive=1','left',FALSE);
//        $this->db->where('cs.IsActive',1);
        $this->db->where('usr.RoleType',ROL_CTR);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }if ($Appliance != '0') {
            $this->db->where('cs.ApplianceName', $Appliance);
        }
        if ($Username != '0') {
            $this->db->where('usr.Username', $Username);
        }
          $this->db->group_by(array("app.Name_","usr.Username"));
          $query1 =  $this->db->get_compiled_select(); 
        
        
        $this->db->select('app.Name_,dr.StateName,w.DistrictName as DistrictName, w.Name_ as Username, ws.StockType,  sum(DISTINCT
        case when  ws.StockType = "FRESH" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As FRESH,
        sum(DISTINCT
        case when  ws.StockType = "DEFECTIVE" then (ws.Qty -IFNULL(ws.QtyUnApproved,0)) else 0 end )As DEFECTIVE', FALSE);
        $this->db->from('ki_appliances as app');
        $this->db->join(' ki_warehouses as w', '1=1 and app.IsActive=1 and w.IsActive=1');
        $this->db->join('ki_warehouse_stock as ws', 'app.Name_= ws.ApplianceName and w.Name_= ifnull(ws.WarehouseName,w.Name_)  and ws.IsActive=1','left',FALSE);
        $this->db->join(' ki_districts as dr', 'dr.Name_ = w.DistrictName and dr.IsActive=1');
//        $this->db->where('w.IsActive',1);
//        $this->db->where('ws.IsActive',1);
        if ($State != '0') {
            $this->db->where('dr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('dr.Name_', $District);
        }if ($Appliance != '0') {
            $this->db->where('ws.ApplianceName', $Appliance);
        }
        if ($Username != '0') {
            $this->db->where('w.Name_', $Username);
        }
//        $this->db->group_by("w.Name_");
        $this->db->group_by( array('app.Name_','w.Name_'));
        $query2 = $this->db->get_compiled_select();
        
        // logic to display data 
        
        if ($_SESSION['RoleType'] == ROL_CTR) {
            $query = $this->db->query($query1);
            
        } else if ($_SESSION['RoleType'] == ROL_WA) {
            $query = $this->db->query($query2);
        } else {
            $query = $this->db->query($query1 . ' UNION ' . $query2);
        }


        //$query = $this->db->get();
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
        
    }
    
     function FetchSaleSummary($Appliance, $State, $District, $From, $To){
        $this->db->select('sd.ApplianceName as Appliance, usr.StateName, usr.DistrictName,sd.ApplianceName,usr.Username, DATE_FORMAT(sd.CreatedOn,"%d-%m-%Y") as `Date`, sum(IFNULL(sd.QtyDistributed,0)) as QtyDistributed , sum(IFNULL(sd.QtyReplaced,0)) as QtyReplaced, sum(IFNULL(sd.QtyBD,0)) as QtyBD', FALSE);
        $this->db->from('ki_stock_distribution as sd');
        $this->db->join('ki_users as usr', 'usr.Username = sd.Username');
        $this->db->where('usr.IsActive',1);
         if ($From != '' ) {
           $this->db->where('sd.EntryDate >=', $From);
        } if ($To != '') {
            $this->db->where('sd.EntryDate <=', $To);
        }
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }
        if ($Appliance != '0') {
            $this->db->where('sd.ApplianceName', $Appliance);
        }
         $this->db->group_by(array('sd.ApplianceName','sd.Username','Date'));
//        $this->db->group_by(array('sd.Username','Date'));
        $query =  $this->db->get(); 
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
        
    }
    
    
    
    //testing for datewise pending
//    function FetchSaleSummary($Appliance, $State, $District, $From, $To){
//        $this->db->select('usr.StateName, usr.DistrictName,sd.ApplianceName,sd.CreatedOn, sd.Username, sd.EntryDate, IFNULL(sd.QtyDistributed,0) as QtyDistributed , IFNULL(sd.QtyReplaced,0) as QtyReplaced, IFNULL(sd.QtyBD,0) as QtyBD', FALSE);
//        $this->db->from('ki_stock_distribution as sd');
//        $this->db->join('ki_users as usr', 'usr.Username = sd.Username');
//        $this->db->where('usr.IsActive',1);
//         if ($From != '' ) {
//           $this->db->where('sd.EntryDate >=', $From);
//        } if ($To != '') {
//            $this->db->where('sd.EntryDate <=', $To);
//        }
//        if ($State != '0') {
//            $this->db->where('usr.StateName', $State);
//        } if ($District != '0') {
//            $this->db->where('usr.DistrictName', $District);
//        }
//        if ($Appliance != '0') {
//            $this->db->where('sd.ApplianceName', $Appliance);
//        }
//        $this->db->group_by("sd.Username");
//        $query =  $this->db->get(); 
//        $error = $this->db->error();
//        //var_dump($query);
//        if (!$query) {
//            //var_dump($query);
//            $result = array(
//                'status' => StatusFailed,
//                'message' => MsgError,
//                'data' => $error
//            );
//        } else {
//            $data = $query->result();
//            // var_dump($data);
//            $result = array(
//                'status' => StatusSuccess,
//                'message' => MsgCommon,
//                'data' => $data
//            );
//        }
//        return $result;
//        
//    }
    
    
//     function FetchCash($State, $District, $Username){
//        $this->db->select('usr.StateName, usr.DistrictName,uc.Username,round(IFNULL(uc.CashInHand,0) - IFNULL(uc.CashUnApproved,0)) As Available, round(uc.CashUnApproved) as Pending, round(uc.CashInHand) as Total');
//        $this->db->from('ki_user_cash as uc');
//        $this->db->join('ki_users as usr', 'usr.Username = uc.Username');
//        $this->db->where('usr.IsActive',1);
//        $this->db->where('uc.IsActive',1);
//        $this->db->where('usr.RoleType',ROL_CTR);
//        if ($State != '0') {
//            $this->db->where('usr.StateName', $State);
//        } if ($District != '0') {
//            $this->db->where('usr.DistrictName', $District);
//        }
//        if ($Username != '0') {
//            $this->db->where('usr.Username', $Username);
//        }
//       
//        $query =  $this->db->get(); 
//        $error = $this->db->error();
//        //var_dump($query);
//        if (!$query) {
//            //var_dump($query);
//            $result = array(
//                'status' => StatusFailed,
//                'message' => MsgError,
//                'data' => $error
//            );
//        } else {
//            $data = $query->result();
//            // var_dump($data);
//            $result = array(
//                'status' => StatusSuccess,
//                'message' => MsgCommon,
//                'data' => $data
//            );
//        }
//        return $result;
//        
//    }
//    
    
     function FetchCash($State, $District, $Username){
        $this->db->select('usr.StateName, usr.DistrictName,usr.Username,round(IFNULL(uc.CashInHand,0) - IFNULL(uc.CashUnApproved,0)) As Available, IFNULL(round(uc.CashUnApproved),0) as Pending, IFNULL(round(uc.CashInHand),0) as Total');
        $this->db->from('ki_users as usr');
        $this->db->join('ki_user_cash as uc', 'usr.Username = uc.Username and uc.IsActive=1','left',FALSE);
        $this->db->where('usr.IsActive',1);
//        $this->db->where('uc.IsActive',1);
        $this->db->where('usr.RoleType',ROL_CTR);
        if ($State != '0') {
            $this->db->where('usr.StateName', $State);
        } if ($District != '0') {
            $this->db->where('usr.DistrictName', $District);
        }
        if ($Username != '0') {
            $this->db->where('usr.Username', $Username);
        }
       
        $query =  $this->db->get(); 
        $error = $this->db->error();
        //var_dump($query);
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            $data = $query->result();
            // var_dump($data);
            $result = array(
                'status' => StatusSuccess,
                'message' => MsgCommon,
                'data' => $data
            );
        }
        return $result;
        
    }
    
    
    
    
//    function FetchGraph() {
//        $this->db->select("DATE_FORMAT(TrnDate,'%Y-%m') as Month, count(1) as Request,
//        sum(AmountSpent) as Expense,sum(AmountSpent) * 100 /  (SELECT sum(AmountSpent) AS s FROM ki_cash_expenses) AS `percent`",false);
//        $this->db->from('ki_cash_expenses');
//        $this->db->where('TrnDate < Now() and TrnDate > DATE_ADD(Now(), INTERVAL- 6 MONTH)');
//        $this->db->group_by('Month(TrnDate)');
//        $this->db->order_by("YEAR(TrnDate)", "asc");
//        $this->db->order_by("Month", "asc");
//        $query = $this->db->get();
//        $error = $this->db->error();
//        if (!$query) {
//            //var_dump($query);
//            $result = array(
//                'status' => StatusFailed,
//                'message' => MsgError,
//                'data' => $error
//            );
//        } else {
//            if ($this->db->affected_rows() > 0) {
//                $data = $query->result();
//                $result = array(
//                    'status' => StatusSuccess,
//                    'message' => MsgCommon,
//                    'data' => $data
//                );
//            } else {
//                $result = array(
//                    'status' => StatusFailed,
//                    'message' => MsgError,
//                    'data' => ''
//                );
//            }
//        }
//         return $result;
//    }

}
