<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommonModel
 *
 * @author CxB012
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonModel extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    // SNT 14th Feb 2020 model func to get applianceList 
    function FetchApplianceList() {
        // $this->db->select("Id, Name_, RatePerPc, CreatedOn, CreatedBy, 
        //  CASE WHEN ModifiedOn IS NULL THEN '' else ModifiedOn END as ModifiedOn ,
        //   ModifiedBy",false);
        $this->CurrentTime();
        //$this->db->select('Id, Name_, RatePerPc, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy');
         $this->db->select("Id, Name_, round(RatePerPc) as RatePerPc, CreatedOn, CreatedBy, 
          CASE WHEN ModifiedOn IS NULL THEN '' else DATE_FORMAT(ModifiedOn,'%d-%b-%Y %H:%I:%S') END as ModifiedOn ,
          ModifiedBy",false);
        $this->db->order_by("Id", "asc");
        $query = $this->db->get_where('ki_appliances', array('IsActive' => 1));
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
         return $result;
    }

    // SNT 14th Feb 2020 Function use to to get user List 
//    function FetchUserList($RoleType, $Username) {
//
//        $this->db->select('usr.Id, usr.RoleType, usr.Name_, usr.Username, usr.WarehouseName, usr.CreatedOn, usr.CreatedBy, usr.ModifiedOn, usr.ModifiedBy');
//        $this->db->order_by("usr.RoleType", "desc");
//        if ($RoleType != '') {
//            //$this->db->where_not_in('Username', "ctr0001");
//            // $this->db->where('Username !=' ,$Username);
//            if($RoleType =='WAREHOUSEADMIN'){
//                $this->db->distinct();
//                $this->db->from('ki_users as usr');
//                $this->db->join('ki_warehouses as ws', 'ws.Name_ = usr.WarehouseName ');
//                $this->db->group_by(" ws.Name_");
//                $query = $this->db->get();
//                //var_dump($query);
//            }
//            else{
//                 $query = $this->db->get_where('ki_users as usr', array('usr.IsActive' => 1, 'usr.RoleType' => $RoleType));
//            }
//           
//        } else {
//            //$this->db->where('Username !=' ,$Username);
//            $query = $this->db->get_where('ki_users as usr', array('usr.IsActive' => 1));
//        }
//
//        $error = $this->db->error();
//        if (!$query) {
//            //var_dump($query);
//            $result = array(
//                'status' => StatusFailed,
//                'message' => MsgError,
//                'data' => $error
//            );
//        } else {
//            if ($this->db->affected_rows() > 0) {
//                $data = $query->result();
//                $result = array(
//                    'status' => StatusSuccess,
//                    'message' => MsgCommon,
//                    'data' => $data
//                );
//            } else {
//                $result = array(
//                    'status' => StatusFailed,
//                    'message' => MsgError,
//                    'data' => ''
//                );
//            }
//        }
//        return $result;
//    }

    // KCJ 02th March 2020 Function use to to get State List 
    function FetchStateList() {

        $this->db->select('Id, Name_, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy');
        $this->db->order_by("Name_", "asc");
        $query = $this->db->get_where('ki_states', array('IsActive' => 1));
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();

                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    // KCJ 02th March 2020 Function use to to get District List 
    function FetchDistrictList() {
        $this->db->select('Id, Name_,StateName, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy');
        $this->db->order_by("Name_", "asc");
        $query = $this->db->get_where('ki_districts', array('IsActive' => 1));
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
           
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
         return $result;
    }

    // SNT 05-03-2020 Function use to add/modify appliances   
   function SaveApplianceDetails($Id, $Name_, $RatePerPc, $ModifiedBy) {
       $this->CurrentTime();
            if ($Id > 0) {
             
            $this->db->select('Name_');
            $que = $this->db->get_where('ki_appliances', array('Name_' => $Name_, 'Id!=' => $Id, 'IsActive' => 1));

            if ($que) {
                if ($this->db->affected_rows() > 0) {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => "Appliance already exists.",
                        'data' => ''
                    );
                } else {
                    $data = array(
                        'Name_' => $Name_,
                        'RatePerPc' => $RatePerPc,
                        'ModifiedOn' => date('Y-m-d H:i:s'),
                        'ModifiedBy' => $ModifiedBy,
                    );
                    $this->db->update('ki_appliances', $data, array('Id' => $Id));
                    $result = array(
                        'status' => StatusSuccess,
                        'message' => 'Appliance details updated successfully.',
                        'data' => $data
                    );
                }
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => $error
                );
            }
            //return $result;
        } else {
           
            $this->db->select('Name_');
            $que = $this->db->get_where('ki_appliances', array('Name_' => $Name_, 'IsActive' => 1));

            if ($que) {
                //echo"hello";
                if ($this->db->affected_rows() > 0) {
                   
                    $result = array(
                        'status' => StatusFailed,
                        'message' => "Appliance already exists.",
                        'data' => ''
                    );
                } else {
                    
                    $query = $this->db->get('ki_appliances');
                    //print_r($query->num_rows());
                    if ($query->num_rows() > 10) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => 'Not inserted',
                            'data' => ''
                        );
                    } else {
                        $data = array(
                            'Name_' => $Name_,
                            'RatePerPc' => $RatePerPc,
                            'CreatedBy' => $ModifiedBy,
                        );

                        $this->db->insert('ki_appliances', $data);
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => RecordInsertion,
                            'data' => $data
                        );
                    }
                }
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => $error
                );
            }
            //return $result;
        }
        return $result;
    }

    // SNT 6th march 2020 Function use to Fetch Warehouse List
    function FetchWarehouseList($State, $District) {
        $this->db->select("kw.Id, kw.DistrictName, kw.Name_, kw.Description, kw.CreatedOn, kw.CreatedBy, CASE WHEN kw.ModifiedOn IS NULL THEN '' else DATE_FORMAT(kw.ModifiedOn,'%d-%b-%Y %H:%I:%S') END as ModifiedOn, kw.ModifiedBy");
        $this->db->from('ki_warehouses as kw');
        $this->db->join('ki_districts  as kd', 'kw.DistrictName = kd.Name_ ');
        if ($State != '0') {
            $this->db->where('kd.StateName', $State);
        }
        if ($District != '0') {
            $this->db->where('kw.DistrictName', $District);
        } 
        $this->db->where('kw.IsActive', 1);
        $query = $this->db->get();
        $error = $this->db->error();
        if (!$query) {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    // SNT 06-03-2020  Function use to add/modify warehouse
    function SaveWarehouseDetails($Id, $DistrictName, $Name_, $Description ,$ModifiedBy){
      $this->CurrentTime();
            if ($Id > 0) {
                $this->db->select('Name_');
                $que = $this->db->get_where('ki_warehouses', array('Name_' => $Name_, 'Id!=' => $Id, 'IsActive'=>1));
                if ($que) {

                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => "Warehouse already exists.",
                            'data' => ''
                        );
                    } else {

                        $data = array(
                            'Name_' => $Name_,
                            'DistrictName' => $DistrictName,
                            'Description' => $Description,
                            'ModifiedOn' => date('Y-m-d H:i:s'),
                            'ModifiedBy' => $ModifiedBy,
                        );
                        $this->db->update('ki_warehouses', $data, array('Id' => $Id));
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => 'Warehouse details updated successfully.',
                            'data' => ''
                        );
                    }
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                }
               
            } 
            
            else {
                $this->db->select('Name_');
                $que = $this->db->get_where('ki_warehouses', array('Name_' => $Name_, 'IsActive' => 1));

                if ($que) {
                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => "Warehouse already exists.",
                            'data' => ''
                        );
                    } else {
                        
                         $data = array(
                            'DistrictName' => $DistrictName,
                            'Name_' => $Name_,
                            'Description' => $Description,
                            'CreatedBy' => $ModifiedBy,
                         );
                        $this->db->insert('ki_warehouses', $data);
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => RecordInsertion,
                            'data' => $data
                        );
                    }
                    
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                }
               
            }
        return $result;
    }

    // SNT 06-03-2020 Function use to add/modify Brand
    
    function UpdateBrandDetails($Id, $ApplianceName, $Name_, $ModifiedBy){
           $this->CurrentTime();
            if ($Id > 0) {

                $this->db->select('Name_');
                $que = $this->db->get_where('ki_brands', array('Name_' => $Name_, 'ApplianceName' => $ApplianceName, "IsActive"=>1));

                if ($que) {
                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => "Brand already exists for this appliance.",
                            'data' => ''
                        );
                    } else {
                        $data = array(
                            'Name_' => $Name_,
                            'ApplianceName' => $ApplianceName,
                            'ModifiedOn' => date('Y-m-d H:i:s'),
                            'ModifiedBy' => $ModifiedBy,
                        );
                        $this->db->update('ki_brands', $data, array('Id' => $Id));
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => 'Brand details updated successfully.',
                            'data' => $data
                        );
                    }
                  
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                    
                }
            } else {
                $this->db->select('Name_');
                $que = $this->db->get_where('ki_brands', array('Name_' => $Name_, 'ApplianceName' => $ApplianceName, "IsActive" => 1));

                if ($que) {

                    if ($this->db->affected_rows() > 0) {
                        $result = array(
                            'status' => StatusFailed,
                            'message' => "Brand is already exists for this appliance.",
                            'data' => ''
                        );
                    } else {

                        $data = array(
                            'Name_' => $Name_,
                            'ApplianceName' => $ApplianceName,
                            'CreatedBy' => $ModifiedBy,
                        );
                        
                        $this->db->insert('ki_brands', $data);
                        $result = array(
                            'status' => StatusSuccess,
                            'message' => RecordInsertion,
                            'data' => $data
                        );
                    }

                   
                } else {
                    $result = array(
                        'status' => StatusFailed,
                        'message' => MsgError,
                        'data' => $error
                    );
                   
                }
            }
       return $result;
    }
    
    // SNT 14th Feb 2020 Function use to to get Brand List 
    function FetchBrandList($ApplianceName) {
        
        $this->db->select("Id, Name_, ApplianceName, CreatedOn, CreatedBy,  CASE WHEN ModifiedOn IS NULL THEN '' else DATE_FORMAT(ModifiedOn,'%d-%M-%Y %H:%I:%S') END as ModifiedOn, ModifiedBy");
        $this->db->order_by("ApplianceName", "asc");
        $this->db->order_by("CreatedOn", "asc");
        if ($ApplianceName != '')
            $query = $this->db->get_where('ki_brands', array('IsActive' => 1, 'ApplianceName' => $ApplianceName));
        else
            $query = $this->db->get_where('ki_brands', array('IsActive' => 1));
        $error = $this->db->error();
        if (!$query) {
            //var_dump($query);
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
           
        } else {
            
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();

                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
               
            } else {
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => ''
                );
            }
        }
         return $result;
    }
    
    
    function CurrentTime(){
        date_default_timezone_set('Asia/Kolkata');
    }
    
    function fetchwhdata($refname) {
        $this->db->select('DistrictName, Name_, Description, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy');
        $this->db->order_by("Id", "asc");
        $query = $this->db->get_where('ki_warehouses', array('IsActive' => 1, 'Name_' => $refname));
        $error = $this->db->error();
        if (!$query) {
            $result = array(
                'status' => StatusFailed,
                'message' => MsgError,
                'data' => $error
            );
        } else {
            if ($this->db->affected_rows() > 0) {
                $data = $query->result();
                $result = array(
                    'status' => StatusSuccess,
                    'message' => MsgCommon,
                    'data' => $data
                );
            } else {
                $result = array(
                    'status' => StatusFailed,
                    'message' => MsgError,
                    'data' => ''
                );
            }
        }
        return $result;
    }

    /* SELECT username, CASE WHEN ModifiedOn IS NULL THEN CreatedOn else ModifiedOn END as m_dt
          FROM ki_users
          ;*/
 }

         