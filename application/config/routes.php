<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'UsersController/LoadLoginView';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/* $route['Insert-ExpenseEntry'] =  'ExpenseController/InsertExpenseEntry';
  $route['Display-ExpenseEntry'] = 'ExpenseController/DisplayExpenseEntry'; */


//Common routes
$route['fetchApplianceList']['get'] = 'CommonController/FetchApplianceList';
//$route['fetchUserList']['POST'] = 'CommonController/FetchUserList';
$route['fetchBrandList']['POST'] = 'CommonController/FetchBrandList';
$route['fetchStateList']['POST'] = 'CommonController/FetchStateList';
$route['fetchDistrictList']['POST'] = 'CommonController/FetchDistrictList';
$route['fetchWarehouseList']['POST'] = 'CommonController/FetchWarehouseList';
$route['appliances']['get'] = 'CommonController/ApplianceView';
$route['brands']['get'] = 'CommonController/BrandView';
$route['fetchWhData']['post'] = 'CommonController/fetchwhdata';
$route['save-appliance']['post'] = 'CommonController/SaveApplianceDetails';
$route['save-brand']['post'] = 'CommonController/UpdateBrandDetails';
$route['warehouse']['get'] = 'CommonController/WarehouseView';
$route['save-warehouse']['post'] = 'CommonController/SaveWarehouseDetails';

//stock related routes
$route['fetchCounterStock1']['post'] = 'StockController/FetchCounterStock';
$route['fetchWarehouseStock1']['post'] = 'StockController/FetchWareHouseStock';
$route['save-MisEntry'] = 'StockController/SaveCounterMISEntry';
$route['fetch-MisEntry']['post'] = 'StockController/FetchCounterMISList';
$route['mis-entry']['get'] = 'StockController/LoadStockDistributionView';
$route['stock-transfer'] = 'StockController/LoadStockTransfer';
$route['saveStockTransfer'] = 'StockController/SaveStockTransfer';
$route['fetchStockTransfer']['post'] = 'StockController/FetchStockTransferList';
$route['defective-stockEntry'] = 'StockController/DefectiveStockEntry';
$route['updated-stockEntry']['post'] = 'StockController/UpdateCounterStock';


//counter cash related routes
$route['fetchCounterCash']['post'] = 'CashController/FetchCounterCash';
$route['saveCashTransfer']['post'] = 'CashController/SaveCashTransfer';
$route['fetchCashTransfer']['post'] = 'CashController/FetchCashTransferList';
$route['expenses']['get'] = 'CashController/LoadExpenseView';
$route['expense-get']['post'] = 'CashController/FetchExpenseList';
$route['expense-save']['post'] = 'CashController/SaveExpenseDetails';
$route['fetchCashDetailsByUser']['post'] = 'CashController/FetchCashForUser';
$route['cash-transfer'] = 'CashController/LoadCashTransferView';

//User related Routes
$route['authenticate-user']['post'] = 'UsersController/AuthenticateUser';
$route['users']['get'] = 'UsersController/LoadUserRegisterView';
$route['roles']['get'] = 'UsersController/FetchRoles';
$route['create-user']['post'] = 'UsersController/CreateUser';
$route['edit-user']['post'] = 'UsersController/ResetUserPassword';
$route['fetch-users']['POST'] = 'UsersController/FetchUserList';
$route['change-password']['get'] = 'UsersController/LoadUserChangePasswordView';
$route['internal-userlist']['POST'] = 'UsersController/FetchInternalUserList';

//Approval Related Routes
$route['approval']['get'] = 'StockController/LoadStockTransferApproval';
$route['fetch-approvalList']['post'] = 'StockController/stockApprovedRejectList';
$route['save-stockTransferApproval']['post'] = 'StockController/ApproveRejectStockTransfer';
$route['fetch-cashapprovalList']['post'] = 'CashController/FetchCashApproveRejectList';
$route['save-cashTransferApproval'] = 'CashController/ApproveRejectCashTransfer';
$route['fetch-ExpapprovalList']['post'] = 'CashController/FetchExpenseApproveRejectList';
$route['save-expTransferApproval'] = 'CashController/ApproveRejectExpense';

//Reports related view
$route['stock-position']['get'] = 'MISReportsController/LoadMISView';
$route['daily-sales']['get'] = 'MISReportsController/LoadSALEView';
$route['cash-pendency']['get'] = 'MISReportsController/LoadCashPendencyview';

$route['available-stock']['POST'] = 'MISReportsController/FetchStock';
$route['available-sale']['POST'] = 'MISReportsController/FetchSaleSummary';
$route['available-cash']['POST'] = 'MISReportsController/FetchCash';

$route['chart']= 'MISReportsController/ChartView';
$route['monthly-expense']['get'] = 'MISReportsController/FetchGraph';
//Redirection
$route['redirect-to-customepage']= 'RedirectController/LoadRedirectView';
$route['file-upload']= 'UsersController/fileupload';
//$route['captcha']= 'Captcha/index';
