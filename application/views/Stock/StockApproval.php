<script src="<?php echo base_url(); ?>script/angular/ApprovalController.js" type="text/javascript"></script>

<div class="col-md-12 responsivePaddingNone" ng-controller="ApprovalController">
    <div class="tab-content col-md-8 form-group" >  
        
    <div class="panel-group" ng-show='panel== false'>
        <div class="panel panel-default">
            <a  class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                        <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1" ></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control"  ng-model="Statesearch" ng-change='fetchDistrictList();'  ng-disabled='{{sdisabled}}'> 
                                <option value="0" >--All--</option>
                                <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control" ng-model="Districtsearch"   ng-disabled='{{ddisabled}}' data-live-search="true">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictListsearch" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                  
                    <div class="col-md-12 paddingNone margin-filter">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="GetApprovalList(); GetCashApprovalList(); GetExpApprovalList(); ">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>


        <ul class="nav nav-tabs">
            <li   class="active"><a data-toggle="tab" href="#home" ng-click="tabChange();">Cash Transfer</a></li>
             <?php
            if ($_SESSION['RoleType'] == 'SUPERADMIN') {
                  echo'<li><a data-toggle="tab" href="#menu3" ng-click="tabChange();">Expenses</a></li>';
                 }
            ?>
            <?php
             if ($_SESSION['RoleType'] != 'DISTRICTADMIN' && $_SESSION['RoleType'] != 'SUPERADMIN') {
            echo'<li><a data-toggle="tab" href="#menu2" ng-click="tabChange();">Stock Transfer</a></li>';
             }
            ?>
        </ul>
        <div id="home" class="tab-pane fade in active">
          <?php $this->load->view('SharedViews/CashApprovalList.php'); ?>
        </div>
        <div id="menu2" class="tab-pane fade">
              <?php $this->load->view('SharedViews/StockApprovalList.php'); ?>
            
        </div>
        <div id="menu3" class="tab-pane fade">
            <?php 
                $this->load->view('SharedViews/ExpenseApprovalList.php');
             ?>
        </div>
    </div>
    <div class="col-md-4 form_View" >
        <?php $this->load->view('SharedViews/StockApprovalForm.php'); ?>
        <?php $this->load->view('SharedViews/CashApprovalForm.php'); ?>
        <?php $this->load->view('SharedViews/ExpenseApprovalForm.php'); ?>
    </div>
</div>
<div class="clearfix"></div>

<script>
    $('select').select2({ width: '100%',
    
    });      
</script>
