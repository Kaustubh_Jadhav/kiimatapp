<a href="#cashview" id="cashview"></a>
<div ng-if="IsCashDataShow == 1" id="cashview" ng-cloak>
        <div class="col-md-12">
            <h2 class="centerAlign formHeaderTitle">Details</h2>
        </div> 
        <div class="col-md-12 alert alert-danger" role="alert" ng-show="ErrorMessage != ''" ng-focus="ErrorMessage != ''">
            {{ErrorMessage}}
        </div>
        <input type="hidden" id="StockId" value="{{SelectedData['Id']}}">
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Recipient</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['RecipientAccountType']}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Transferred  From</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['Username_From']}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone ">Transferred On</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['TrnDate']| dateToISO | date:'dd-MM-yyyy'}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Amount Transferred</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label"><i class=" rupe fas fa-rupee-sign " style='font-size:12px' ></i> &nbsp; {{SelectedData['TrnAmount'] | INR}} /- </label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Sender Comments</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['Comments']}}</label>
            </div>
        </div>
        <br />
        
<!--        <div class="col-md-12 form-group"> 
             <label class="col-md-4 col-xs-6 paddingNone">Attachment</label>
              <div class="col-md-8 col-xs-6">
             <input type="file" file-model="cashfile" id="cashfile" class="cashfile" />
             </div>
        </div>
        <div class="clearfix"></div>-->
       
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval == 1">
            <label class="col-md-4 col-xs-6 paddingNone">Approval Remarks</label>
            <div class="col-md-8 col-xs-6 form-group">
                <textarea class="form-control txt_TextBox" placeholder="Approval Remark" name="txt_remarks" limit-to="100" ng-model="txt_remarks.text" required ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)"></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval != 1">
            <label class="col-md-4 col-xs-6 paddingNone">Rejection Remarks</label>
            <div class="col-md-8 col-xs-6 form-group">
                <textarea class="form-control txt_TextBox" placeholder="Rejection Remark" name="txt_remarks" limit-to="100" ng-model="txt_remarks.text" ng-change="ClearMsg()" ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)" required></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending'">
            <input type="button" class="btn btn_Cancel btn_form" value="Cancel" ng-click="CancelCashPanel();">&nbsp;
            <input type="button" class="btn btn_Button btn_form" value="APPROVE" ng-click="SaveCashComments();" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval == 1">
            <input type="button" class="btn btn_Button btn_form" value="REJECT" ng-click="SaveCashComments();" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval != 1">
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() !=  'pending'">
            <input type="button" class="btn btn_Cancel btn_form" value="Cancel" ng-click="CancelCashPanel();">
        </div>
        <div class="clearfix"></div>
    </div>
<div class="clearfix"></div>
