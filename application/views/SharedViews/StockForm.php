

<div class="col-md-12" ng-cloak>
    <h2 class="centerAlign formHeaderTitle">Add New MIS Entry</h2>
</div>

<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="myForm"  class="paddingNone" ng-cloak novalidate id='FormDiv'  ng-submit="SaveStockDetails(myForm.$valid)">
        
        
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Appliance Type</label>
            <div class="col-md-8">
                <select id="dd_expnsType" class="form-control dd_DropDown" name="dd_expnsType" ng-model="dd_expnsType" ng-change="getdetails1()">
                    <option value="0" ng-selected="selected">--Select--</option>
                    <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                    
                    
                </select>
                <p ng-if="IsExpenseType == 1" class="errorMessage">Select Appliance Name.</p>
            </div>
        </div>
        
       <div class="row-container" ng-show="show=='true'">
         
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label"> Entry Date</label>
            <div class="col-md-8">
                <md-datepicker ng-model="txt_trnDate" md-placeholder="Enter date" md-min-date="minmisDate" md-max-date="maxmisDate"></md-datepicker>
            </div>
        </div>
            
         <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Distributed</label>
            <div class="col-md-3">
                <input type="text" class="form-control txt_TextBox small_txt_TextBox" name="txt_QtyDistributed" ng-model='txt_QtyDistributed' only-digits limit-to="10" autocomplete="off">
                <p ng-show="myForm.txt_QtyDistributed.$invalid && !myForm.txt_QtyDistributed.$pristine" class="errorMessage">Quantity distributed is required.</p>
            </div>
        </div>
        
         <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Replaced</label>
            <div class="col-md-3">
                <input type="text" class="form-control txt_TextBox small_txt_TextBox" name="QtyReplaced" ng-model="QtyReplaced" only-digits limit-to="10" ng-model="QtyReplaced" autocomplete="off">
                <p ng-show="myForm.QtyReplaced.$invalid && !myForm.QtyReplaced.$pristine" class="errorMessage">Enter Quantity Replaced.</p>
            </div>
        </div>
        
         <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">BD</label>
            <div class="col-md-3">
                <input type="text" class="form-control txt_TextBox small_txt_TextBox" name="QtyBD" ng-model="QtyBD" only-digits limit-to="10" ng-model="QtyBD"  autocomplete="off">
                <p ng-show="myForm.QtyBD.$invalid && !myForm.QtyBD.$pristine" class="errorMessage">Enter Quantity distributed for BD.</p>
            </div>
        </div>
        
         <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Amount Collected</label>
             <div class="col-md-8" ng-model="totalAmount" ng-show="dd_expnsType=='0' && txt_QtyDistributed=='' "></div>
              <div class="col-md-8" ng-model="totalAmount" ng-show="dd_expnsType != '0' && txt_QtyDistributed!=''"><i class=" rupe fas fa-rupee-sign " style='font-size:13px' ></i> &nbsp; {{ RatePerPc*txt_QtyDistributed|| ''}}
               /-</div>
        </div>
            
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Comments</label>
            <div class="col-md-8">
                <textarea  class="form-control txt_TextBox" name="txt_comments" limit-to="100" ng-model="txt_comments" required></textarea>
                <p ng-show="myForm.txt_comments.$invalid && !myForm.txt_comments.$pristine" class="errorMessage">Comments are required.</p>
            </div>
        </div>
         
        <div class="col-md-12 form-group"  ng-show="dd_expnsType != 0">
            &nbsp;&nbsp;&nbsp; &nbsp;<span  ng-model="Available">Available stock ={{Available}}</span>&nbsp;&nbsp;  
            <span> Rate/Pcs = <i class=" rupe fas fa-rupee-sign " style='font-size:13px' ></i> &nbsp;{{RatePerPc|number : 0}} /-</span>
        </div>
         
       
        <!--<div class="col-md-12 form-group centerAlign">
            <input type="submit" value="Save" class="btn btn_Button" ng-disabled="Available=='' && RatePerPc==''">
        </div>-->
        
         <div class="col-md-12 form-group">
             <div class="col-md-12 ">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToDistribution()">&nbsp;
                <input type="submit" value="Save" class="btn  btn_Button btn_form"  ng-disabled="Available=='' && RatePerPc==''" >
            </div>  
         </div>
        </div> 
       
    </form>
<div class="clearfix"></div>

