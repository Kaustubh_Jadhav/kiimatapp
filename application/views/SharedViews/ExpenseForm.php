<?php 

   $img = $this->session->userdata('captchaImg');
//   echo"hii";

?>
<div class="col-md-12" ng-cloak>
    <h2 class="centerAlign formHeaderTitle">Add Expense</h2>
</div>
<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-show="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-show="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="myForm" ng-cloak class='paddingNone'  novalidate ng-submit="SaveExpenseDetails(myForm.$valid)">
        <div ng-show="IsCashInHandPresent">
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Transaction Date</label>
                <div class="col-md-8">
                    <md-datepicker ng-model="txt_trnDate" md-placeholder="Enter date" md-min-date="minDate" md-max-date="maxDate" readonly id="datePicker" ></md-datepicker>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Expense Type</label>
                <div class="col-md-8">
                    <select id="dd_expnsType" class="form-control dd_DropDown" name="dd_expnsType" ng-model="dd_expnsType" ng-change="IsExpenseType = 0">
                        <option value="0" ng-selected="selected">--Select--</option>
                        <option value="COMMISSION EXPENSE">COMMISSION EXPENSE</option>
                        <option value="ELECTRICITY BILL">ELECTRICITY BILL </option>
                        <option value="INSURANCE">INSURANCE</option>
                        <option value="LOADING AND UNLOADING CHARGE">LOADING AND UNLOADING CHARGE</option>
                        <option value="MARKETING / PROMOTIONS">MARKETING / PROMOTIONS</option>
                        <option value="MOBILE AND TELEPHONE BILL">MOBILE AND TELEPHONE BILL</option>
                        <option value="OFFICE RENT">OFFICE RENT</option>
                        <option value="PRINTING / STATIONERY">PRINTING / STATIONERY</option>
                        <option value="SALARY">SALARY</option>
                        <option value="SECURITY DEPOSIT">SECURITY DEPOSIT</option>
                        <option value="STATIONARY EXPENSES">STATIONARY EXPENSES</option>
                        <option value="TELE / MOBILE">TELE / MOBILE</option>
                        <option value="TRAVEL">TRAVEL</option>
                        <option value="TRANSPORTATION CHARGE">TRANSPORTATION CHARGE</option>
                        <option value="TRAVELLING EXPENSES">TRAVELLING EXPENSES</option>
                        <option value="WAREHOUSE RENT">WAREHOUSE RENT</option>
                        <option value="WATER BILL">WATER BILL</option>
                        <option value="ANY OTHER">OTHER</option>
                    </select>
                    <p ng-if="IsExpenseType == 1" class="errorMessage">Expense type is required.</p>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Amount Spent</label>
                <div class="col-md-8">
                    <input autocomplete="off" type="text" class="form-control small_txt_TextBox txt_TextBox" name="txt_amountSpent" ng-model="txt_amountSpent" only-digits limit-to="7" ng-model="txt_amountSpent" required>
                    <p ng-show="myForm.txt_amountSpent.$invalid && !myForm.txt_amountSpent.$pristine" class="errorMessage">Amount spent is required.</p>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Comments</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox" name="txt_comments" limit-to="100" ng-model="txt_comments" required ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)"></textarea>
                    <p ng-show="myForm.txt_comments.$invalid && !myForm.txt_comments.$pristine" class="errorMessage">Comments are required.</p>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Cash In-Hand</label>
                <div class="col-md-8">
                    <p><i class=" rupe fas fa-rupee-sign " style='font-size:13px' ></i> &nbsp;{{TotalAmount| INR}} /-</p>
                </div>
            </div>
            
            
<!--          <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label"></label>
                <div class="col-md-8">
                           <input type="text" class="form-control" style="margin-bottom: 0px; width: 180px; height: 40px; font-size: 24px; font-weight: bolder; text-decoration: line-through; display: inline-block; background-color: #f2f2f2;" id="mainCaptcha" disabled="">
                           <p id='mainCaptcha' class='captcha'></p>          
                           <i class="fas fa-sync-alt refresh-icon"  id="btnExport" onclick="Captcha();" ></i>
                </div>
          </div>-->
            
<!--           <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label"></label>
                <div class="col-md-8">
                      <input type="text" class="form-control" ng-keydown="[32].includes($event.keyCode) && onKeydown($event)" ng-model="captcha_value" required placeholder="Enter Captcha Value" maxlength=''>
                </div>
          </div>-->

<!--           <div class="col-md-12">
                <label class="col-md-4 lbl_Label"></label>
                <div class="col-md-8">
                    <p id="captImg"><?php echo $this->session->userdata('captchaImg') ; ?></p>
                    <p id="captImg"></p>
                    <p>Can't read the image? click <a href="javascript:void(0);" class="refreshCaptcha">here</a> to refresh.</p>
                </div>
           </div>

            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label"></label>
                <div class="col-md-8">
                      <input type="text" class="form-control" ng-keydown="[32].includes($event.keyCode) && onKeydown($event)" ng-model="captcha_value" required placeholder="Enter Captcha Value" maxlength=''>
                </div>
            </div>-->
            <div class="col-md-12 form-group centerAlign">
<!--                 <input type="button" value="Check" class="btn btn_Cancel btn_form" ng-click="validate()">&nbsp;-->
                <input type="submit" value="Submit For Approval" class="btn btn_Button">
            </div>
        </div>
        
    </form>
<div class="clearfix"></div>
