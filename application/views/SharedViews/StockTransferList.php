<?php $StockType = unserialize(StockType) ?>

<div ng-cloak>
     <h2 class=" formHeaderTitle">Stock Transfer</h2>
<div class="panel-group">
        <div class="panel panel-default">
            <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="StateSearch" ng-disabled="IsStateEnable" ng-change="GetDistrictList()"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="DistrictSearch" ng-disabled="IsDistrictEnable" ng-change="GetUserList();FetchWarehouseList();">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">Requested By User</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Uname" ng-disabled="IsUserEnable">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in UList |filter:{ WarehouseName:null}| orderBy:'Username':asc" value="{{x.Username}}">{{x.Username}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">Requested By Warehouse</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Wname" ng-disabled="IsWhEnable">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in WarehouseList| orderBy:'Name_':asc" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    
                     <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">Status</label>
                        <div class="col-md-3 form-group">
                            <!--<input type="text" class="form-control txt_TextBox" ng-model="search">-->
                            <select class="form-control select2" ng-model="Status">
                                <option value="0" ng-selected="selected">--All--</option>
                                <option value="<?php echo TRN_STATUS_PENDING ?>"><?php echo TRN_STATUS_PENDING ?></option>
                                <option value="<?php echo TRN_STATUS_APPROVED ?>"><?php echo TRN_STATUS_APPROVED ?></option>
                                <option value="<?php echo TRN_STATUS_REJECTED ?>"><?php echo TRN_STATUS_REJECTED ?></option>
                            </select>
                        </div>
                       
                         <div class=" form-group col-md-3">
                            <label class="col-md-1">From</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="STfrom_Date" id="datePicker"  md-placeholder="Select date" md-max-date="maxDate" readonly flex ></md-datepicker>
                             </div>
                        </div>
                        <div class=" form-group col-md-3">
                            <label class="col-md-1">To</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="STto_Date" id="datePicker1"  md-placeholder="Select date" md-max-date="maxDate" readonly flex ></md-datepicker>
                             </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-1 lbl_Label">Appliance Type</label>
                        <div class="col-md-3 form-group">
                            <select id="dd_applienceType" class="form-control dd_DropDown select2"  ng-model="applienceType" ng-change="FetchStock();">
                                <option value="0" ng-selected="selected">--All--</option>
                                <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                         
                        </div>
                        
                        <label class="col-md-1 lbl_Label">Stock Type</label>
                        <div class="col-md-3 form-group"> 
                              <select  class="form-control dd_DropDown select2"  ng-model="stockType" ng-change="FetchBrandList();">
                                 <option value="0" ng-selected="selected">--All--</option>
                                 <option value="<?php echo $StockType[0] ?>"><?php echo $StockType[0] ?></option>
                                 <option value="<?php echo $StockType[1] ?>"><?php echo $StockType[1] ?></option>
                               </select>
                        </div>
                        
                        <label class="col-md-1 lbl_Label">Brand</label>
                        <div class="col-md-3 form-group">
                            <select id="dd_brandName" class="form-control dd_DropDown select2"  ng-model="brandName" >
                                <option value="0" ng-selected="selected">--All--</option>
                                <option ng-repeat="x in BrandList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12 paddingNone" style='margin-top:10px;'>
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="getDate();GetStockTrasnferList();">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>

<div class="col-md-12 centerAlign listView" ng-if="StockTransferList.length > 0">
    <div class="col-md-12">
        <div class="col-md-3 right-btn" >
             <i class="fas fa-file-excel excel-icon right-btn"  id="btnExport" onclick="Export()" ng-show='StockTransferList.length > 0'></i>
        <!--<input type="button" class="action_btn btn btn_Button" id="btnExport" value="Export" onclick="Export()" ng-show='ExpenseList.length > 0'>-->
        </div>
   </div>
   <div class="clearfix"></div> 
    <div class="tableScroll">
    <table class="table table-striped table-responsive-md" ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th ng-click='sortColumn("Transaction Date")' ng-class='sortClass("Transaction Date")'>Transaction Date</th>
                <th ng-click='sortColumn("Stock Type")' ng-class='sortClass("Stock Type")'>Stock Type</th>
                <th ng-click='sortColumn("Brand Name")' ng-class='sortClass("Brand Name")'>Brand Name</th>
                <th ng-click='sortColumn("Appliance Name")' ng-class='sortClass("Appliance Name")'>Appliance Name</th>
                <th ng-click='sortColumn("Transfer From")' ng-class='sortClass("Transfer From")'>Transfer From</th>
                <th ng-click='sortColumn("Transfer To")' ng-class='sortClass("Transfer To")'>Transfer To</th>
                <th ng-click='sortColumn("Qty")' ng-class='sortClass("Qty")'>Qty</th>
                <th ng-click='sortColumn("Comments")' ng-class='sortClass("Comments")'>Comments</th>
                <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                <th ng-click='sortColumn("Username Approver")' ng-class='sortClass("Username Approver")'>Username_<br>Approver</th>
                 <th ng-click='sortColumn("Approver Remarks")' ng-class='sortClass("Approver Remarks")'>Approver<br>Remarks</th>
                <th ng-click='sortColumn("Action TakenOn")' ng-class='sortClass("Action TakenOn")'>Action<br>Taken On</th>
                <!--<th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>-->
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in StockTransferList | filter:search | orderBy:sortBy:reverse| itemsPerPage:10">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x['Transaction Date'] | dateToISO | date:'dd-MMM-yyyy'}}</td>
                <td>{{ x['Stock Type']}}</td>
                <td>{{ x['Brand Name']}}</td>
                <td>{{ x['Appliance Name']}}</td>
                <td>{{ x['Transfer From']}}</td>
                <td>{{ x['Transfer To']}}</td>
                <td>{{ x.Qty}}</td>
                <td>{{ x.Comments}}</td>
                <td>{{ x.Status |uppercase}}</td>
                <td>{{ x['Userame Approver']}}</td>
                <td>{{ x['Approver Remarks']}}</td>
                <td>{{ x['Action TakenOn']}}</td>
                <!--<td>{{ x.CreatedOn | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>-->
            </tr>
        </tbody>
    </table>
    </div>
    <dir-pagination-controls
        max-size="5"
        direction-links="true"true
        boundary-links="true" on-page-change='indexCount(newPageNumber)'>
    </dir-pagination-controls>
    <div class="clearfix"></div>
</div>
<div class="col-md-12 centerAlign listView">
         <p>{{STnoRecord}}</p>
    </div>
</div>
<script>
   function Export() {
        var data = angular.copy($('[ng-controller="StockController"]').scope().StockTransferList);
            //var fullDate = new Date();
            
            var Username =  $('[ng-controller="StockController"]').scope().Username;
            var parse_data= JSON.stringify(data, function(key, value) { 
            
                    if(value == null || value == undefined) {
                         return ""
                    }
                    else{
                        return value
                    }
          });
          
            var send= JSON.parse(parse_data);
            var date=  new Date();
            var format_date = date.getFullYear() +""+ (date.getMonth()+1) +""+
                                + date.getDate();
            var title="StockTransferList" +"-"+ Username;
           
            $("#el").excelexportjs({
                containerid: null, 
                datatype:'json',
                dataset: send,
                columns: getColumns(send),
                returnUri:false,
                worksheetName: title,
                //filename:"ExpenseList" +Username+ new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
                encoding: "utf-8",
                locale:'en-US'
            });
        }
        
        $('.select2').select2({ width: '100%' });
 </script>      
