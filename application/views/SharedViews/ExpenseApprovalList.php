<div ng-cloak >
    <h2 class=" formHeaderTitle">PENDING EXPENSES</h2>
    <div class="panel-group">
        <div class="panel panel-default" ng-show="show==true">
            <a  style="color: #000;" data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="State"> 
                                <option value="0">--All--</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="District">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">User</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="Users">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 centerAlign listView" ng-if="ExpApprovalList.length > 0">
        <div class="tableScroll" style="overflow:auto;">
            <table class="table table-striped table-responsive-md" ng-table="tableParams">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th ng-click='sortColumn("TrnDate")' ng-class='sortClass("TrnDate")'>Transaction Date</th>
                        <th ng-click='sortColumn("Username")' ng-class='sortClass("Username")'>Username</th>
                        <th ng-click='sortColumn("ExpenseType")' ng-class='sortClass("ExpenseType")'>Expense Type</th>
                        <th ng-click='sortColumn("AmountSpent")' ng-class='sortClass("AmountSpent")'>Amount Spent (&#8377;)</th>
                        <th>Comments</th>
                        <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                        <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in ExpApprovalList | filter:search | orderBy:sortBy:reverse|  itemsPerPage:10" pagination-id="page3">
                        <td>{{ $index + 1 + serial}}</td>
                        <td>{{ x.TrnDate | dateToISO | date:'dd-MMM-yyyy'}}</td>
                        <td>{{ x.Username}}</td>
                        <td>{{ x.ExpenseType}}</td>
                        <td class='align'>{{ x.AmountSpent | INR}}</td>
                        <td>{{ x.Comments}}</td>
                        <td>{{ x.Status |uppercase}}</td>
                        <td>{{ x.CreatedOn | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                        <td ng-if="x.Status.toLowerCase() == 'pending'">
                            <input type="button" value="Approve" class="action_btn btn btn_Button btn-small" id='btn'  ng-click="OpneExpenseAction(1, x.Id);save('expenseview'); ">                           
                        </td>
                        <td ng-if="x.Status.toLowerCase() == 'pending'">
                            <input type="button" value="Reject" class="action_btn btn btn_Button btn-small" ng-click="OpneExpenseAction(2, x.Id);save('expenseview');">
                        </td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
        <dir-pagination-controls
            max-size="5"
            direction-links="true"true
            boundary-links="true"  pagination-id="page3" on-page-change='indexCount(newPageNumber)'>
        </dir-pagination-controls>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 centerAlign listView" >
        <p>{{ExpRecord}}</p>
    </div>
</div>
<script type="text/javascript">

//function save() {
//          alert('hii');
//          location.hash = "form_Div";
//    };
</script>
