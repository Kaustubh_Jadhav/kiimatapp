<?php $RecipientAccountType = unserialize(RecipientAccountType) ;
        ?>
<div ng-cloak>
     <h2 class=" formHeaderTitle">Cash Transfer</h2>
    <div class="panel-group" ng-show='panel== true'>
        <div class="panel panel-default">
            <a  class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group" >
                            <select class="form-control select2" ng-model="State" ng-disabled="IsStateEnable" ng-change="GetDistrictList()"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group">
                             <select class="form-control select2" ng-model="District" ng-disabled="IsDistrictEnable" ng-change="GetUserList();">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">Requested By</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Uname" ng-disabled="IsUserEnable">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in UserList" value="{{x.Username}}">{{x.Username}}</option>
                            </select>
                        </div>
                      
                        <label class="col-md-2 lbl_Label">Status</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Status">
                                <option value="0" ng-selected="selected">-- All --</option>
                                <option value="<?php echo TRN_STATUS_PENDING ?>"><?php echo TRN_STATUS_PENDING ?></option>
                                <option value="<?php echo TRN_STATUS_APPROVED ?>"><?php echo TRN_STATUS_APPROVED ?></option>
                                <option value="<?php echo TRN_STATUS_REJECTED ?>"><?php echo TRN_STATUS_REJECTED ?></option>
                            </select>
                        </div>
                    </div>
                    
                     <div class="col-md-12 paddingNone filter-group margin-filter">
                       <label class="col-md-2 lbl_Label">Account Type </label>
                       <div class="col-md-4 form-group">
                           <select class="form-control dd_DropDown select2" name="RecipientType" ng-model="RecipientType">
                               <option value="0" ng-selected="selected">--All--</option>
                               <option value="<?php echo $RecipientAccountType[0] ?>"><?php echo $RecipientAccountType[0] ?></option>
                               <option value="<?php echo $RecipientAccountType[1] ?>"><?php echo $RecipientAccountType[1] ?></option>
                               <option value="<?php echo $RecipientAccountType[2] ?>"><?php echo $RecipientAccountType[2] ?></option>

                           </select>
                       </div>
                       
<!--                       <label class="col-md-2 lbl_Label">Transferred To </label>
                       <div class="col-md-4 form-group">
                            <select id="Trn_To" class="form-control dd_DropDown" name="Trn_To" ng-model="Trn_To">
                                <option value="0" selected>--All--</option>
                                <option ng-repeat="x in ToList" value="{{x.Username}}">{{x.Username| uppercase }}</option>
                             </select>
                       </div>-->
                        
                    </div>
                    
                     <div class="col-md-12 paddingNone filter-group margin-filter">
                        <label class="col-md-1 lbl_Label">From</label>
                        <div class=" form-group col-md-3">
                             <md-datepicker ng-model="from_Date" id="datePicker"  md-placeholder="Select date" md-max-date="maxDate" readonly flex ></md-datepicker>
                        </div>
                        <label class="col-md-1 lbl_Label">To</label>
                        <div class=" form-group col-md-3">
                             <md-datepicker ng-model="to_Date" id="datePicker1"  md-placeholder="Select date" md-max-date="maxDate" readonly flex ></md-datepicker>
                        </div>
                    </div>
                   
                    
                    <div class="col-md-12 paddingNone margin-filter">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click=" getDate();GetCashTransferList();">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
 
<div class="col-md-12 centerAlign listView" ng-if="CashTransferList.length > 0" >
    
<div class="col-md-12">
        <div class="col-md-3 right-btn" >
         <i class="fas fa-file-excel excel-icon right-btn"  id="btnExport" onclick="Export()" ng-show='CashTransferList.length > 0'></i>
       <!-- <input type="button" class="action_btn btn btn_Button" id="btnExport" value="Export" onclick="Export()" ng-show='CashTransferList.length > 0'>-->
        </div>
</div>
<div class="clearfix"></div>
    <div class="tableScroll">
    <table class="table table-striped table-responsive-md" id='tblCustomers' ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th ng-click='sortColumn("Transaction Date")' ng-class='sortClass("Transaction Date")'>Transaction Date</th>
                <th ng-click='sortColumn("From")' ng-class='sortClass("From")'>From</th>
                <th ng-click='sortColumn("Recipient Account Type")' ng-class='sortClass("Recipient Account Type")'>Recipient AccountType</th>
                <th ng-click='sortColumn("To")' ng-class='sortClass("To")'>To</th>
                <th ng-click='sortColumn("AmountTransferred")' ng-class='sortClass("AmountTransferred")'>Amount Transferred (&#8377;)</th>
                <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                <th ng-click='sortColumn("Comments")' ng-class='sortClass("Comments")'>Comments</th>
                <th ng-click='sortColumn("Username Approver")' ng-class='sortClass("Username Approver")'>Username Approver</th>
                <th ng-click='sortColumn("ActionTakenOn")' ng-class='sortClass("ActionTakenOn")'>Action TakenOn</th>
                <th ng-click='sortColumn("Approver Remarks")' ng-class='sortClass("Approver Remarks")'>Approver Remarks</th>
                <th ng-click='sortColumn("Created On")' ng-class='sortClass("Created On")'>Created On</th>
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in CashTransferList| filter:search | orderBy:sortBy:reverse  | itemsPerPage:10"  pagination-id="page3">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x['Transaction Date']}}</td>
                <td>{{ x['From']}}</td>
                <td>{{ x['Recipient Account Type']}}</td>
                <td>{{ x['To']}}</td>
                <td class='align'>{{ x['AmountTransferred']| INR}}</td>
                <td>{{ x.Status |uppercase}}</td>
                <td>{{ x.Comments}}</td>
                   <td>{{ x['Username Approver']}}</td>
                <td>{{ x.ActionTakenOn}}</td>
                <td>{{ x['Approver Remarks']}}</td>
                <td>{{ x['Created On'] | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
            </tr>
        </tbody>
    </table>
    </div>
    <dir-pagination-controls
            max-size="5"
            direction-links="true"
            boundary-links="true" on-page-change='indexCount(newPageNumber)' pagination-id="page3">
    </dir-pagination-controls>
    <div class="clearfix"></div>
    
</div>
<div class="col-md-12 centerAlign listView">
        <p>{{noRecord}}</p>
    </div>
</div>
<!--<input type="button" id="btnExport" value="Export" onclick="Export()" ng-show='CashTransferList.length > 0' />-->
<script>
     $('.select2').select2({ width: '100%' });
   function Export() {
       
           //var data = JSON.stringify(makeJsonFromTable('tblCustomers'));
           //alert(data.length);
           
            var data = angular.copy($('[ng-controller="CashController"]').scope().CashTransferList);
            //var fullDate = new Date();
            
            var Username =  $('[ng-controller="CashController"]').scope().Username;
           // var date= new Date();
            //var l = date.toString();
            var parse_data= JSON.stringify(data, function(key, value) { 
                    
//                    if( key === "$$hashKey" ){
//                        return ""
//                    }
                    if(value == null || value == undefined) {
                         return ""
                    }
                    else{
                        return value
                    }
          });
          
            var send= JSON.parse(parse_data);
            var date=  new Date();
            var format_date = date.getFullYear() +""+ (date.getMonth()+1) +""+
                                + date.getDate();
            var title="CashTransferList" +"-"+ Username +format_date;
            var today = new Date().toString().replace(/[^\w]/g, '');
            var filename = new Date().getTime();
    
           // JSONToCSVConvertor(send, title, true);
           $("#el").excelexportjs({
                containerid: null, 
                datatype:'json',
                dataset: send,
                columns: getColumns(send),
                returnUri:false,
                worksheetName:title ,
                //filename:"ExpenseList" +Username+ new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
                encoding: "utf-8",
                locale:'en-US'
               
            });
        }
            
 </script>      


