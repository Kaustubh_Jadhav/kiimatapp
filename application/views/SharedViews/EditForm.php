 <div ng-show='EditForm==1' ng-cloak>
        <form name="EditForm" novalidate ng-submit="EditUserDetails(EditForm.$valid)">
            <div class="col-md-12">
                <h2 class="centerAlign  formHeaderTitle">Reset User</h2>
            </div>
            <input type="hidden" id="StockId" value="{{SelectedData['Id']}}">
        
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 ">Username</label>
            <div class="col-md-8">
                <label class="lbl_Label">{{SelectedData['Username']}}</label>
            </div>
        </div> 

        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 ">New Password</label>
            <div class="col-md-8">
                <input type="text" class="form-control txt_TextBox" name="password"  ng-model="password" autocomplete="off" maxlength='15'>
                <p ng-show="EditForm.password.$invalid && !EditForm.password.$pristine" class="errorMessage">Password is required.</p>
            </div>
        </div>
        
        <!--<div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Confirm Password</label>
            <div class="col-md-8">
                <input type="text" class="form-control txt_TextBox" name="cnfpassword"  ng-model="cnfpassword" autocomplete="off" maxlength='15'>
                <p ng-show="EditForm.cnfpassword.$invalid && !EditForm.cnfpassword.$pristine" class="errorMessage">Confirm Password is required.</p>
            </div>
        </div>-->
        <div class="clearfix"></div>
            <div class="col-md-12 form-group">
                <div class="col-md-12 ">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form"  ng-click="clearScreen()">&nbsp;
                <input type="submit" value="Save" class="btn btn_Button btn_form">
                </div>
            </div>
         <div class="clearfix"></div>
        </form>
    </div>
<div class="clearfix"></div>


