<div ng-cloak>
    
  <!--<div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="GetCashApprovalList();">
  </div>-->
     <h2 class=" formHeaderTitle">PENDING CASH TRANSFERS</h2>
    <div class="panel-group">
        <div class="panel panel-default" ng-show="show==true">
            <a  class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="State"> 
                                <option value="0">--All--</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="District">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">User</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="Users">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 centerAlign listView" ng-if="CashApprovalList.length > 0">
        <div class="tableScroll" style="overflow:auto;">
            <table class="table table-striped table-responsive-md" ng-table="tableParams">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th ng-click='sortColumn("TrnDate")' ng-class='sortClass("TrnDate")'>Transferred On</th>
                        <th ng-click='sortColumn("Username_From")' ng-class='sortClass("Username_From")'>Transferred  From</th>
                        <th ng-click='sortColumn("RecipientAccountType")' ng-class='sortClass("RecipientAccountType")'>Recipient</th>
                        <th ng-click='sortColumn("Username_To")' ng-class='sortClass("Username_To")'>Transferred To</th>
                        <th ng-click='sortColumn("TrnAmount")' ng-class='sortClass("TrnAmount")'>Amount Transferred (&#8377;)</th>
                        <th>Comments</th>
                        <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                        <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in CashApprovalList | filter:search | orderBy:sortBy:reverse|  itemsPerPage:10" pagination-id="page1">
                        <td>{{ $index + 1 + serial}}</td>
                        <td>{{ x.TrnDate | dateToISO | date:'dd-MMM-yyyy'}}</td>
                        <td>{{ x.Username_From}}</td>
                        <td>{{ x.RecipientAccountType}}</td>
                        <td>{{ x.Username_To}}</td>
                        <td class='align'>{{ x.TrnAmount | INR}}</td>
                        <td>{{ x.Comments}}</td>
                        <td>{{ x.Status |uppercase}}</td>
                        <td>{{ x.CreatedOn | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                        <td ng-if="x.Status.toLowerCase()  == 'pending'">
                            <input type="button" value="Approve" class="action_btn btn btn_Button btn-small" ng-click="OpneCashAction(1, x.Id); save('cashview');">                           
                        </td>
                        <td ng-if="x.Status.toLowerCase()  == 'pending'">
                            <input type="button" value="Reject" class="action_btn btn btn_Button btn-small" ng-click="OpneCashAction(2, x.Id); save('cashview');">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <dir-pagination-controls
            max-size="5"
            direction-links="true"true
            boundary-links="true" pagination-id="page1" on-page-change='indexCount(newPageNumber)'>
        </dir-pagination-controls>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 centerAlign listView" >
        <p>{{noRecord}}</p>
    </div>
</div>


