<?php $StockType = unserialize(StockType) ?>
<?php $PartyType = unserialize(VendorsType) ?>
<?php $TrnType = unserialize(TrnType) ;
  $StockType = unserialize(StockType) ;
  $Username= $this->session->userdata('Username');       
  $RefName= $this->session->userdata('WarehouseName');  
  //$DRole= $this->session->userdata('DRole');
        ?>

<div class="col-md-12"  ng-init="Username ='<?php echo $Username ?>';
    RefName ='<?php echo $this->session->userdata('RefName'); ?>' ;
     " ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="StockTransfer" class="paddingNone" id='FormDiv' ng-cloak novalidate ng-submit="SaveStockTransferDetails(StockTransfer.$valid)">
     
       <!-- {{Role}}-->
        <div class="form-group centerAlign" ng-show="show=='false' ">
           <!-- {{DRole}}-->
            <div class="col-md-12">
                <h2 class="centerAlign  formHeaderTitle">Stock Transfer Entry</h2>
            </div>
           
           <div class="form-group" ng-show="Role!='COUNTER'">
            <input type="button" value="SUPPLY RECEIVED FROM VENDOR" class="btn btn_Button Stk_btn_Button col-md-12 " ng-click="GetWhList('<?php echo $TrnType[7] ?>', '<?php echo $StockType[0] ?>', 'SUPPLY RECEIVED FROM VENDOR', 6)">
            </div>
            <div class=" form-group" ng-show="Role!='WAREHOUSEADMIN'">
            <input type="button" value="RETURN OF FRESH GOODS TO WAREHOUSE "  class="btn btn_Button Stk_btn_Button col-md-12 " ng-click="GetWhList('<?php echo $TrnType[0] ?>', '<?php echo $StockType[0] ?>','RETURN OF FRESH GOODS TO WAREHOUSE' , 2)">
            </div>
            
            <div class=" form-group" ng-show="Role!='WAREHOUSEADMIN'">
            <input type="button" value="RETURN OF DEFECTIVE GOODS TO WAREHOUSE "  class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[1] ?>', '<?php echo $StockType[1] ?>','RETURN OF DEFECTIVE GOODS TO WAREHOUSE ', 3)">
            </div>
            
            <div class=" form-group" ng-show="Role!='COUNTER'">
            <input type="button" value="DISPATCH OF FRESH GOODS TO COUNTER"  class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[2] ?>','<?php echo $StockType[0] ?>','DISPATCH OF FRESH GOODS TO COUNTER' ,1)">
            </div>
            
            <div class=" form-group" ng-show="Role!='COUNTER'">
            <input type="button" value="TRANSFER OF FRESH GOODS TO WAREHOUSE" class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[3] ?>', '<?php echo $StockType[0] ?>','TRANSFER OF FRESH GOODS TO WAREHOUSE',  2)">
            </div>
            
            <div class="form-group" ng-show="Role!='COUNTER'">
            <input type="button" value="TRANSFER OF DEFECTIVE GOODS TO WAREHOUSE" class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[4] ?>','<?php echo $StockType[1] ?>','TRANSFER OF DEFECTIVE GOODS TO WAREHOUSE',  3)">
            </div>
            
            <div class="form-group" ng-show="Role!='COUNTER'">
            <input type="button" value="RETURN OF FRESH GOODS TO VENDOR " class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[5] ?>', '<?php echo $StockType[0] ?>','RETURN OF FRESH GOODS TO VENDOR', 4)">
            </div>
            
            <div class=" form-group" ng-show="Role!='COUNTER'">
             <input type="button" value="RETURN OF DEFECTIVE GOODS TO VENDOR " class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetWhList('<?php echo $TrnType[6] ?>', '<?php echo $StockType[1] ?>','RETURN OF DEFECTIVE GOODS TO VENDOR', 5)">
            </div>
         
        </div>

        <div ng-show="show=='true' ">
         
        <div class="col-md-12">
                <h2 class="centerAlign">{{text}}</h2>
        </div>   
        
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Appliance Type</label>
            <div class="col-md-8">
                <select id="dd_applienceType" class="form-control dd_DropDown" name="dd_applienceType" ng-model="dd_applienceType" ng-change=" display(); GetBrandList(); GeAvailableStock();">
                    <option value="0" ng-selected="selected">--Select--</option>
                    <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
                <p ng-if="IsExpenseType == 1" class="errorMessage">Appliance type is required.</p>
            </div>
        </div>

        <div ng-show="hide=='false'"> 
         <div ng-show="Available!=0 || openForm==true">    
            
            <div class="col-md-12 form-group" ng-show="select==true">
                <label class="col-md-4 lbl_Label">Transfer From</label>
                <div class="col-md-8" >
                    <select id="dd_partyType" class="form-control dd_DropDown" name="dd_partyType" ng-model="dd_partyType"   ng-show="TrnType=='WH_WH_D' || TrnType=='WH_WH_F' || TrnType=='WH_CTR_F' || TrnType=='WH_VEN_F' || TrnType=='WH_VEN_D'|| TrnType=='CTR_WH_F' || TrnType=='CTR_WH_D'" ng-change="GeAvailableStock(); GetFilter(); " required>
                        <option  value="0"  selected ng-show="DRole=='COUNTER' && Role!='COUNTER'">--Select--</option>
                        <option  value="0"  selected  ng-show="DRole=='WAREHOUSEADMIN' && Role!='WAREHOUSEADMIN' ">--Select--</option>
                        <option  ng-repeat="x in WhList " ng-show="DRole=='WAREHOUSEADMIN' && Role!='WAREHOUSEADMIN' " value="{{x.Name_}}">{{x.Name_}}</option>
                        <option ng-repeat="x in UserList " ng-show="DRole=='COUNTER' && Role!='COUNTER'" value="{{x.Username}}">{{x.Username}}</option>
                    </select> 
                    
                   

                    <select ng-show="TrnType=='VEN_WH'" class="form-control dd_DropDown"  id="dd_partyType" name="dd_partyType"   ng-model="dd_partyType" ng-change="GeAvailableStock();">
                        <option value="0" ng-selected="selected">--Select--</option>
                        <option value="<?php echo $PartyType[0] ?>"><?php echo $PartyType[0] ?></option>
                        <option value="<?php echo $PartyType[1] ?>"><?php echo $PartyType[1] ?></option>
                        <option value="<?php echo $PartyType[2] ?>"><?php echo $PartyType[2] ?></option>

                    </select>
                    <p ng-show="StockTransfer.dd_partyType.$invalid && !StockTransfer.dd_partyType.$pristine" class="errorMessage">Transferred From  is required.</p>
                </div>
            </div>   
       


        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Quantity</label>
            <div class="col-md-3  additional-margin">
                <input type="text" class="form-control txt_TextBox small_txt_TextBox" name="txt_quantity" ng-model="txt_quantity" only-digits limit-to="6" autocomplete="off" required  >
            </div>
             <div class="col-md-4" ng-show="txt==3 || txt==5">
                <select id="dd_brandName" class="form-control dd_DropDown" name="dd_brandName" ng-model="dd_brandName" style="width: 145px; margin-left:-2px;">
                    <option value="0" ng-selected="selected">--Select Brand--</option>
                    <option ng-repeat="x in BrandList" value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
                <p ng-if="IsExpenseType == 1" class="errorMessage">Brand type is required.</p>
            </div>
        </div>


        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Transferred On</label>
            <div class="col-md-8">
                <md-datepicker ng-model="TransactionDate" md-placeholder="Enter date" md-min-date="minDate" md-max-date="maxDate" readonly id="datePicker" ></md-datepicker>
            </div>
        </div>
        
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Transfer To</label>
            <div class="col-md-8">
                <select  ng-show="txt!=4 && txt!=5" class="form-control dd_DropDown" name="dd_usernameTo" ng-model="dd_usernameTo" required>
                    <option value="" disabled selected>--Select--</option>
                    <option ng-repeat="x in UserList " value="{{x.Username}}" ng-show="txt==1">{{x.Username }}</option>
                    <option ng-repeat="x in TmpList" value="{{x.Name_}}" ng-show="((TrnType=='WH_WH_D' || TrnType=='WH_WH_F') && Role=='WAREHOUSEADMIN') ">{{x.Name_}}</option>
                    <option ng-repeat="x in WhList "  value="{{x.Name_}}" ng-show="((TrnType=='CTR_WH_F' || TrnType=='CTR_WH_D' || TrnType=='VEN_WH' ||txt==6 ) &&(Role!='WAREHOUSEADMIN' ) )">{{x.Name_}}</option>
                    <option ng-repeat="x in WhList "  value="{{x.Name_}}" ng-show="(( TrnType=='VEN_WH') &&(Role=='WAREHOUSEADMIN' ) )">{{x.Name_}}</option>
                    <option ng-repeat="x in TmpList "  value="{{x.Name_}}" ng-show="((TrnType=='WH_WH_D' || TrnType=='WH_WH_F') &&(Role!='WAREHOUSEADMIN'))">{{x.Name_}}</option>
                </select> 
                
                <select ng-show="txt==4 || txt==5" id="dd_usernameTo" class="form-control dd_DropDown" name="dd_usernameTo" ng-model="dd_usernameTo">
                    <option value="" ng-selected="selected">--Select--</option>
                    <option value="<?php echo $PartyType[0] ?>"><?php echo $PartyType[0] ?></option>
                    <option value="<?php echo $PartyType[1] ?>"><?php echo $PartyType[1] ?></option>
                    <option value="<?php echo $PartyType[2] ?>"><?php echo $PartyType[2] ?></option>
                    
                </select>
                
             
            </div>
        </div>
        
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Remarks</label>
            <div class="col-md-8">
                <textarea class="form-control txt_TextBox" name="txt_comments" limit-to="100" ng-model="txt_comments" required  ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)"></textarea>
                <p ng-show="StockTransfer.txt_comments.$invalid && !StockTransfer.txt_comments.$pristine" class="errorMessage">Comments are required.</p>
            </div>
        </div>
            
            <div class="col-md-12 form-group" ng-show="TrnType!='VEN_WH'">
                <label class="col-md-4 lbl_Label">Available Stock</label>
                {{Available}}
            </div>   
            
            <div class="col-md-12 form-group">
                 <div class="col-md-12 ">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="back()">&nbsp;
                <input type="submit" value="Submit For Approval" class="btn  btn_Button btn_form btn-width">
                 </div>
            </div>
      </div><!-- end of hide --> 
      
    </form>
    </div>
    <div class="col-md-12 form-group centerAlign" ng-show="hide=='true'">
        <div class="col-md-12 ">
            <input type="button" value="Cancel" class="btn btn_Button" ng-click="back()">
        </div>
    </div>     
    </div><!-- end of show -->

<div class="clearfix"></div>



<script type="text/javascript">

    $(document).ready(function (e) {
        $("#txt_trnDate").datepicker({
            dateFormat:  'dd-mm-yy ',
        });
    });

</script>

