
<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="myForm" class="paddingNone" ng-cloak  novalidate ng-submit="SaveWarehouseDetails(myForm.$valid)">
       
        <div ng-show="ShowForm ==1">
            <div class="col-md-12">
                <h4 class="centerAlign formHeaderTitle">{{text}} Warehouse</h4>
            </div>

            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6">State</label>
                <div class="col-md-8">
                    <select id="State" class="form-control dd_DropDown " name="State" ng-model="State" ng-change="GetDistrictList();" ng-disabled='model.isDisabled'>
                        <option value="0" ng-selected="selected">-Select-</option>
                        <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                    </select>
<!--                    <p ng-if="State == 1" class="errorMessage">State Name is required.</p>-->
                </div>
                
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6">District</label>
                <div class="col-md-8">
                    <select id="aname" class="form-control dd_DropDown " name="District" ng-model="District"  ng-disabled='model.isDisabled'>
                        <option value="0" ng-selected="selected">-Select-</option>
                        <option ng-repeat="x in DistrictList" value="{{x.Name_}}">{{x.Name_}}</option>
                    </select>
<!--                    <p ng-if="State == 1" class="errorMessage">District Name is required.</p>-->
                </div>
            </div> 
            
            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Warehouse Code (Eg. MUM_WH_01)</label>
                <div class="col-md-8">
                    <input autocomplete="off" type="text" class="form-control txt_TextBox text-upper"  name="wname" maxlength='15'  ng-model="wname" required  ng-keydown="[32].includes($event.keyCode) && onKeydown($event)" ng-disabled='model.isDisabled'>  
                </div>
            </div>
            
            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Address</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox" name="txt_comments" limit-to="100" ng-model="txt_comments" placeholder='Enter Address'></textarea>
                     <p ng-show="myForm.txt_comments.$invalid && !myForm.txt_comments.$pristine" class="errorMessage">Comments are required.</p>
                </div>
            </div>
            
            <div class="col-md-12 form-group">
                 <div class="col-md-12 ">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToButton()">&nbsp;
                <input type="submit" value="Save" class="btn btn_Button btn_form">
                 </div>
            </div>   
        </div>
    </form>
<div class="clearfix"></div> 