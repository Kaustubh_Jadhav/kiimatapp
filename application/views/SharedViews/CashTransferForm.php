<?php $RecipientAccountType = unserialize(RecipientAccountType) ;
      $Username= $this->session->userdata('Username');
        ?>


<div class="col-md-12" ng-cloak>
    <div class="col-md-12  alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12   alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
  
    
    <form name="myForm" ng-cloak class='myForm paddingNone' id='FormDiv'  novalidate ng-submit="SaveCashTransfer(myForm.$valid)">
        <div class="col-md-12" >
            
<!--            <a href='#Form_Div' id="back"></a>-->
            <h2 class="centerAlign  formHeaderTitle" id='Form_Div'>Cash Transfer Entry</h2>
        </div>
        <div ng-show="Available!=0">
      
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label ">Cash In-Hand</label>
            <p class="col-md-4 ">
                <i class=" rupe fas fa-rupee-sign " style='font-size:13px' ></i>&nbsp; {{Available |number : 0}} /-</span>
            </p>
        </div>
            
        <div class="col-md-12 form-group" ng-show="Available >0 ">
            <label class="col-md-4 lbl_Label ">Recipient</label>
            <div class="col-md-8 ">
                <select id="dd_expnsType" class="form-control dd_DropDown" name="dd_expnsType" ng-model="dd_expnsType" ng-change="displayusers();">
                    <option value="0" ng-selected="selected">--Select--</option>
                    <option value="<?php echo $RecipientAccountType[0] ?>"><?php echo $RecipientAccountType[0] ?></option>
                    <option value="<?php echo $RecipientAccountType[1] ?>"><?php echo $RecipientAccountType[1] ?></option>
                    <option value="<?php echo $RecipientAccountType[2] ?>" ng-show='IsEnable ==false'><?php echo $RecipientAccountType[2] ?></option>
                    
                </select>
                <p ng-if="dd_expnsType == 1" class="errorMessage">Recipient Account Type is required.</p>
            </div>
        </div>
            
        <div class="col-md-12 form-group"  ng-show="dd_expnsType=='<?php echo $RecipientAccountType[2] ?>'">
            <label class="col-md-4 lbl_Label ">Transferred To </label>
            <div class="col-md-8 ">
                <select id="dd_userType" class="form-control dd_DropDown" name="dd_userType" ng-model="dd_userType">
                    <option value="" disabled selected>--Select--</option>
                    <option ng-repeat="x in TempList |filter : { Username: '!<?php echo $Username ?>'}" value="{{x.Username}}">{{x.Username | uppercase}}</option>
                </select>
<!--                <p ng-show='((dd_expnsType == "INTERNAL_USERS") && (dd_userType==null)) ' class="errorMessage">*Select User Name.</p>-->
            </div>
        </div>   
          
        <div class="row-container" ng-show="show=='true'">   
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label"> Transferred On</label>
            <div class="col-md-8">
                <md-datepicker ng-model="txt_trnDate" md-placeholder="Enter date" md-min-date="minDate" md-max-date="maxDate" readonly id="datePicker" ></md-datepicker>
            </div>
        </div>
            
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label ">Amount Transferred</label>
            <div class="col-md-3 ">
                <input autocomplete="off" type="text" class="form-control txt_TextBox small_txt_TextBox" name="txt_amountSpent" ng-model="txt_amountSpent" only-digits limit-to="7" ng-model="txt_amountSpent" required>
                <p ng-show="myForm.txt_amountSpent.$invalid && !myForm.txt_amountSpent.$pristine" class="errorMessage">Enter Amount for Cash Transfer.</p>
            </div>
        </div>
      
        
        
       
        <div class="col-md-12 form-group">
            <label class="col-md-4 lbl_Label">Comments</label>
            <div class="col-md-8 ">
                <textarea class="form-control txt_TextBox " name="txt_comments" limit-to="100" ng-model="txt_comments" placeholder='Enter Transaction Reference Notes' required></textarea>
                <p ng-show="myForm.txt_comments.$invalid && !myForm.txt_comments.$pristine" class="errorMessage">Comments are required.</p>
            </div>
        </div>
       
       
        <div class="col-md-12 form-group" >
            <div class="col-md-12 ">
                 <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToCash()">&nbsp;
                 <input type="submit" value="Submit For Approval" class="btn  btn_Button btn_form btn-width">
            </div>
            </div>
        </div>
        </div>
    </form>
  

<div class="clearfix"></div>
