<div ng-cloak>
     <h2 class=" formHeaderTitle"> MIS Entry</h2>
<div class="panel-group">
        <div class="panel panel-default">
             <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                         <label class="col-md-2 lbl_Label">Appliance Type</label>
                         <div class="col-md-3 form-group">
                            <select class="form-control select2" ng-model="Appliance"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                         
                        <div class=" form-group col-md-3">
                            <label class="col-md-1">From</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="from_Date" id="datePicker"  md-placeholder="Select date" md-max-date="maxmisDate" readonly flex ></md-datepicker>
                             </div>
                        </div>
                          
                        <div class=" form-group col-md-4">
                            <label class="col-md-1">To</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="to_Date" id="datePicker1"  md-placeholder="Select date" md-max-date="maxmisDate"  readonly flex ></md-datepicker>
                             </div>
                        </div>
                    </div>
                   
                    <div class="col-md-12 paddingNone margin-filter">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="GetDistributionList();">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>

<div class="col-md-12 centerAlign listView" ng-if="DistributionList.length > 0">
     <div class="tableScroll">
    <table class="table table-striped table-responsive-md" ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th>Id</th>
                <th ng-click='sortColumn("Username")' ng-class='sortClass("Username")'>Username</th>
                <th ng-click='sortColumn("ApplianceName")' ng-class='sortClass("ApplianceName")'>Appliance Name</th>
                <th ng-click='sortColumn("QtyDistributed")' ng-class='sortClass("QtyDistributed")'>Qty Distributed</th>
                <th ng-click='sortColumn("QtyReplaced")' ng-class='sortClass("QtyReplaced")'>Qty Replaced</th>
                <th ng-click='sortColumn("QtyBD")' ng-class='sortClass("QtyBD")'>Qty BD</th>
                <th >Comments</th>
                <th ng-click='sortColumn("CashReceived")' ng-class='sortClass("CashReceived")'>Cash Received (&#8377;)</th>
                <th ng-click='sortColumn("EntryDate")' ng-class='sortClass("EntryDate")'>Entry Date</th>
                <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                <th ng-click='sortColumn("CreatedBy")' ng-class='sortClass("CreatedBy")'>Created By</th>
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in DistributionList| filter:search | orderBy:sortBy:reverse | itemsPerPage:10">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x.Username}}</td>
                <td>{{ x.ApplianceName}}</td>
                <td>{{ x.QtyDistributed}}</td>
                <td>{{ x.QtyReplaced}}</td>
                <td>{{ x.QtyBD}}</td>
                <td>{{ x.Comments}}</td>
                <td class='align'>{{x.CashReceived | INR}}</td>
                <td>{{x.EntryDate}}</td>
                <td>{{x.CreatedOn| dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                <td>{{x.CreatedBy}}</td>
            </tr>
        </tbody>
    </table>
     </div>
    <dir-pagination-controls
        max-size="5"
        direction-links="true"true
        boundary-links="true" on-page-change='indexCount(newPageNumber)'>
    </dir-pagination-controls>
    <div class="clearfix"></div>
</div>
<div class="col-md-12 centerAlign listView">
        <p>{{noRecord}}</p>
    </div>
</div>
<script>
     $('.select2').select2({ width: '100%' });
</script>

