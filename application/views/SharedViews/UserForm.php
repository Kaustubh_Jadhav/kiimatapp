
<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form ng-cloak name="myForm" novalidate class='paddingNone' ng-submit="SaveUserDetails(myForm.$valid)">
        
         <div class="col-md-12 form-group centerAlign" ng-show="IsDataShow == 1">
             
            <div class="col-md-12">
                <h2 class="centerAlign  formHeaderTitle">Select Role</h2>
            </div>
            <div class=" form-group" ng-show="Role=='DISTRICTADMIN'">
            <input type="button" value="COUNTER"  class="btn btn_Button Stk_btn_Button col-md-12 "   ng-click="GetUser('COUNTER' , 1)">
            </div>
            
            <div class=" form-group" ng-show="Role=='SUPERADMIN'">
            <input type="button" value="WAREHOUSE ADMIN "  class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetUser('WAREHOUSEADMIN', 2)">
            </div>
            
            <div class=" form-group" ng-show="Role=='STATEADMIN'">
            <input type="button" value="DISTRICT ADMIN"  class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetUser('DISTRICTADMIN' ,3)">
            </div>
            
            <div class=" form-group" ng-show="Role=='SUPERADMIN'">
            <input type="button" value="STATE ADMIN" class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetUser('STATEADMIN',  4)">
            </div>
            
            <div class="form-group" ng-show="Role=='SUPERADMIN'">
            <input type="button" value="SUPER ADMIN" class="btn btn_Button Stk_btn_Button col-md-12" ng-click="GetUser('SUPERADMIN',  5)">
            </div>
           
        </div>
        
        <div ng-show="ShowForm ==1">
        <div class="col-md-12">
            <h4 class="centerAlign formHeaderTitle">{{text}}</h4>
        </div>
       

        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 ">Username</label>
            <div class="col-md-8">
                <input type="text" class="form-control txt_TextBox " name="Uname" ng-model="Uname"  ng-model="Uname"  maxlength='15' autocomplete="off" >
                <p ng-show="myForm.Uname.$invalid && !myForm.Uname.$pristine" class="errorMessage">User Name is required.</p>
            </div>
        </div>
        <div class="col-md-12 form-group form-bottom" ng-show="RoleType!='SUPERADMIN'">
            <label class="col-md-4 col-xs-6 ">{{label}}</label>
            <div class="col-md-8">
                <select id="State" class="form-control dd_DropDown " name="State" ng-model="State" ng-change="GetDistrictList();" ng-disabled='{{StateDisable}}'>
                    <option value="0" ng-selected="selected">-Select-</option>
                    <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
<!--                <p ng-if="State == 1" class="errorMessage">Select State.</p>-->
            </div>
             <!--<div class="col-md-4" ng-show="RoleType!='SUPERADMIN' && RoleType!='STATEADMIN'">
                <select id="District" class="form-control dd_DropDown small_dropdowns" name="District" ng-model="District" ng-change="getwhList()">
                    <option value="0" ng-selected="selected">--Select--</option>
                       <option ng-repeat="x in DistrictList " value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
                <p ng-if="District == 1" class="errorMessage">*Select District.</p>
            </div>-->
        </div>
            
        <div class="col-md-12 form-group " ng-show="RoleType!='SUPERADMIN' && RoleType!='STATEADMIN'">
            <label class="col-md-4 col-xs-6 "></label>
            <div class="col-md-8">
                <select id="District" class="form-control dd_DropDown" name="District" ng-model="District" ng-change="getwhList();" ng-disabled='{{DistrictDisable}}'>
                    <option value="0" ng-selected="selected">--Select--</option>
                       <option ng-repeat="x in DistrictList " value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
<!--                <p ng-if="District == 1" class="errorMessage">Select District.</p>-->
            </div>
        </div>   
       
        <div class="col-md-12 form-group form-top" ng-show="RoleType=='WAREHOUSEADMIN'">
            <label class="col-md-4 col-xs-6 "></label>
            <div class="col-md-8">
                <select id="WareHouse" class="form-control dd_DropDown" name="WareHouse" ng-model="WareHouse">
                       <option value="0" ng-selected="selected">--Select--</option>
                       <option ng-repeat="x in TmpList " value="{{x.Name_}}">{{x.Name_}}</option>
                </select>
<!--                <p ng-if="WareHouse == 0 && !myForm.WareHouse.$pristine" class="errorMessage">Select WareHouse.</p>-->
            </div>
        </div>
         
            <div class="col-md-12 form-group ">
                <label class="col-md-4 col-xs-6">Full Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox" name="Name"  ng-model="Name" autocomplete="off" maxlength='30'>
                    <p ng-show="myForm.Name.$invalid && !myForm.Name.$pristine" class="errorMessage">Name is required.</p>
                </div>
            </div>
        
             <div class="clearfix"></div>
            <div class="col-md-12 form-group">
                  <div class="col-md-12">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToButton()">&nbsp;
                <input type="submit" value="Create" class="btn btn_Button btn_form">
                  </div>
            </div>   
        </div>
    </form>
   
