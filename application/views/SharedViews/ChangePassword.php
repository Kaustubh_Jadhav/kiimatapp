<script src="<?php echo base_url(); ?>/script/angular/ChangePassword.js" type="text/javascript"></script> 
<div class="col-md-12" ng-controller="ChangePassword">
<div class="col-md-8 form-view">
<div  ng-cloak>
     
    <div class="col-md-4 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-4 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
        <form name="EditForm"  class="paddingNone" novalidate ng-submit="EditUserDetails(EditForm.$valid)">
            <div class="col-md-12 form-group ">
                <div class='col-md-12'>
                <h4 class="formHeaderTitle change-password">Change Password</h4>
                </div>
            </div>
        
        <div class="col-md-12 form-group">
            <label class="col-md-2 col-xs-8 ">Username</label>
            <div class="col-md-4">
                <label class="lbl_Label">{{Username}}</label>
            </div>
        </div> 

        <div class="col-md-12 form-group">
            <label class="col-md-2 col-xs-8 ">New Password</label>
            <div class="col-md-4">
                <input type="password" class="form-control txt_TextBox" name="password"  ng-model="password" autocomplete="off" maxlength='15'>
                <p ng-show="EditForm.password.$invalid && !EditForm.password.$pristine" class="errorMessage">Password is required.</p>
            </div>
        </div>
        <div class="col-md-12 form-group">
            <label class="col-md-2 col-xs-8 ">Confirm Password</label>
            <div class="col-md-4">
                <input type="password" class="form-control txt_TextBox" name="cnfpassword"  ng-model="cnfpassword" autocomplete="off" maxlength='15'>
                <p ng-show="EditForm.cnfpassword.$invalid && !EditForm.cnfpassword.$pristine" class="errorMessage">Password is required.</p>
            </div>
        </div>
        
         <div class="col-md-12 form-group">
                <div class="col-md-12 ">
                <input type="submit" value="Save" class="btn btn_Button btn_form">
                </div>
            </div>
         <div class="clearfix"></div>
        </form>
    </div>
<!--    
    <input type="file" file-model="myFile" multiple />
    <input type="button" value="button" class="btn btn_Cancel btn_form" ng-click="uploadFile()">-->
   
<div class="clearfix"></div>
</div>
</div>
<div class="clearfix"></div>
    
    
 





