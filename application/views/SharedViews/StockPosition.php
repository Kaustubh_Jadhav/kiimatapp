<div ng-cloak>
     <h2 class=" formHeaderTitle">Stock Position Report</h2>
     <p>Report Generated On &nbsp; <?php  echo CURRDATE ?></p>
<div class="panel-group"  ng-show="panel==true">
        <div class="panel panel-default">
            <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">Appliance</label>
                        <div class="col-md-3 form-group">
                            <select class="form-control select2" ng-model="Appliance" ng-change="GetStock()"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        
                        <div class=" form-group col-md-3">
                            <input type="text" class="form-control txt_TextBox" ng-change="filter();" ng-model='filterValue' placeholder='Search User'/>
                        </div>
                        
                    </div>
                     <div class="col-md-12 paddingNone margin-filter">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-2">
                                <input type="button" value="Go" class="btn btn_Button" ng-click=" filter();GetStock();">
                            </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>

<!--<div class="col-md-12" ng-if="StockList.length > 0">-->

<!--<strong>Scrolling Vertically</strong> {{ gridApi.grid.isScrollingVertically }}
      <br>
<strong>Scrolling Horizontally</strong> {{ gridApi.grid.isScrollingHorizontally }}-->
  
<div ng-show="gridOptions.data.length >0">   
<div ui-grid="gridOptions" ui-grid-grouping  ui-grid-selection ui-grid-exporter  class='grid'  ui-grid-infinite-scroll ui-grid-auto-resize></div>
</div>

<!--</div>-->
<div class="col-md-12 centerAlign listView" ng-show="!gridOptions.data.length">
        <p>{{noRecord}}</p>
    </div>
</div>

<script>
     $('.select2').select2({ width: '100%' });
</script>


