<div class="Approvalmargin"  ng-cloak>
     <h2 class=" formHeaderTitle">PENDING STOCK TRANSFERS</h2>
     <!--<div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="GetApprovalList();">
      </div>-->
     
    <div class="panel-group" ng-show="show==true">
        <div class="panel panel-default">
            <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="State"> 
                                <option value="0">--All--</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="District">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone filter-group">
                        <label class="col-md-2 lbl_Label">User</label>
                        <div class="col-md-4 filter-group">
                            <select class="form-control" ng-model="Users">
                                <option value="0">--All--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 centerAlign listView" ng-if="StockApprovalList.length > 0">
        <div class="tableScroll" style="overflow:auto;">
            <table class="table table-striped table-responsive-md" ng-table="tableParams">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th ng-click='sortColumn("TrnDate")' ng-class='sortClass("TrnDate")'>Transaction Date</th>
                        <th ng-click='sortColumn("StockType")' ng-class='sortClass("StockType")'>Stock Type</th>
                        <th ng-click='sortColumn("BrandName")' ng-class='sortClass("BrandName")'>Brand Name</th>
                        <th ng-click='sortColumn("ApplianceName")' ng-class='sortClass("ApplianceName")'>Appliance Name</th>
                        <th ng-click='sortColumn("Username_From")' ng-class='sortClass("Username_From")'>Username From</th>
                        <th ng-click='sortColumn("TrnType")' ng-class='sortClass("TrnType")'>Trn Type</th>
                        <th ng-click='sortColumn("Username_To")' ng-class='sortClass("Username_To")'>Username To</th>
                        <th ng-click='sortColumn("Qty")' ng-class='sortClass("Qty")'>Quantity</th>
                        <th>Comments</th>
                        <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                        <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in StockApprovalList | filter:search | orderBy:sortBy:reverse|  itemsPerPage:10" pagination-id="page2">
                        <td>{{ $index + 1 + serial}}</td>
                        <td>{{ x.TrnDate | dateToISO | date:'dd-MMM-yyyy'}}</td>
                        <td>{{ x.StockType}}</td>
                        <td>{{ x.BrandName}}</td>
                        <td>{{ x.ApplianceName}}</td>
                        <td>{{ x.Username_From}}</td>
                        <td>{{ x.TrnType}}</td>
                        <td>{{ x.Username_To}}</td>
                        <td>{{ x.Qty}}</td>
                        <td>{{ x.Comments}}</td>
                        <td>{{ x.Status |uppercase}}</td>
                        <td>{{ x.CreatedOn | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                        <td ng-if="x.Status.toLowerCase() == 'pending'">
                            <input type="button" value="Approve" class="action_btn btn btn_Button btn-small" ng-click="OpneAction(1, x.Id); save('stockview');">                           
                        </td>
                        <td ng-if="x.Status.toLowerCase() == 'pending'">
                            <input type="button" value="Reject" class="action_btn btn btn_Button btn-small" ng-click="OpneAction(2, x.Id); save('stockview');">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <dir-pagination-controls
            max-size="5"
            direction-links="true"
            boundary-links="true"  pagination-id="page2" on-page-change='indexCount(newPageNumber)'>
        </dir-pagination-controls>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 centerAlign listView" >
        <p>{{stockRecord}}</p>
    </div>
</div>



