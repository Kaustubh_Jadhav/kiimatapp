
<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="myForm" class="paddingNone" ng-cloak novalidate ng-submit="SaveBrandDetails(myForm.$valid)">
       
        <div ng-show="ShowForm ==1">
            <div class="col-md-12">
                <h4 class="centerAlign formHeaderTitle">Add Brand</h4>
            </div>

            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Appliance Type</label>
                <div class="col-md-8">
                    <select id="aname" class="form-control dd_DropDown " name="aname" ng-model="aname">
                        <option value="0" ng-selected="selected">-Select-</option>
                        <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                    </select>
<!--                    <p ng-if="State == 1" class="errorMessage">Appliance Name is required.</p>-->
                </div>
                
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Brand Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox text-upper" name="bname" ng-model="bname"  ng-model="bname"  only-letter maxlength='15' autocomplete="off">
                    <p ng-show="myForm.bname.$invalid && !myForm.bname.$pristine" class="errorMessage">Brand Name is required.</p>
                </div>
            </div> 
            <div class="col-md-12 form-group">
                <div class="col-md-12 ">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToButton()">&nbsp;
                <input type="submit" value="Save" class="btn btn_Button btn_form">
                </div>
            </div>   
        </div>
    </form>