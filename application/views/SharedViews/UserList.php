<div ng-cloak>
  <h2 class=" formHeaderTitle">Users</h2>
<div class="panel-group">
        <div class="panel panel-default">
            <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                        <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                     
                </div>
              
            </a>
            
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Statesearch" ng-change='fetchDistrictList()'  ng-disabled='{{sdisabled}}'> 
                                <option value="0" >--All--</option>
                                <option ng-repeat="x in StateListforsearch" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group" >
                            <select class="form-control select2" ng-model="Districtsearch" ng-change='fetchwhList()'  ng-disabled='{{ddisabled}}'>
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictListforsearch" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone form-group">
                        <label class="col-md-2 lbl_Label">Warehouse</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="WareHousesearch" ng-disabled='{{wdisabled}}'>
                               <option value="0" >--All--</option>
                               <option  ng-repeat="x in TmpLists| orderBy:'Name_':asc" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">Role</label>
                        <div class="col-md-4 form-group">
                           <select class="form-control select2" ng-model="userRole">
                               <option value="">--All--</option>
                               <option ng-disabled='{{udisabled}}' ng-repeat="x in RoleList" value="{{x.RoleType}}">{{x.RoleType}}</option>
                           </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="GetUserList();">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
<!--<div class="divHeight50"></div>-->


<div class="col-md-12 centerAlign listView" ng-if="UserList.length > 0">
    <div class="col-md-12">
        <div class="col-md-3 right-btn">
        <input type="button" value="Add User" class="action_btn btn btn_Button" ng-click="OpneAction();">
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="tableScroll">
    <table class="table table-striped table-responsive-md" ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th >Id</th>
                <th ng-click='sortColumn("RoleType")' ng-class='sortClass("RoleType")'>RoleType</th>
                <th ng-click='sortColumn("Username")' ng-class='sortClass("Username")'>Username</th>
                <th ng-click='sortColumn("Name_")' ng-class='sortClass("Name_")'>Name_</th>
                <th ng-click='sortColumn("StateName")' ng-class='sortClass("StateName")'>State Name</th>
                <th ng-click='sortColumn("DistrictName")' ng-class='sortClass("DistrictName")'>District Name</th>
                <th ng-click='sortColumn("WarehouseName")' ng-class='sortClass("WarehouseName")'>Warehouse Name</th>
                <th ng-click='sortColumn("m_dt")' ng-class='sortClass("m_dt")'>Last Modified On</th>
                <th ng-click='sortColumn("m_md")' ng-class='sortClass("m_md")'>Last Modified By</th>
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in UserList| filter:search |orderBy:sortBy:reverse | itemsPerPage:10">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x.RoleType}}</td>
                <td>{{ x.Username |lowercase}}</td>
                <td>{{ x.Name_}}</td>
                <td>{{ x.StateName}}</td>
                <td>{{ x.DistrictName}}</td>
                <td>{{ x.WarehouseName}}</td>
                <td>{{x.m_dt| dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                <td>{{x.m_md}}</td>
                <td >
                    <input type="button" value="Reset" class=" btn btn_Button action_btn btn-small" ng-click="EditAction(x.Id);">                           
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    
    <dir-pagination-controls
        max-size="5"
        direction-links="true"true
        boundary-links="true" on-page-change='indexCount(newPageNumber)'>
    </dir-pagination-controls>
    <div class="clearfix"></div>
</div>
<div class="col-md-12 centerAlign listView" >
         <p>{{noRecord}}</p>
    </div>
</div>

<script>
     $('.select2').select2({ width: '100%' });
 </script>      

