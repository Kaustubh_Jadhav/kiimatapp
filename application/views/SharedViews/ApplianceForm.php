


<div class="col-md-12" ng-cloak>
    <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
        {{ErrorMessage}}
    </div>
    <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
</div>
    <form name="myForm" novalidate ng-cloak ng-submit="SaveApplianceDetails(myForm.$valid)">
       
        <div ng-show="ShowForm ==1">
            <div class="col-md-12">
                <h4 class="centerAlign formHeaderTitle">{{text}} Appliance</h4>
            </div>

            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Appliance Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox text-upper" name="name" ng-model="name"  ng-model="name"  only-letter maxlength='15' autocomplete="off"  ng-disabled='model.isDisabled' >
                    <p ng-show="myForm.name.$invalid && !myForm.name.$pristine" class="errorMessage">Appliance Name is required.</p>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 col-xs-6 ">Rate/Pcs</label>
                <div class="col-md-8">
                    <input autocomplete="off" type="text" class="form-control txt_TextBox" name="rate" ng-model="rate" only-digits limit-to="6" ng-model="rate" required>
                    <p ng-show="myForm.rate.$invalid && !myForm.rate.$pristine" class="errorMessage">Rate/Pcs is required.</p>
                </div>
            </div> 
            <div class="col-md-12 form-group">
                <div class="col-md-12 form-group">
                <input type="button" value="Cancel" class="btn btn_Cancel btn_form" ng-click="backToButton()">&nbsp;
                <input type="submit" value="Save" class="btn btn_Button btn_form">
                </div>
                </div>   
        </div>
    </form>
    

