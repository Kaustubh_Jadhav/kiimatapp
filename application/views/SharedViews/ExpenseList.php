<div ng-cloak>
     <h2 class=" formHeaderTitle">Expenses</h2>
    <div class="panel-group" ng-show='panel== true'>
        <div class="panel panel-default" >
            <a class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone filter-group margin-filter">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="State" ng-disabled="IsStateEnable" ng-change="GetDistrictList()"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="District" ng-disabled="IsDistrictEnable" ng-change="GetUserList();">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 paddingNone filter-group margin-filter">
                        <label class="col-md-2 lbl_Label">Requested By</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="User" ng-disabled="IsUserEnable">
                                <option value="0">--All--</option>
                                <option ng-repeat="x in UserList" value="{{x.Username}}">{{x.Username}}</option>
                            </select>
                        </div>
                        
                        <label class="col-md-2 lbl_Label">Status</label>
                        <div class="col-md-4 form-group">
                            <!--<input type="text" class="form-control txt_TextBox" ng-model="search">-->
                            <select class="form-control select2" ng-model="Status">
                                <option value="0" ng-selected="selected">--ALL--</option>
                                <option value="<?php echo TRN_STATUS_PENDING ?>"><?php echo TRN_STATUS_PENDING ?></option>
                                <option value="<?php echo TRN_STATUS_APPROVED ?>"><?php echo TRN_STATUS_APPROVED ?></option>
                                <option value="<?php echo TRN_STATUS_REJECTED ?>"><?php echo TRN_STATUS_REJECTED ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-12 paddingNone filter-group margin-filter">
                        <label class="col-md-2 lbl_Label">Expense Type</label>
                        <div class="col-md-4 form-group">
                           <select id="expnsType" class="form-control dd_DropDown select2" name="expnsType" ng-model="expnsType">
                            <option value="0" ng-selected="selected">--All--</option>
                            <option value="COMMISSION EXPENSE">COMMISSION EXPENSE</option>
                            <option value="ELECTRICITY BILL">ELECTRICITY BILL </option>
                            <option value="INSURANCE">INSURANCE</option>
                            <option value="LOADING AND UNLOADING CHARGE">LOADING AND UNLOADING CHARGE</option>
                            <option value="MARKETING / PROMOTIONS">MARKETING / PROMOTIONS</option>
                            <option value="MOBILE AND TELEPHONE BILL">MOBILE AND TELEPHONE BILL</option>
                            <option value="OFFICE RENT">OFFICE RENT</option>
                            <option value="PRINTING / STATIONERY">PRINTING / STATIONERY</option>
                            <option value="SALARY">SALARY</option>
                            <option value="SECURITY DEPOSIT">SECURITY DEPOSIT</option>
                            <option value="STATIONARY EXPENSES">STATIONARY EXPENSES</option>
                            <option value="TELE / MOBILE">TELE / MOBILE</option>
                            <option value="TRAVEL">TRAVEL</option>
                            <option value="TRANSPORTATION CHARGE">TRANSPORTATION CHARGE</option>
                            <option value="TRAVELLING EXPENSES">TRAVELLING EXPENSES</option>
                            <option value="WAREHOUSE RENT">WAREHOUSE RENT</option>
                            <option value="WATER BILL">WATER BILL</option>
                            <option value="ANY OTHER">OTHER</option>
                           </select>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12 paddingNone filter-group margin-filter">
                        
                        <label class="col-md-1 lbl_Label">From</label>
                        <div class=" form-group col-md-2">
                             <md-datepicker ng-model="from_Date" id="datePicker"  md-placeholder="Select date"  md-max-date="maxDate" readonly flex ></md-datepicker>
                        </div>
                        <label class="col-md-1 lbl_Label">To</label>
                        <div class=" form-group col-md-2">
                             <md-datepicker ng-model="to_Date" id="datePicker1"  md-placeholder="Select date" md-max-date="maxDate"  readonly flex></md-datepicker>
                        </div>
                        
                   </div>
                  
                    <div class="col-md-12 paddingNone margin-filter">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                         <input type="button" value="Go" class="btn btn_Button" ng-click="getDate(); GetExpenseList()">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 centerAlign listView" ng-if="ExpenseList.length > 0">
            
<div class="col-md-12">
        <div class="col-md-3 right-btn" >
             <i class="fas fa-file-excel excel-icon right-btn"  id="btnExport" onclick="Export()" ng-show='ExpenseList.length > 0'></i>
        <!--<input type="button" class="action_btn btn btn_Button" id="btnExport" value="Export" onclick="Export()" ng-show='ExpenseList.length > 0'>-->
        </div>
</div>
   <div class="clearfix"></div>     
        <div class="tableScroll">
            <table class="table table-striped table-responsive-md" ng-table="tableParams">
                <thead class="thead-dark">
                    <tr>
                        <th ng-click='sortColumn("Transaction Date")' ng-class='sortClass("Transaction Date")'>Transaction Date</th>
                        <th ng-click='sortColumn("Username")' ng-class='sortClass("Username")'>Username</th>
                        <th ng-click='sortColumn("Expense Type")' ng-class='sortClass("Expense Type")'>Expense Type</th>
                        <th ng-click='sortColumn("Amount Spent")' ng-class='sortClass("Amount Spent")'>Amount Spent(&#8377;)</th>
                        <th>Comments</th>
                        <th ng-click='sortColumn("Status")' ng-class='sortClass("Status")'>Status</th>
                        <th ng-click='sortColumn("Username Approver")' ng-class='sortClass("Username Approver")'>Username Approver</th>
                        <th ng-click='sortColumn("Action TakenOn")' ng-class='sortClass("Action TakenOn")'>Action Taken On</th>
                        <th ng-click='sortColumn("Approver Remarks")' ng-class='sortClass("Approver Remarks")'>Approver Remark</th>
                        <th ng-click='sortColumn("Created On")' ng-class='sortClass("Created On")'>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in ExpenseList| filter:search | orderBy:sortBy:reverse | itemsPerPage:10">
                        <td>{{ x['Transaction Date'] | dateToISO | date:'dd-MMM-yyyy'}}</td>
                        <td>{{ x.Username}}</td>
                        <td>{{ x['Expense Type']}}</td>
                        <td class='align'>{{ x['Amount Spent'] | INR}}</td>
                        <td>{{ x.Comments}}</td>
                        <td>{{ x.Status |uppercase}}</td>
                        <td>{{ x['Username Approver']}}</td>
                        <td>{{ x['Action TakenOn']}}</td>
                        <td>{{ x['Approver Remarks']}}</td>
                        <td>{{ x['Created On'] | dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <dir-pagination-controls
            max-size="5"
            direction-links="true"true
            boundary-links="true" on-page-change='indexCount(newPageNumber)'>
        </dir-pagination-controls>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 centerAlign listView">
      <p>{{noRecord}}</p>
    </div>
</div>
<script>
   function Export() {
            var data = angular.copy($('[ng-controller="ExpenseController"]').scope().ExpenseList);
            var Username =  $('[ng-controller="ExpenseController"]').scope().Username;
            var parse_data= JSON.stringify(data, function(key, value) { 
            
                    if(value == null || value == undefined) {
                         return ""
                    }
                    else{
                        return value
                    }
          });
          
            var send= JSON.parse(parse_data);
            var date=  new Date();
//            var format_date = date.getFullYear() +""+ (date.getMonth()+1) +""+
//                                + date.getDate() +""+ date.getHours() + ':' + date.getMinutes() + ':' 
//                                + date.getSeconds();
//                                
           var format_date = date.getFullYear() +""+ (date.getMonth()+1) +""+
                                + date.getDate();
            //var title="ExpenseList" +"-"+ Username+ "-" +format_date;
           var title="ExpenseList" +"-"+ Username+format_date;
           $("#el").excelexportjs({
                containerid: null, 
                datatype:'json',
                dataset: send,
                columns: getColumns(send),
                returnUri:false,
                worksheetName: title,
                //filename:"ExpenseList" +Username+ new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
                encoding: "utf-8",
                locale:'en-US'
            });
            
        }
        
     $('.select2').select2({ width: '100%' });
     
//      function Captcha(){
//            var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
//            var i;
//            for (i=0;i<6;i++){
//                var a = alpha[Math.floor(Math.random() * alpha.length)];
//                var b = alpha[Math.floor(Math.random() * alpha.length)];
//                var c = alpha[Math.floor(Math.random() * alpha.length)];
//                var d = alpha[Math.floor(Math.random() * alpha.length)];
//                var e = alpha[Math.floor(Math.random() * alpha.length)];
//                var f = alpha[Math.floor(Math.random() * alpha.length)];
//            }
//            var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f ;
////            document.getElementById("mainCaptcha").value = code;
//            document.getElementById("mainCaptcha").innerHTML  = code;
//            var colors = ["#B40404", "#beb1dd", "#b200ff", "#faff00", "#0000FF", "#FE2E9A", "#FF0080", "#2EFE2E", ];
//            var rand = Math.floor(Math.random() * colors.length);
//            //$('#mainCaptcha').css("background-color", colors[rand]);
//
//        }
       
// function CaptchaCall(){
//          $.get('<?php echo base_url().'Captcha/index'; ?>', function(data){
//            $('#captImg').html(data);
//        });
//     }  
// $(document).ready(function(){
//      $.get('<?php echo base_url().'Captcha/index'; ?>', function(data){
//            $('#captImg').html(data);
//        });
//        
//    $('.refreshCaptcha').on('click', function(){
//        $.get('<?php echo base_url().'Captcha/index'; ?>', function(data){
//            $('#captImg').html(data);
//        });
//    });
//});
 </script>      

