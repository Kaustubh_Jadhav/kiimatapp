<div ng-cloak>
        <h2 class=" formHeaderTitle">Daily Sales Report</h2>
        <p>Report Generated On &nbsp; <?php  echo CURRDATE ?></p>
<div class="panel-group">
        <div class="panel panel-default">
            <a  class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                          <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1"></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-1 lbl_Label">Appliance</label>
                        <div class="col-md-2 form-group">
                            <select class="form-control select2" ng-model="Appliance" ng-change="FetchSaleSummary()"> 
                                <option value="0">--All--</option>
                                <option ng-repeat="x in ApplianceList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                       
                        <div class=" form-group col-md-3">
                            <label class="col-md-1">From</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="from_Date" id="datePicker"  md-placeholder="Select date" md-max-date="txt_trnDate" readonly flex ></md-datepicker>
                             </div>
                        </div>
                          
                        <div class=" form-group col-md-3">
                            <label class="col-md-1">To</label>
                            <div class="col-md-2 form-group">
                             <md-datepicker ng-model="to_Date" id="datePicker1"  md-placeholder="Select date" md-max-date="txt_trnDate" readonly flex ></md-datepicker>
                             </div>
                        </div>
                        
                        <div class=" form-group col-md-3">
                        <input type="text" class="form-control txt_TextBox" ng-model='filterValue' ng-change="filter();" placeholder='Search User'/>
                        </div>
<!--                          <p><button ng-click='filterBtn()'>Filter for "Company = Mixers"</button></p>
  
                        <p><input ng-model='filterValue' ng-change="filter()" placeholder='Search'/>-->
  
                        <div class="col-md-12 paddingNone margin-filter">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-2">
                                <input type="button" value="Go" class="btn btn_Button" ng-click=" filter();FetchSaleSummary();">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>

<!--<div class="col-md-12" ng-if="StockList.length > 0">-->
<div  ng-show="gridOptions.data.length >0">    
     <div ui-grid="gridOptions" ui-grid-grouping  ui-grid-selection ui-grid-exporter  class='grid' ui-grid-infinite-scroll ui-grid-auto-resize></div>
     <!--<div ui-grid="{ data: StockList }" ui-grid="gridOptions" ui-grid-grouping  class='myGrid'></div>-->
   
</div>
<div class="col-md-12 centerAlign listView" ng-show="!gridOptions.data.length">
        <p>{{noRecord}}</p>
    </div>
</div>

<script>
     $('.select2').select2({ width: '100%' });
</script>

