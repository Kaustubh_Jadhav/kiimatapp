<div ng-cloak>
     <h2 class=" formHeaderTitle">Warehouses</h2>
<div class="panel-group" ng-show='panel== true'>
        <div class="panel panel-default">
            <a  class='additional-filters' data-toggle="collapse" href="#collapse1"><div class="panel-heading">
                    <h4 class="panel-title">
                        Additional Filters
                        <i class="fas fa-caret-down caret-down" data-toggle="collapse" href="#collapse1" ></i>
                    </h4>
                </div></a>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12 filterBox">
                    <div class="col-md-12 paddingNone form-group margin-filter">
                        <label class="col-md-2 lbl_Label">State</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Statesearch" ng-change='fetchDistrictListforSearch()'  ng-disabled='{{sdisabled}}'> 
                                <option value="0" >--All--</option>
                                <option ng-repeat="x in StateList" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                        <label class="col-md-2 lbl_Label">District</label>
                        <div class="col-md-4 form-group">
                            <select class="form-control select2" ng-model="Districtsearch" ng-change='fetchwhList()'  ng-disabled='{{ddisabled}}'>
                                <option value="0">--All--</option>
                                <option ng-repeat="x in DistrictListSearch" value="{{x.Name_}}">{{x.Name_}}</option>
                            </select>
                        </div>
                    </div>
                  
                    <div class="col-md-12 paddingNone margin-filter">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Go" class="btn btn_Button" ng-click="FetchWarehouseList()">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>

<div class="col-md-12 centerAlign listView" ng-if="WarehouseList.length > 0">
    <div class="col-md-12">
        
        <div class="col-md-3 right-btn">
        <input type="button" value="Add Warehouse" ng-show='buttonShow==true' class="action_btn btn btn_Button" ng-click="OpneAction();">
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="tableScroll">
    <table class="table table-striped table-responsive-md" ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th >Id</th>
                <th ng-click='sortColumn("DistrictName")' ng-class='sortClass("DistrictName")'>District Name</th>
                <th ng-click='sortColumn("Name_")' ng-class='sortClass("Name_")'>Name</th>
                <th ng-click='sortColumn("Description")' ng-class='sortClass("Description")'>Description</th>
                <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                <th ng-click='sortColumn("CreatedBy")' ng-class='sortClass("CreatedBy")'>Created By</th>
                <th ng-click='sortColumn("ModifiedOn")' ng-class='sortClass("ModifiedOn")'>Modified On</th>
                <th ng-click='sortColumn("ModifiedBy")' ng-class='sortClass("ModifiedBy")'>Modified By</th>
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in WarehouseList| filter:search | orderBy:sortBy:reverse | itemsPerPage:10">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x.DistrictName}}</td>
                <td>{{ x.Name_}}</td>
                <td>{{ x.Description}}</td>
                <td>{{x.CreatedOn| dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                <td>{{x.CreatedBy}}</td>
                <td>{{x.ModifiedOn }}</td>
                <td>{{x.ModifiedBy}}</td>
                <td >
                    <input type="button" value="Edit" class="btn btn_Button action_btn btn-small" ng-click="EditAction(x.Id);">                           
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    
    <dir-pagination-controls
        max-size="5"
        direction-links="true"true
        boundary-links="true" on-page-change='indexCount(newPageNumber)'>
    </dir-pagination-controls>
    <div class="clearfix"></div>
</div>
<div class="col-md-12 centerAlign listView">
         <p>{{noRecord}}</p>
</div>
</div>

<script>
        $('.select2').select2({ width: '100%' });
 </script>      


