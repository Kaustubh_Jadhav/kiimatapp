<div ng-cloak>
     <h2 class=" formHeaderTitle">Appliances</h2>

<div class="clearfix"></div>


<div class="col-md-12 centerAlign listView"  ng-if="TmpList.length > 0">
    <div class="col-md-12">
        <div class="col-md-3 right-btn">
        <input type="button" value="Add Appliance" ng-show='buttonShow==true' class="action_btn btn btn_Button" ng-click="OpneAction();">
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="tableScroll">
    <table class="table table-striped table-responsive-md" ng-table="tableParams">
        <thead class="thead-dark">
            <tr>
                <th >Id</th>
                <th ng-click='sortColumn("Name_")' ng-class='sortClass("Name_")'>Name</th>
                <th ng-click='sortColumn("RatePerPc")' ng-class='sortClass("RatePerPc")'>Rate/Pcs (&#8377;)</th>
                <th ng-click='sortColumn("CreatedOn")' ng-class='sortClass("CreatedOn")'>Created On</th>
                <th ng-click='sortColumn("CreatedBy")' ng-class='sortClass("CreatedBy")'>Created By</th>
                <th ng-click='sortColumn("ModifiedOn")' ng-class='sortClass("ModifiedOn")'>Modified On</th>
                <th ng-click='sortColumn("ModifiedBy")' ng-class='sortClass("ModifiedBy")'>Modified By</th>
            </tr>
        </thead>
        <tbody>
            <tr dir-paginate="x in TmpList| filter:search | orderBy:sortBy:reverse| itemsPerPage:10">
                <td>{{ $index + 1 + serial}}</td>
                <td>{{ x.Name_}}</td>
                <td class='align'>{{ x.RatePerPc |INR}}</td>
                <td>{{x.CreatedOn| dateToISO | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                <td>{{x.CreatedBy}}</td>
                <td>{{x.ModifiedOn}}</td>
                <td>{{x.ModifiedBy}}</td>
                <td >
                    <input type="button" value="Edit" class="btn btn_Button action_btn btn-small" ng-click="EditAction(x.Id);">                           
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    
    <dir-pagination-controls
        max-size="5"
        direction-links="true"true
        boundary-links="true" on-page-change='indexCount(newPageNumber)'>
    </dir-pagination-controls>
    <div class="clearfix"></div>
</div>
<div class="col-md-12 centerAlign listView" >
         <p>{{noRecord}}</p>
    </div>
</div>

