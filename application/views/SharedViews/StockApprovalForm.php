
<div class="col-md-12"  ng-cloak>   
    
    <div class="col-md-12 alert alert-success" role="alert" ng-show="SuccessMessage != ''">
        {{SuccessMessage}}
    </div>
    <a href="#stockview" id="stockview"></a>
    <div ng-show="IsDataShow == 1" id="stockview">
        <div class="col-md-12">
            <h2 class="centerAlign formHeaderTitle">Details</h2>
        </div> 
        <div class="col-md-12 alert alert-danger" role="alert" ng-show="ErrorMessage != ''" ng-focus="ErrorMessage != ''">
            {{ErrorMessage}}
        </div>
        <input type="hidden" id="StockId" value="{{SelectedData['Id']}}">

        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone ">Transfer Type</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'CTR_WH_F'">RETURN OF FRESH GOODS TO WAREHOUSE</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'CTR_WH_D'">RETURN OF DEFECTIVE GOODS TO WAREHOUSE</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'WH_CTR_F'">RETURN OF FRESH GOODS TO COUNTER</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'WH_WH_F'">TRANSFER OF FRESH GOODS TO WAREHOUSE</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'WH_WH_D'">TRANSFER OF DEFECTIVE GOODS TO WAREHOUSE</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'WH_VEN_F'">RETURN OF FRESH GOODS TO VENDOR</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'WH_VEN_D'">RETURN OF DEFECTIVE GOODS TO VENDOR</label>
                <label class="lbl_Label" ng-if="SelectedData['TrnType'] == 'VEN_WH'">SUPPLY RECEIVED FROM VENDOR</label>
            </div>
        </div>

        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Appliance Type</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['ApplianceName'] + " / " + SelectedData['StockType']}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Transferred From</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['Username_From']}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone ">Transferred On</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['TrnDate']| dateToISO | date:'dd-MM-yyyy'}}</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Quantity</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['Qty']}} <span ng-if="SelectedData['BrandName'] != '0' && SelectedData['BrandName'] != ''">{{"/ " + SelectedData['BrandName']}}</span></label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <label class="col-md-4 col-xs-6 paddingNone">Sender Comments</label>
            <div class="col-md-8 col-xs-6">
                <label class="lbl_Label">{{SelectedData['Comments']}}</label>
            </div>
        </div>
        <br />
        
<!--        <div class="col-md-12 form-group"> 
             <label class="col-md-4 col-xs-6 paddingNone">Attachment</label>
              <div class="col-md-8 col-xs-6">
             <input type="file" file-model="stockfile"  id="stockfile" class="stockfile" />
             </div>
        </div>
        <div class="clearfix"></div>-->
        
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval == 1">
            <label class="col-md-4 col-xs-6 paddingNone">Approval Remarks</label>
            <div class="col-md-8 col-xs-6 form-group">
                <textarea class="form-control txt_TextBox" placeholder="Approval Remark" name="txt_remarks" limit-to="100" ng-model="txt_remarks.text" required ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)"></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval != 1">
            <label class="col-md-4 col-xs-6 paddingNone">Rejection Remarks</label>
            <div class="col-md-8 col-xs-6 form-group">
                <textarea class="form-control txt_TextBox" placeholder="Rejection Remark" name="txt_remarks" limit-to="100" ng-model="txt_remarks.text" ng-change="ClearMsg()" ng-keydown="[13, 32].includes($event.keyCode) && onKeydown($event)" required></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() == 'pending'">
            <input type="button" class="btn btn_Cancel btn_form" value="Cancel" ng-click="CancelPanel();">&nbsp;
            <input type="button" class="btn btn_Button btn_form" value="APPROVE" ng-click="SaveComments();" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval == 1">
            <input type="button" class="btn btn_Button btn_form" value="REJECT" ng-click="SaveComments();" ng-if="SelectedData['Status'].toLowerCase() == 'pending' && IsDataForApproval != 1">
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group" ng-if="SelectedData['Status'].toLowerCase() != 'pending'">
            <input type="button" class="btn btn_Cancel btn_form" value="Cancel" ng-click="CancelPanel();">
        </div>
        <div class="clearfix"></div>
    </div>

