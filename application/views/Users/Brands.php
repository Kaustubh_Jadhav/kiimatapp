
<script src="<?php echo base_url(); ?>/script/angular/BrandController.js" type="text/javascript"></script> 
<div class="col-md-12" ng-controller="BrandController">
    <div class="col-md-8 form-group">
        <?php $this->load->view('SharedViews/BrandList.php'); ?>
    </div>
    <div class="col-md-4 form_View">
        <?php $this->load->view('SharedViews/BrandForm.php'); ?>
        <?php $this->load->view('SharedViews/EditBrandForm.php'); ?>
    </div>
</div>
<div class="clearfix"></div>
{{token_ = '<?php echo $this->security->get_csrf_hash() ?>'}}


