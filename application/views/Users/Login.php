<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>/script/angular/loginController.js" type="text/javascript"></script> 
<div class="login-form" ng-clock ng-controller="LoginController">
    <form ng-submit="submitLoginForm()">
        <h2 class="text-center loginHeader">Sign In To <br> Kishan Infra Inventory Management &amp; Accounting Tool (KIIMAT)</h2>
        <div class="col-md-12 alert alert-danger" role="alert" ng-if="ErrorMessage != ''">
            {{ErrorMessage}}
        </div>
        <div class="col-md-12 alert alert-success" role="alert" ng-if="SuccessMessage != ''">
            {{SuccessMessage}}
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Username" required="required" ng-model="user.UserName">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Password" required="required" ng-model="user.Password">
            </div>
        </div>        
        <div class="form-group centerAlign">
            <button type="submit" class="btn btn_Button">Log in</button>
        </div>
        <!--        <div class="clearfix">
                    <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
                    <a href="#" class="pull-right">Forgot Password?</a>
                </div>        -->
    </form>
</div>
<style type="text/css">
    .login-form {
        width: 320px;
        margin: 50px auto;
    }
    .login-form form {        
        margin-bottom: 15px;
        background: #eaeaea80;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .input-group-addon .fa {
        font-size: 18px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>