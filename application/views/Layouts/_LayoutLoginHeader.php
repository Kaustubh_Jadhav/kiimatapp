<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $Title; ?> - <?php echo $Heading; ?></title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <link rel="icon" href="<?php echo base_url(); ?>/images/fevicon.ico" sizes="32x32" />

        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
       
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
        <link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet" type="text/css"/>
        
        <script src="<?php echo base_url(); ?>/script/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/Common.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/hammer.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/chartjs-plugin-zoom.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/bootstrap-toggle.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/jquery.slimscroll.js" type="text/javascript"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular.min.js"></script>


        <script type="text/javascript">
            var app = angular.module('kiimatApp', []);
            app.filter('dateToISO', function () {
                return function (input) {
                    return new Date(input).toISOString();
                };
            });

            app.directive('onlyDigits', function () {
                return {
                    require: 'ngModel',
                    restrict: 'A',
                    link: function (scope, element, attr, ctrl) {
                        function inputValue(val) {
                            if (val) {
                                var digits = val.replace(/[^0-9.]/g, '');

                                if (digits.split('.').length > 2) {
                                    digits = digits.substring(0, digits.length - 1);
                                }

                                if (digits !== val) {
                                    ctrl.$setViewValue(digits);
                                    ctrl.$render();
                                }
                                return parseFloat(digits);
                            }
                            return undefined;
                        }
                        ctrl.$parsers.push(inputValue);
                    }
                };
            });

            app.directive("limitTo", [function () {
                    return {
                        restrict: "A",
                        link: function (scope, elem, attrs) {
                            var limit = parseInt(attrs.limitTo);
                            angular.element(elem).on("keypress", function (e) {
                                if (this.value.length == limit)
                                    e.preventDefault();
                            });
                        }
                    }
                }]);

        </script>
    </head>
    <body ng-app="kiimatApp" class="LoginBg">
        <div id="wrapper">
            <div class="col-md-12 headerStripe">
                <div class="col-md-2 centerAlign">
                        <image src="<?php echo base_url(); ?>/images/kiimat_logo.png" class="logoImage" />
                </div>
                <div class="col-md-8 centerAlign loginTitle"></div>
                <div class="col-md-2 centerAlign">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="content">
                <div class="divHeight50"></div>