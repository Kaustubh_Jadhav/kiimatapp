<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <title><?php echo $Title; ?> - <?php echo $Heading; ?></title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <link rel="icon" href="<?php echo base_url(); ?>/images/fevicon.ico" sizes="32x32" />

        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ng-table/1.0.0/ng-table.css">
       


        <script src="<?php echo base_url(); ?>/script/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/Common.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/hammer.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/chartjs-plugin-zoom.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/bootstrap-toggle.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/jquery.slimscroll.js" type="text/javascript"></script>   
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-animate.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-aria.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-messages.min.js"></script>
        <!-- Angular Material Library -->
        <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/dirPagination.js" type="text/javascript"></script> 

        
        <!-- angular UI library -->
<!--        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-touch.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-animate.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-aria.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/lodash.min.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/jszip.min.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/excel-builder.dist.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.js"></script>
        <link rel="stylesheet" href="http://ui-grid.info/release/ui-grid.css" type="text/css">
        <script src="http://ui-grid.info/release/ui-grid.grouping.min.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.core.min.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.tree-base.min.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.edit.min.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.selection.min.js"></script>-->
        
        
        
         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-touch.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-animate.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.7.0/angular-aria.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/lodash.min.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/jszip.min.js"></script>
        <script src="http://ui-grid.info/docs/grunt-scripts/excel-builder.dist.js"></script>
        <script src="http://ui-grid.info/release/ui-grid.js"></script>
        <link rel="stylesheet" href="http://ui-grid.info/release/ui-grid.css" type="text/css">
       
        
        <script src="<?php echo base_url(); ?>/script/jquery.tableToExcel.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/excelexportjs.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/TableExport/5.2.0/js/tableexport.min.js"></script>
<!--        <script src="<?php echo base_url(); ?>/script/jhxlsx.js"></script>
        <script src="<?php echo base_url(); ?>/script/xlsx.core.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/FileSaver.js"></script>
        <script src="<?php echo base_url(); ?>/script/xls-export.js"></script>-->

       <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet" />
       <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
       
       <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
       <script src="https://cdn.jsdelivr.net/angular.chartjs/latest/angular-chart.min.js"></script>
       
       
<!--      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>
 
 
        <script type="text/javascript">
            var app = angular.module('kiimatApp', ['ngMaterial', 'ngMessages', 'angularUtils.directives.dirPagination','ui.grid', "ui.grid.pagination", 'ui.grid.grouping', 'ngAnimate', 'ngTouch', 'ui.grid.selection', 'ui.grid.exporter', 'ui.grid.autoResize', 'ui.grid.edit','ui.grid.cellNav', 'ui.grid.resizeColumns', 'ui.grid.moveColumns','chart.js','ui.grid.infiniteScroll']).config(function ($mdDateLocaleProvider) {
                /**
                 * @param date {Date}
                 * @returns {string} string representation of the provided date
                 */
                $mdDateLocaleProvider.formatDate = function (date) {
                    return date ? moment(date).format('DD-MM-YYYY') : '';
                };

                /**
                 * @param dateString {string} string that can be converted to a Date
                 * @returns {Date} JavaScript Date object created from the provided dateString
                 */
                $mdDateLocaleProvider.parseDate = function (dateString) {
                    var m = moment(dateString, 'DD-MM-YYYY', true);
                    return m.isValid() ? m.toDate() : new Date(NaN);
                };

                /**
                 * Check if the date string is complete enough to parse. This avoids calls to parseDate
                 * when the user has only typed in the first digit or two of the date.
                 * Allow only a day and month to be specified.
                 * @param dateString {string} date string to evaluate for parsing
                 * @returns {boolean} true if the date string is complete enough to be parsed
                 */
                $mdDateLocaleProvider.isDateComplete = function (dateString) {
                    dateString = dateString.trim();
                    // Look for two chunks of content (either numbers or text) separated by delimiters.
                    var re = /^(([a-zA-Z]{3,}|[0-9]{1,4})([ .,]+|[/-]))([a-zA-Z]{3,}|[0-9]{1,4})/;
                    return re.test(dateString);
                };
            });

            app.filter('dateToISO', function () {
                return function (input) {
                    return new Date(input).toISOString();
                };
            });

            app.directive('onlyDigits', function () {
                return {
                    require: 'ngModel',
                    restrict: 'A',
                    link: function (scope, element, attr, ctrl) {
                        function inputValue(val) {
                            if (val) {
                                var digits = val.replace(/[^0-9.]/g, '');

                                if (digits.split('.').length > 2) {
                                    digits = digits.substring(0, digits.length - 1);
                                }

                                if (digits !== val) {
                                    ctrl.$setViewValue(digits);
                                    ctrl.$render();
                                }
                                return parseFloat(digits);
                            }
                            return undefined;
                        }
                        ctrl.$parsers.push(inputValue);
                    }
                };
            });
            
            app.directive('onlyLetter', function () {
                return {
                    require: 'ngModel',
                    restrict: 'A',
                    link: function (scope, element, attr, ctrl) {
                        function inputValue(val) {
                            if (val == undefined) return '';
                            if (val) {
                                var digits = val.replace(/[^a-zA-Z]/g, '');
                                if (digits !== val) {
                                    ctrl.$setViewValue(digits);
                                    ctrl.$render();
                                }
                                return digits;
                            }
                            return undefined;
                            
                        }
                        ctrl.$parsers.push(inputValue);
                    }
                };
            });
            
            
            
             app.directive('readonly',function(){
                return {
                 restrict: 'EAC',
                 link: function(scope, elem, attr) {
                 document.querySelectorAll("#datePicker input")[0].setAttribute("readonly","readonly");
                 angular.element(".md-datepicker-button").each(function(){
                 var el = this;
                 var ip = angular.element(el).parent().find("input").bind('click', function(e){
                    angular.element(el).click();
                     });
                 //angular.element(this).css('visibility', 'hidden');
                });
               }
             }
  
            });
            
            
            app.directive('readonly',function(){
                return {
                 restrict: 'EAC',
                 link: function(scope, elem, attr) {
                 document.querySelectorAll("#datePicker1 input")[0].setAttribute("readonly","readonly");
                 angular.element(".md-datepicker-button").each(function(){
                 var el = this;
                 var ip = angular.element(el).parent().find("input").bind('click', function(e){
                    angular.element(el).click();
                     });
                 //angular.element(this).css('visibility', 'hidden');
                });
               }
             }
  
            });

            app.directive("limitTo", [function () {
                    return {
                        restrict: "A",
                        link: function (scope, elem, attrs) {
                            var limit = parseInt(attrs.limitTo);
                            angular.element(elem).on("keypress", function (e) {
                                if (this.value.length == limit)
                                    e.preventDefault();
                            });
                        }
                    }
                }]);
            
            
           app.directive('resize',['$window', function ($window) {
	return function (scope, element, attr) {

		var w = angular.element($window);
		scope.$watch(function () {
			return {
				'h': window.innerHeight,
				'w': window.innerWidth
			};
		}, function (newValue, oldValue) {
			scope.windowHeight = newValue.h;
			scope.windowWidth = newValue.w;

			scope.resizeWithOffset = function (offsetH) {
				scope.$eval(attr.notifier);
				return {
					'height': (newValue.h - offsetH) + 'px'
				};
			};

		}, true);

		w.bind('resize', function () {
			scope.$apply();
		});
	}
}]);

            app.filter('INR', function () {
                return function (input) {
                    if (!isNaN(input)) {

                        var result = input.toString().split('.');

                        var lastThree = result[0].substring(result[0].length - 3);
                        var otherNumbers = result[0].substring(0, result[0].length - 3);
                        if (otherNumbers != '')
                            lastThree = ',' + lastThree;
                        var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                        if (result.length > 1) {
                            output += "." + result[1];
                        }

                        return output;
                    }
                }
            });

          $(document).ready(function (e) {

                var Routename = '<?php echo $this->uri->segment(1); ?>';
                if (Routename.toLowerCase() == 'mis-entry')
                {
                    $("#MISEntry").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'expenses')
                {
                    $("#ExpenseEntry").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'cash-transfer')
                {
                    $("#CashTransfer").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'stock-transfer')
                {
                    $("#StockTransfer").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'approval')
                {
                    $("#Approval").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'users')
                {
                    $("#Admin").addClass("selectedMenu");
                }
                else if (Routename.toLowerCase() == 'brands')
                {
                    $("#Admin").addClass("selectedMenu");
                }
                 else if (Routename.toLowerCase() == 'appliances')
                {
                    $("#Admin").addClass("selectedMenu");
                
                }
                else if (Routename.toLowerCase() == 'warehouse')
                {
                    $("#Admin").addClass("selectedMenu");
                
                }
                 else if (Routename.toLowerCase() == 'daily-sales')
                {
                    $("#MISReports").addClass("selectedMenu");
                }
                else if (Routename.toLowerCase() == 'stock-position')
                {
                    $("#MISReports").addClass("selectedMenu");
                } else if (Routename.toLowerCase() == 'change-password')
                {
                    $("#ChangePassword").addClass("selectedMenu");
                }
                
            });


            app.factory('UserSesionService', function () {
                return {
                    Username: '<?php echo $this->session->userdata('Username') ?>',
                    RoleType: '<?php echo $this->session->userdata('RoleType') ?>',
                    Id: '<?php echo $this->session->userdata('Id') ?>',
                    Name: '<?php echo $this->session->userdata('Name_') ?>',                
                    StateName: '<?php echo $this->session->userdata('StateName') ?>',
                    DistrictName: '<?php echo $this->session->userdata('DistrictName') ?>',
                    WarehouseName: '<?php echo $this->session->userdata('WarehouseName') ?>',
                };
            });
            
            $(document).ready(function() {
                $('#Admin').click(function(e){
                e.preventDefault();
                $("#sm2").prop('checked', function(i, current) { return !current; });
                });
            });
            
             $(document).ready(function() {
                $('#MISReports').click(function(e){
                e.preventDefault();
                $("#sm1").prop('checked', function(i, current) { return !current; });
                });
            });
            
//        setTimeout(function() {
//            location.hash = "#Form_Div";
//             //location.hash = '#Form_Div';
//             
//        },3000);

app.directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);

// We can write our own fileUpload service to reuse it in the controller
app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
         var fd = new FormData();
         fd.append('file', file);
//         return $http.post(uploadUrl, fd, {
//             transformRequest: angular.identity,
//             headers: {'Content-Type': undefined,'Process-Data': false}
//         }).then(function (response){
//            // console.log(response);
//             var Response_ =   response["data"];
//            // console.log(Response_);
//             var response =  Response_;
//             return response;
//         });
         return $http.post(uploadUrl, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         });
     }
 }]);
 
        </script>
    </head>
    <body ng-app="kiimatApp">
        <div id="wrapper">
            <div class="fixedHeader">
                <div class="col-md-12 headerStripe" style="border: none;">
                    <div class="col-md-2 col-xs-4 centerAlign">
                        <image src="<?php echo base_url(); ?>/images/kiimat_logo.png" class="logoImage" />
                    </div>
                  <div class="col-md-8 col-xs-4 centerAlign welcomeHeader"><span class="header_Welcome">Welcome</span>&nbsp;<span class="header_Username"><?php echo $this->session->userdata('Name_'); ?></span>&nbsp;(<?php echo $this->session->userdata('Username'); ?>)</div> 
                  <div class="col-md-2 col-xs-4 centerAlign">
                       <!-- <a class="anchor_link_dark" href="<?php echo site_url('/') ?>" onclick="return confirm('Are you sure to logout?');"><span class="welcomeHeader">Logout</span>&nbsp;<i class="fa fa-lock" aria-hidden="true"></i></a>-->
                      <a class="anchor_link_dark" data-toggle="modal" data-target="#ModalExample"><span class="logout">Logout</span>&nbsp;<i class="fa fa-lock" aria-hidden="true"></i></a>

                  </div>
                </div>
                
                
                <div class="clearfix"></div>

                 <nav id="menu">
                    <label for="tm" id="toggle-menu">Navigation <span class="drop-icon">▾</span></label>
                    <input type="checkbox" id="tm">
                    <ul class="main-menu clearfix">
                    <?php if ($_SESSION['RoleType'] == 'COUNTER') { ?>
                            <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="MISEntry" href="<?php echo site_url('mis-entry') ?>">MIS Entry</a></li>

                    <?php } ?>
                            
                     <?php if ($_SESSION['RoleType'] == 'WAREHOUSEADMIN' || $_SESSION['RoleType'] == 'COUNTER') { ?>          
                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="StockTransfer" href="<?php echo site_url('stock-transfer') ?>">Stock Transfer</a></li>
                     <?php } ?>
                        

                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="CashTransfer" href="<?php echo site_url('cash-transfer') ?>" >Cash Transfer</a></li>

                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="ExpenseEntry" href="<?php echo site_url('expenses') ?>">Expenses</a></li>
                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="Approval" href="<?php echo site_url('approval') ?>">Approvals</a></li>
                        
                    
                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="MISReports" href="#" for="sm1">MIS Report
                                <span class="drop-icon">▾</span>
                                <label title="Toggle Drop-down" class="drop-icon" for="sm1">▾</label>
                            </a>
                            <input type="checkbox" id="sm1">
                            <ul class="sub-menu">
                                <?php if ($_SESSION['RoleType'] == 'SUPERADMIN' || $_SESSION['RoleType'] == 'STATEADMIN' || $_SESSION['RoleType'] == 'DISTRICTADMIN' ) { ?>
                                <li><a class="anchor_link_light"  href="<?php echo site_url('daily-sales') ?>">Daily Sales</a></li>
                                   <?php } ?>
                                
                                <li><a class="anchor_link_light anchor"  href="<?php echo site_url('stock-position') ?>">Stock Position</a></li>
                                
                                <?php if ( $_SESSION['RoleType'] != 'WAREHOUSEADMIN' ) { ?>
                                <li><a class="anchor_link_light"  href="<?php echo site_url('cash-pendency') ?>">Cash Pendency</a></li>
                                <?php } ?>
<!--                                <li><a class="anchor_link_light" href="<?php echo site_url('chart') ?>">Chart</a></li>-->
                                <!--<li><a class="anchor_link_light" href="#">Payment Summary</a></li>-->
                            </ul>
                        </li>
                  
                        
                        <?php if ($_SESSION['RoleType'] != 'COUNTER' && $_SESSION['RoleType'] != 'WAREHOUSEADMIN') { ?>
                            <li><a class="anchor_link_light menu_Box_middle" id="Admin" href="#" for="sm2" style="text-decoration: none;">Admin
                                    <span class="drop-icon">▾</span>
                                    <label title="Toggle Drop-down" class="drop-icon" for="sm2">▾</label>
                                </a>
                                <input type="checkbox" id="sm2">
                                <ul class="sub-menu">
                                    <li><a class="anchor_link_light anchor"  href="<?php echo site_url('users') ?>">Users</a></li>
                                    <?php if ($_SESSION['RoleType'] =='SUPERADMIN') { ?>
                                    <li><a class="anchor_link_light" href="<?php echo site_url('appliances') ?>">Appliances</a></li>
                                    <li><a class="anchor_link_light" href="<?php echo site_url('brands') ?>">Brands</a></li>
                                    <li><a class="anchor_link_light" href="<?php echo site_url('warehouse') ?>">Warehouses</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                            
                        <li><a class="anchor_link_light menu_Box_middle" style="text-decoration: none;" id="ChangePassword" href="<?php echo site_url('change-password') ?>">Change Password</a></li>

                    </ul>
                </nav>

                <div class="clearfix"></div>
            </div>
            <div class="content headerContent">
                
<!--                <div id="#" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">See more of this awesome website by logging in</h4>
                            </div>
                            <div class="modal-body">
                                <p class="lead text-xs-center">It only takes a few seconds to level up!</p>
                                <div class="lead text-xs-center"><a class="btn btn-info" href="/auth/register">Create Account</a> or
                                    <a class="btn btn-success" href="/auth/login">Sign In</a></div>
                            </div>
                            <div class="modal-footer">
                                :-)
                            </div>
                        </div> /.modal-content 
                    </div> /.modal-dialog 
                </div> /.modal -->
                
                
                <div id="ModalExample" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h2 class="modal-title centerAlign">Are you sure to logout?</h2>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary accordian-btn-color"  OnClick=" location.href='<?php echo site_url('/') ?>' ">Logout</button>
                            </div>
                        </div>
                    </div>
                </div>

  


