<script src="<?php echo base_url(); ?>script/angular/ExpenseController.js" type="text/javascript"></script> 
<div class="col-md-12 responsivePaddingNone" ng-controller="ExpenseController">
    <div class="col-md-8 form-group">
        <?php $this->load->view('SharedViews/ExpenseList.php'); ?>
    </div>

    <div class="col-md-4 form_View" id="FormDiv">
        <?php $this->load->view('SharedViews/ExpenseForm.php'); ?>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var ScreenResolutation = $(window).width();
        if (ScreenResolutation <= 1000)
        {

            $('html, body').animate({
                scrollTop: $('#FormDiv').offset().top + 540
            }, 'slow');
        }
    });
</script>