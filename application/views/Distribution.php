
<script src="<?php echo base_url(); ?>/script/angular/StockController.js" type="text/javascript"></script> 
<div class="col-md-12" ng-controller="StockController">
    <div class="col-md-8 form-group">
        <?php $this->load->view('SharedViews/StockList.php'); ?>
    </div>
    <div class="col-md-4 form_View">
        <?php $this->load->view('SharedViews/StockForm.php'); ?>
    </div>
</div>
<div class="clearfix"></div>
{{token_ = '<?php echo $this->security->get_csrf_hash() ?>'}}

<script type="text/javascript">
    $(document).ready(function () {
        var ScreenResolutation = $(window).width();
        if (ScreenResolutation <= 1000)
        {

            $('html, body').animate({
                scrollTop: $('#FormDiv').offset().top + 540
            }, 'slow');
        }
    });
</script>

