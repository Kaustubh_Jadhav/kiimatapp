<div class="col-md-12" ng-app="kiimatApp" ng-controller="kiimatController">
    <div class="col-md-8 listView">
        <div class="col-md-12 filterBox">
            <div class="col-md-12 paddingNone form-group">
                <label class="col-md-2 lbl_Label">Filter 1:</label>
                <div class="col-md-4">
                    <select class="form-control">
                        <option value="0">--Select--</option>
                    </select>
                </div>
                <label class="col-md-2 lbl_Label">Filter 2:</label>
                <div class="col-md-4">
                    <select class="form-control">
                        <option value="0">--Select--</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 paddingNone form-group">
                <label class="col-md-2 lbl_Label">Filter 3:</label>
                <div class="col-md-4">
                    <select class="form-control dd_select">
                        <option value="0">--Select--</option>
                    </select>
                </div>
                <label class="col-md-2 lbl_Label">Filter 4:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 paddingNone">
                <div class="col-md-9">
                </div>
                <div class="col-md-3">
                    <input type="button" value="Go" class="btn btn_Button">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="divHeight50"></div>
        <div class="col-md-12">
            <table class="table table-striped table-responsive-md">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>First Name</th>
                        <th>Last name</th>
                        <th>Avatar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="x in CounterList">
                        <td>{{ x.id}}</td>
                        <td>{{ x.email}}</td>
                        <td>{{ x.first_name}}</td>
                        <td>{{ x.last_name}}</td>
                        <td>{{ x.avatar}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-4 form_View">
        <div class="col-md-12">
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 1:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 2:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 3:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 4:</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 1:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 2:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 3:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 4:</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 1:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 2:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 3:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 4:</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 1:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 2:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 3:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control txt_TextBox">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-4 lbl_Label">Label 4:</label>
                <div class="col-md-8">
                    <textarea class="form-control txt_TextBox"></textarea>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 form-group">
            <input type="button" value="Save" class="btn btn_Button">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    var app = angular.module('kiimatApp', []);
    app.controller('kiimatController', function ($scope, $http) {
        $http.get('https://reqres.in/api/users?page=2').then(function (response) {
            console.log(response["data"]["data"]);
            $scope.CounterList = response["data"]["data"]

        });
    });
</script>
