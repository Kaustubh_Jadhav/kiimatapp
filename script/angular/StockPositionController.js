app.controller('StockPositionController', function ($scope, $http, $interval, UserSesionService, uiGridGroupingConstants, uiGridConstants, $timeout, $element) {
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.StateList = [];
    $scope.DistrictList = [];
    $scope.UserList = [];
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    $scope.WarehouseName = UserSesionService.WarehouseName;
    $scope.RoleType = UserSesionService.RoleType; 
    $scope.Status='0';
    $scope.Appliance = '0';
    $scope.noRecord = "";
    $scope.panel=true;


    if ($scope.RoleType == "SUPERADMIN")
    {
        $scope.State = "0";
        $scope.District = "0";
        $scope.Username="0";
    }
    else if($scope.RoleType == "COUNTER" ){
         $scope.Username=  UserSesionService.Username;
         $scope.panel=false;
    }
    else if( $scope.RoleType == 'WAREHOUSEADMIN'){
         $scope.Username=  UserSesionService.WarehouseName;
         $scope.panel=false;
    }
    else if( $scope.RoleType == "STATEADMIN"){
        $scope.Username="0";
    }
    else if( $scope.RoleType == 'DISTRICTADMIN'){
        $scope.Username="0";
    }
    
//   
//   alert($scope.Username);
   $scope.width="88.5%";
    $scope.UserList = [];
   // $scope.Username = UserSesionService.Username;
   
   
   
   
    $scope.gridOptions = {
    enableFiltering: false,
    treeRowHeaderAlwaysVisible: false,
    paginationPageSize: 1,
    enablePaginationControls: true,
    enableColumnResizing: true,
    enableGridMenu: true,
    excessRows:1500,
    enableCellEdit: false,
    showGridFooter: true,
    groupingShowCounts: false,
    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
    enableVerticalScrollbar: 1,
    allowCellFocus: false,
    minimumColumnSize:110,
    columnDefs: [
      { displayName: 'Appliance',field: 'Name_',  grouping: { groupPriority: 0 }, sort: {  direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { field: 'StateName',  grouping: { groupPriority: 1 }, sort: {  direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { field: 'DistrictName', grouping: { groupPriority: 2 },sort: {  direction: 'asc' }, cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { field: 'Username', sort: { direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'  },
      //{ field: 'ApplianceName', width: '10%', grouping: { groupPriority: 3 }, sort: { priority: 3, direction: 'asc' }, width: '20%', cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'  },
      //{ field: 'StockType', width: '10%',  grouping: { groupPriority: 3 }, sort: { priority: 3, direction: 'asc' }, width: '20%', cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { field: 'FRESH', cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }},
      { field: 'DEFECTIVE', cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }}
      ],
      gridMenuShowHideColumns:false,
      exporterMenuVisibleData:false,
      exporterMenuPdf: false,
      exporterMenuCsv: false,
      onRegisterApi: function (gridApi) {
      $scope.gridApi = gridApi;
      
      $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
      $timeout(function () {
            var $viewport = angular.element('.ui-grid-render-container');
            ['touchstart', 'touchmove', 'touchend','keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
                $viewport.unbind(eventName);
            });
       });
       
       
        if($scope.RoleType == "COUNTER" || $scope.RoleType == "WAREHOUSEADMIN"){
            
             $scope.gridApi.grid.registerDataChangeCallback(function() {
               $scope.gridApi.treeBase.expandAllRows();
               $scope.gridApi.grouping.clearGrouping();
           // $scope.gridApi.grouping.groupColumn('ApplianceName').setGrouping(2);
             });
        }
      }
  };
  
  
//    $scope.changeGrouping = function() {
//                alert('hii');
//                $scope.gridApi.grouping.clearGrouping();
//                 $scope.gridApi.grouping.groupColumn('ApplianceName').setGrouping(2);
//              };
//  
   
    $scope.GetStock = function () {
        //$http.get('fetchStockTransfer').then(function (response) {
        $http.post('available-stock', {"State": $scope.State, "District": $scope.District, "Username":$scope.Username, "Appliance":$scope.Appliance}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                //console.log(Status_);
                var data = Response_[0]["data"];
               // $scope.gridOptions.data = data;
                // console.log($scope.gridOptions.data);
                
                if(data.length == 0){
                       $scope.noRecord ="No record found";
                       $scope.gridOptions.data=[];
                }
                else{
                     $scope.gridOptions.data = data;
                     for ( var i = 0; i < data.length; i++ ){
                          data[i].Username =  data[i].Username.toUpperCase();
                      }
                     $scope.noRecord="";
                }

            } else
            {
                //console.log(Status_);
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetStock();
    
       $scope.GetApplianceList = function () {
        $http.get('fetchApplianceList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.ApplianceList = Data_;
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApplianceList();
    
    
    $scope.filterText = '';
    $scope.filterValue = '';
//    $scope.filterBtn = function () {
//        $scope.filterText = 'MAHARASHTRA';
//        $scope.gridApi.grid.refresh();
//    }

    $scope.filter = function () {
        $scope.filterText = $scope.filterValue;
        $scope.gridApi.grid.refresh();
    };

    $scope.singleFilter = function (renderableRows) {
//        alert($scope.filterText);
        var matcher = new RegExp($scope.filterText.toLowerCase());
        
        renderableRows.forEach(function (row) {
            var match = false;
            ['Name_','StateName', 'DistrictName','Username'].forEach(function (field) {
                if (row.entity[field].toLowerCase().match(matcher)) {
                    match = true;
//                    alert(true);
                }
            });
            if (!match) {
                    row.visible = false;
//                  alert(false);
//                    $scope.gridOptions.data.length=0;
//                    $scope.noRecord ="No record found";
            }
        });
        return renderableRows;
    };
});