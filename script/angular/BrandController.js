app.controller('BrandController', function ($scope, $http, UserSesionService) {
   
    $scope.ShowForm = 0;
    $scope.SelectedData = "";
    $scope.Id ='';
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.column = 'TrnDate';
    $scope.EditForm = 0;
    $scope.aname = '0';
    $scope.bname = '';
    $scope.buttonShow=true; 
    $scope.noRecord="";
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };


    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };
    
     
    $scope.OpneAction = function ()
    {
        $scope.ShowForm =1;
        $scope.EditForm =0;
        $scope.SuccessMessage = "";
        $scope.ErrorMessage = "";
        $scope.aname = "0";
        $scope.bname = "";
        $scope.SelectedData = "";
       $scope.myForm.$setPristine();
    };


   $scope.GetApplianceList = function () {
        $http.get('fetchApplianceList').then(function (response) {
            //console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            //console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                $scope.ApplianceList = Data_;
                //console.log($scope.ApplianceList);
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApplianceList();
    
    
    $scope.getBrandList = function () {
            //console.log('hii');
            $http.post('fetchBrandList',{'ApplianceName':''}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
               var Data_ = Response_[0]["data"];
               if(Data_.length ==0){
                    $scope.noRecord="No record found.";
                    $scope.TmpList=[];
                }
                else{
                    $scope.TmpList = Data_;
                    $scope.noRecord="";
                }
            } else
            {
                 $scope.noRecord="No record found.";
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        
    };
    $scope.getBrandList();
    
    $scope.SaveBrandDetails = function (isValid) {
        if ($scope.aname != "0" && $scope.bname != "") {
            $http({
                method: 'post',
                url: 'save-brand',
                data: {"Id": '', "ApplianceName": $scope.aname.toUpperCase(), "Name_": $scope.bname.toUpperCase()},
                headers: {'Content-Type': 'application/json'}
            }).then(function (response)
            {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == "1")
                {
                    $scope.SuccessMessage = Message_;
                    $scope.ErrorMessage = "";
                    $scope.aname = "0";
                    $scope.bname = "";
                    $scope.myForm.$setPristine();
                    $scope.getBrandList();
                    $scope.backToButton();

                } else
                {
                    $scope.ErrorMessage = Message_;
                    //$scope.aname = "0";
                    //$scope.bname = "";
                    //$scope.backToButton();
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        } else {
            if ($scope.aname == '0') {
                $scope.ErrorMessage = "Please Select Appliance Name.";
            } else if ($scope.bname == '') {
                $scope.ErrorMessage = "Please Enter Brand Name. ";
            }
        }
    };
    
    $scope.EditBrandDetails = function (isValid)
    {
       
       // console.log($scope.eappname);
        //console.log($scope.ebrname);
        if($scope.ebrname == undefined){
            $scope.ebrname = "";
        }
        if ($scope.eappname != "0" && ($scope.ebrname != ""))
        {
            $http({
                method: 'post',
                url: 'save-brand',
                data: {"Id": $scope.SelectedData["Id"], "ApplianceName": $scope.eappname.toUpperCase(), "Name_": $scope.ebrname.toUpperCase()
                },
                headers: {'Content-Type': 'application/json'}
            }).then(function (response)
            {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == "1")
                {
                    $scope.SuccessMessage = Message_;
                    $scope.ErrorMessage = "";
                    $scope.eappname = "0";
                    $scope.ebrname = "";
                    $scope.getBrandList();
                    $scope.clearScreen();

                } else
                {
                    $scope.ErrorMessage = Message_;
                    $scope.clearScreen();
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });

        } else
        {
            if ($scope.eappname == '0') {
                $scope.ErrorMessage = "Please Select Appliance Name";
            } else if ($scope.ebrname == ''|| $scope.ebrname == undefined) {
                $scope.ErrorMessage = "Please Enter Brand Name ";
            }

        }
    };
   

    $scope.backToButton=function(){
        $scope.ShowForm = 0;
    };
    
     $scope.EditAction = function (Id)
    {   
        $scope.SuccessMessage = '';
        $scope.ErrorMessage = '';
        $scope.password='';
        $scope.cnfpassword='';
        $scope.EditForm =1;
        $scope.ShowForm = 0;
        $scope.SelectedData = $scope.TmpList[$scope.TmpList.findIndex(x => x.Id == Id)];
        $scope.eappname = $scope.SelectedData['ApplianceName'];
        $scope.ebrname = $scope.SelectedData['Name_'];
        $scope.myForm.$setPristine();
       // console.log($scope.eappname);
       // console.log($scope.ebrname);
        
        $scope.model = {
            isDisabled: true
        };
    };
    
    $scope.clearScreen=function(){
        $scope.EditForm =0;
         $scope.myForm.$setPristine();
    };

});


