app.controller('StockController', function ($scope, $http, UserSesionService) {
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.IsExpenseType = "0";
    $scope.dd_transferType = "0";
    $scope.dd_brandName = "0";
    $scope.dd_applienceType = "0";
    $scope.dd_partyType1 = "0";
    $scope.dd_partyType = "0";

    $scope.StateList = [];
    $scope.DistrictList = [];
    $scope.UserList = [];
    //for form
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    //for filter
    $scope.StateSearch = UserSesionService.StateName;
    $scope.DistrictSearch = UserSesionService.DistrictName;
    $scope.IsUserEnable =false;
    $scope.IsDistrictEnable =false;
    $scope.IsStateEnable =false;
    $scope.IsWhEnable = false;
    $scope.WarehouseName = UserSesionService.WarehouseName;
    $scope.RoleType = UserSesionService.RoleType; 
    $scope.Role = UserSesionService.RoleType;
    $scope.Status='0';
    $scope.noRecord="";
    $scope.STnoRecord="";
    //distribution
    $scope.Appliance='0';
    $scope.select=true;
    
    // adding removing days from date
    $scope.day= 6;
    $scope.PrevNoofMonths =2;
    $scope.AddedNumDay = 2;
    $scope.PrevNoofDays = 15;
    $scope.openForm=false;
    $scope.Prevday=6;
    if ($scope.Role == 'COUNTER') 
    {
        $scope.dd_partyType = UserSesionService.Username;
        $scope.Uname = UserSesionService.Username.toUpperCase();
        $scope.Wname ="0";
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
        $scope.IsUserEnable =true;
        $scope.IsWhEnable = true;
        $scope.select=false;
        //$scope.State="0";
       // $scope.District="0"
       //alert($scope.Uname);
    } 
    else if ($scope.Role == 'WAREHOUSEADMIN') {
        $scope.dd_partyType = UserSesionService.WarehouseName;
        $scope.Wname = UserSesionService.WarehouseName;
        $scope.Uname = "0";
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
        $scope.IsUserEnable =true;
        $scope.IsWhEnable = true;
        $scope.select=false;
    } 
    else if ($scope.RoleType == "DISTRICTADMIN")
    {
        $scope.Uname = "0";
        $scope.Wname = "0";
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
    } 
    else if ($scope.RoleType == "STATEADMIN")
    {
        $scope.Uname = "0";
        $scope.Wname = "0";
        $scope.IsStateEnable =true;
    }
    else
    {
        $scope.Uname = "0";
        $scope.Wname = "0";
        $scope.State = "0";
        $scope.District = "0";
        $scope.Status='0';
        //console.log($scope.Uname);
    }
    

    $scope.dd_usernameTo = "0";
    $scope.StockTransferList = [];
    $scope.ApplianceList = [];
    $scope.BrandList = [];
    $scope.UserList = [];
    $scope.Username = UserSesionService.Username;
    $scope.show = 'false';
    $scope.hide = 'true';
    $scope.stockType = "0"
    $scope.applienceType = "0";
    $scope.brandName = "0";
    
    // date for stock distribution
    $scope.txt_trnDate = new Date();
    $scope.minmisDate = new Date(
            $scope.txt_trnDate.getFullYear(),
           // $scope.txt_trnDate.getMonth() - $scope.AddedNumDay,
            $scope.txt_trnDate.getMonth(),
            $scope.txt_trnDate.getDate()- $scope.Prevday
            );
    $scope.maxmisDate = new Date();
    this.isOpen = false;
    
      $scope.TransactionDate = new Date();
      $scope.to_Date = new Date();
      $scope.from_Date =  new Date(
            $scope.to_Date.getFullYear(),
            $scope.to_Date.getMonth(),
            $scope.to_Date.getDate()- $scope.PrevNoofDays
            );
 
     $scope.from_Date = new Date( $scope.from_Date.getTime() + Math.abs($scope.from_Date.getTimezoneOffset()*60000) );
     this.isOpen = false;
     
     
     // min max date validation for form
    if (UserSesionService.RoleType != 'COUNTER' && UserSesionService.RoleType != 'WAREHOUSEADMIN')
    {
        $scope.minDate = new Date(
                $scope.TransactionDate.getFullYear(),
                //$scope.TransactionDate.getMonth() -  $scope.PrevNoofMonths,
                $scope.TransactionDate.getMonth(),
                $scope.TransactionDate.getDate()- $scope.Prevday
                );
        $scope.maxDate = new Date(
                $scope.TransactionDate.getFullYear(),
                $scope.TransactionDate.getMonth(),
                $scope.TransactionDate.getDate()
                );
        this.isOpen = false;

    } else {
        $scope.minDate = new Date(
                $scope.TransactionDate.getFullYear(),
                $scope.TransactionDate.getMonth(),
                $scope.TransactionDate.getDate() - $scope.day
                );
        $scope.maxDate = new Date(
                $scope.TransactionDate.getFullYear(),
                $scope.TransactionDate.getMonth(),
                $scope.TransactionDate.getDate()
                );
        this.isOpen = false;
    }
    // sharvari section

    //get appilanlist for stock distribution
    $scope.GetApplianceList = function () {
        $http.get('fetchApplianceList').then(function (response) {
            //console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            //console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                $scope.ApplianceList = Data_;
                //console.log($scope.ApplianceList);
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApplianceList();

    //func to fetch Available stock for distribution
    $scope.getdetails1 = function () {

        var ApplianceName = $scope.dd_expnsType;
        var stockType = "FRESH";
        if ($scope.dd_expnsType != "0") {
            
            $scope.SuccessMessage = "";
            $scope.ErrorMessage = ""; 
            $http({
                method: 'POST',
                url: 'fetchCounterStock1',
                data: {"ApplianceName": ApplianceName, "Username": $scope.Username, "stockType": stockType},
                headers: {'Content-Type': 'application/json'}
            }).then(function (response) {
                //console.log(response);
                var Response_ = response["data"];
                // console.log(response);
                var Status_ = Response_[0]["status"];
                //console.log(Status_);
                var Message_ = Response_[0]["message"];
                if (Status_ == 1)
                {
                    $scope.getstatus = Status_;
                    var Data_ = Response_[0]["data"];

                    //console.log(Data_);
                    if (Data_ == "") {
                        $scope.show = 'false';
                        $scope.Available = "";
                        $scope.ErrorMessage = "Stock is  not present for specified user";
                    } else if (Data_[0]['QtyAvailable'] == 0) {
                        $scope.show = 'false';
                        $scope.Available = "";
                        $scope.ErrorMessage = "Stock is  not present for specified user";
                    } else {
                        $scope.show = 'true';
                        $scope.Available = Data_[0]['QtyAvailable'];
                        $scope.RatePerPc = Data_[0]['RatePerPc'];
                    }

                    //console.log($scope.Available);

                } else
                {
                    $scope.show = 'false';
                    $scope.Available = "";
                    $scope.RatePerPc = "";
                    $scope.ErrorMessage = "Stock is  not present for specified user";
                }

            }, function (response) {
                $scope.show = 'false';
                $scope.ErrorMessage = "Unknown error occured. Please try again later";
            });
        }

    }
    //$scope.getdetails1();

    // func to fetch stock distribution List
    $scope.GetDistributionList = function () {
        $scope.send =  new Date(
            $scope.to_Date.getFullYear(),
            $scope.to_Date.getMonth(),
            $scope.to_Date.getDate(),
            23, 59, 59, 0
            );
        //convert to date with specific time
        $scope.send=new Date( $scope.send.getTime()-($scope.send.getTimezoneOffset())*60000);
        //alert($scope.send);
        $http({
            method: 'POST',
            url: 'fetch-MisEntry',
            data: {"ApplianceName": $scope.Appliance, "Username": $scope.Uname, "From": $scope.from_Date, "To":$scope.send},
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            // console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            // console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
               var Data_ = Response_[0]["data"];
               if(Data_.length ==0){
                    $scope.noRecord ="No details found for mis-entry.";
                    $scope.DistributionList=[];
                }
                else{
                    $scope.DistributionList = Data_;
                    $scope.noRecord="";
                }

            } else
            {
                $scope.ErrorMessage = Message_;
            }

        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetDistributionList();

    // func to save stock distribution details
    $scope.SaveStockDetails = function (isValid) {
        //var username = "";
        var cash = $scope.RatePerPc * $scope.txt_QtyDistributed;
        var dist = parseInt($scope.txt_QtyDistributed) || 0;
        var rep = parseInt($scope.QtyReplaced) || 0;
        var bd = parseInt($scope.QtyBD) || 0;
        var TotalStock = dist + rep + bd;
        
       
        // console.log(TotalStock);
        if (isValid) {
            //console.log('hii');
             //SNT 28-03-2020 for tie zone
            $scope.txt_trnDate = new Date( $scope.txt_trnDate.getTime() -($scope.txt_trnDate.getTimezoneOffset())*60000);
            if ($scope.dd_expnsType != "0")
            {
                if (TotalStock > 0 && $scope.Available >= TotalStock) {
                    $http({
                        method: 'post',
                        url: 'save-MisEntry',
                        data: {"Username": $scope.Username, "ApplianceName": $scope.dd_expnsType, "QtyDistributed": $scope.txt_QtyDistributed, "QtyReplaced": $scope.QtyReplaced, "QtyBD": $scope.QtyBD, "CashReceived": cash, "Comments": $scope.txt_comments, "TrnDate":$scope.txt_trnDate},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        //console.log(response);
                        var Status_ = Response_[0]["status"];
                        // console.log(Status_);
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                          
                            $scope.GetDistributionList();
                            //$scope.UpdateCounterStock();
                            $scope.dd_expnsType = "0";
                            $scope.txt_QtyDistributed = "";
                            $scope.QtyReplaced = "";
                            $scope.QtyBD = "";
                            $scope.txt_comments = "";
                            $scope.SuccessMessage = Message_;
                            //$scope.myForm = {};
                            $scope.ErrorMessage = "";
                            $scope.show="false";
                            $scope.txt_trnDate = new Date();
                            $scope.myForm.$setPristine();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else {
                    $scope.ErrorMessage = "Stock Distribution can not be zero or greater than available stock";
                }
            } else
            {
                $scope.IsExpenseType = "1";
            }
        } else
        {
            $scope.ErrorMessage = "Please fill all details.";
        }
    };
    
     $scope.backToDistribution = function () {
        $scope.dd_expnsType = "0";
        $scope.txt_QtyDistributed = "";
        $scope.QtyReplaced = "";
        $scope.QtyBD = "";
        $scope.txt_comments = "";
        $scope.SuccessMessage = "";
        $scope.ErrorMessage = ""; 
        $scope.show = 'false';
        $scope.txt_trnDate = new Date();
        $scope.myForm.$setPristine();
    };


    //Kaustubh Section  Stock Transfer Section
     $scope.STto_Date =''; //tdays date
     $scope.STfrom_Date = '';
     $scope.STsend="";
     
     
     // date range validation
    $scope.getDate = function(){
         if($scope.STfrom_Date !=''){
            //$scope.from_Date = new Date();
            $scope.STfrom_Date = new Date(
                    $scope.STfrom_Date.getFullYear(),
                    $scope.STfrom_Date.getMonth(),
                    $scope.STfrom_Date.getDate(),
                    0, 0, 0, 0
                    );
            $scope.STfrom_Date = new Date($scope.STfrom_Date.getTime() - ($scope.STfrom_Date.getTimezoneOffset()) * 60000);
            
        }
        if($scope.STto_Date !=''){
            $scope.STsend = new Date(
                    $scope.STto_Date.getFullYear(),
                    $scope.STto_Date.getMonth(),
                    $scope.STto_Date.getDate(),
                    23, 59, 59, 0
                    );
            //convert to date with specific time
            $scope.STsend = new Date($scope.STsend.getTime() - ($scope.STsend.getTimezoneOffset()) * 60000);
        }
       
     };

    $scope.GetStockTrasnferList = function () {
        //$http.get('fetchStockTransfer').then(function (response) {
        
        if($scope.STfrom_Date !='' && ($scope.STsend!='' && $scope.STsend < $scope.STfrom_Date)){
            $scope.STnoRecord="Please select valid range.";
             $scope.StockTransferList = [];
        }
        else{
            if ($scope.Uname != "0" && $scope.Wname != "0") {
                // $scope.ErrorMessage = "Please Select Only one User";
                $scope.STnoRecord = "Please Select Only one User.";
                $scope.StockTransferList = [];
            }
            else {
                $http.post('fetchStockTransfer', {"State": $scope.StateSearch, "District": $scope.DistrictSearch, 'Username': $scope.Uname, 'Wname': $scope.Wname, 'Status': $scope.Status, "Appliance":$scope.applienceType, "StockType":$scope.stockType ,"Brand":$scope.brandName, "From": $scope.STfrom_Date, "To": $scope.STsend}).then(function (response) {
                    var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == 1)
                    {
                        console.log(Status_);
                        var Data_ = Response_[0]["data"];
                        if (Data_.length == 0) {
                            $scope.STnoRecord = "No details found for Stock Transfer.";
                            $scope.StockTransferList = [];
                        }
                        else {
                            $scope.StockTransferList = Data_;
                            $scope.STnoRecord = "";
                        }

                    } else
                    {
                        console.log(Status_);
                        $scope.ErrorMessage = Message_;
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
            }
         }
    };
    $scope.GetStockTrasnferList();


    //func to fetch appliancelist
    $scope.GetApplianceList = function () {
        $http.get('fetchApplianceList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.ApplianceList = Data_;
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApplianceList();

    //funct to get list of counter user
    $scope.GetCounterUserList = function () {
        $http.post('fetch-users', {'RoleType': 'COUNTER', 'StateName':$scope.State, 'DistrictName':$scope.District, 'WarehouseName':"0", Para:0}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.UserList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };

    //func to fetch List of  warehouses
    $scope.GetWarehouseName = function () {
        $http.post('fetchWarehouseList', {'State':$scope.State, 'District':$scope.District}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.WhList = Data_;
                
                if($scope.Role == 'WAREHOUSEADMIN'){
                    $scope.WhList = $scope.WhList.filter(x => x.Name_ == $scope.Wname);
                }
                $scope.GetFilter();
               // $scope.TmpList = Data_;
                //console.log($scope.WhList);
            
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    
    //func used to filter other warehouse basiss of one warehouse
    $scope.GetFilter = function () {
        if($scope.Uname=='0'){
            //console.log('hii');
            $http.post('fetchWarehouseList', {'State':'0', 'District':'0'}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.TmpList = Data_;
                
                if($scope.dd_partyType!="0"){
                    $scope.dd_usernameTo="";
                    $scope.TmpList.splice($scope.TmpList.findIndex(p => p.Name_ == $scope.dd_partyType), 1);
                }
                else{
                     $scope.dd_usernameTo="";
                     $scope.TmpList = Data_; 
                }
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        }
    };
    
    //$scope.GetFilter();

    //func to get brand List
    $scope.GetBrandList = function () {
        //$scope.STOCKs;
        if ($scope.dd_applienceType != "0")
        {
            $http.post('fetchBrandList', {"ApplianceName": $scope.dd_applienceType}).then(function (response) {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == 1)
                {
                    var Data_ = Response_[0]["data"];
                    $scope.BrandList = Data_;
                } else
                {
                    $scope.ErrorMessage = Message_;
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        } else
        {
            $scope.BrandList = "";
        }
    }


    // to get available stock

    $scope.GeAvailableStock = function () {
         if ($scope.DRole == 'COUNTER') {
             $scope.url="fetchCounterStock1";
         }
         else if ($scope.DRole == 'WAREHOUSEADMIN' && $scope.TrnType != 'VEN_WH'){
            $scope.url = "fetchWarehouseStock1";
         }
         
      
        if ($scope.TrnType !='VEN_WH' && $scope.dd_partyType!="0") {
            $http({
                    method: 'post',
                    url: $scope.url,
                    data: {"Username": $scope.dd_partyType, "ApplianceName": $scope.dd_applienceType, "stockType": $scope.dd_transferType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response){
                    
                 var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == 1)
                    {
                        var Data_ = Response_[0]["data"];
                        $scope.Available = Data_[0]['QtyAvailable'];
                        // $scope.UserList = Data_;
                        $scope.ErrorMessage = '';
                        if($scope.Available==0){
                             $scope.hide = 'true';
                             $scope.ErrorMessage = 'Stock is not present for specified user';
                        }
                    } else
                    {
                        $scope.Available = 0;
                        $scope.hide = 'true';
                      $scope.ErrorMessage = 'Stock is not present for specified user';
                  }
                }, function (response) {
                     $scope.hide = 'true';
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
         }else{
             if($scope.openForm==true){
                    console.log($scope.open);
                    $scope.hide = 'false'; 
             }
         }
   
    };


    // SNT 24th Feb 2020 method func save stock transfer
    $scope.SaveStockTransferDetails = function (IsValid)
    {
            if (IsValid) {
                if ($scope.TrnType == 'VEN_WH'){
                    
                    if ($scope.dd_brandName == '' || $scope.dd_brandName == undefined || $scope.dd_brandName=='0') {
                            $scope.dd_brandName = "";
                        }
                        
                    $scope.TransactionDate = new Date( $scope.TransactionDate.getTime() -($scope.TransactionDate.getTimezoneOffset())*60000);
                     $http.post('saveStockTransfer', {"Username": $scope.dd_partyType, "StockType": $scope.dd_transferType, "TrnType": $scope.TrnType, "TrnDate": $scope.TransactionDate, "Name_": $scope.dd_brandName, "ApplianceName": $scope.dd_applienceType, "Qty": $scope.txt_quantity, "Username_To": $scope.dd_usernameTo, "Comments": $scope.txt_comments}).then(function (response) {
                        
                            var Response_ = response["data"];
                            var Status_ = Response_[0]["status"];
                            var Message_ = Response_[0]["message"];
                            if (Status_ == 1)
                            {
                                //$scope.GetUserList();
                                $scope.GetApplianceList();
                                $scope.GetBrandList();
                                $scope.GetStockTrasnferList();
                                $scope.SuccessMessage = Message_;
                                $scope.ErrorMessage = "";
                                $scope.StockTransfer.$setPristine();
                               // $scope.TransactionDate = new Date();
                                $scope.back();

                            } else
                            {
                                $scope.ErrorMessage = Message_;
                            }
                        }, function (response) {
                            $scope.ErrorMessage = "Unknown error occured. Please try again later";

                        });

                }
                else{
                    
                    if ($scope.txt_quantity > 0 && $scope.Available >= $scope.txt_quantity) {
                        $scope.TransactionDate = new Date( $scope.TransactionDate.getTime() -($scope.TransactionDate.getTimezoneOffset())*60000);
                        if ($scope.dd_brandName == '' || $scope.dd_brandName == undefined || $scope.dd_brandName=='0') {
                            $scope.dd_brandName = "";
                        }
                        $http.post('saveStockTransfer', {"Username": $scope.dd_partyType, "StockType": $scope.dd_transferType, "TrnType": $scope.TrnType, "TrnDate": $scope.TransactionDate, "Name_": $scope.dd_brandName, "ApplianceName": $scope.dd_applienceType, "Qty": $scope.txt_quantity, "Username_To": $scope.dd_usernameTo, "Comments": $scope.txt_comments}).then(function (response) {
                            var Response_ = response["data"];
                            var Status_ = Response_[0]["status"];
                            var Message_ = Response_[0]["message"];
                            if (Status_ == 1)
                            {
                                //$scope.GetUserList();
                                $scope.GetApplianceList();
                                $scope.GetBrandList();
                                $scope.GetStockTrasnferList();
                                $scope.SuccessMessage = Message_;
                                $scope.ErrorMessage = "";
                                $scope.StockTransfer.$setPristine();
                               // $scope.TransactionDate = new Date();
                                $scope.back();

                            } else
                            {
                                $scope.ErrorMessage = Message_;
                            }
                        }, function (response) {
                            $scope.ErrorMessage = "Unknown error occured. Please try again later";

                        });

                    } else {
                            $scope.ErrorMessage = "Stock Tranfer cannot be zero or greater than available stock";
                    }
                }

            } else {
                $scope.ErrorMessage = "Please fill all details.";
            }

    };


    // pagination common
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };

    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };

   
    
    $scope.GetWhList = function (TrnType, StockType, text, para) {

        $scope.txt = '';
        $scope.txt_comments = "";
        $scope.dd_brandName = "0";
        $scope.dd_applienceType = "0";
        $scope.txt_quantity = "";
        $scope.dd_usernameTo = "";
        $scope.Available = "";
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.dd_partyType = "0";
        $scope.TransactionDate = new Date();
        if (UserSesionService.RoleType == 'COUNTER') {
            $scope.dd_partyType = UserSesionService.Username;
        } else if (UserSesionService.RoleType == 'WAREHOUSEADMIN') {
            $scope.dd_partyType = UserSesionService.WarehouseName;
        }
        else{
            $scope.Uname='0';
        }
        
        
        // reinitialize variables
        $scope.show = 'true';
        $scope.hide = 'true';
        $scope.txt = para;
        $scope.TrnType = TrnType;
        $scope.STOCK = StockType;
        $scope.dd_transferType = StockType;
        $scope.openForm=false;
        //console.log( $scope.dd_transferType);
        $scope.dd_transferType= $scope.STOCK;// 26-03
        
        if ($scope.TrnType == 'CTR_WH_F' || $scope.TrnType == 'CTR_WH_D') {
            $scope.DRole = 'COUNTER';
            $scope.openForm=true;

        } else if ($scope.TrnType == 'WH_WH_F' || $scope.TrnType == 'WH_WH_D') {
            $scope.DRole = 'WAREHOUSEADMIN';
            $scope.openForm=true;
            $scope.GetWarehouseName();
        }
        else if ($scope.TrnType == 'WH_VEN_F' || $scope.TrnType == 'WH_VEN_D') {
            $scope.DRole = 'WAREHOUSEADMIN';
            $scope.openForm=true;
        }
        else if ($scope.TrnType == 'VEN_WH') {
            $scope.DRole = 'WAREHOUSEADMIN';
            $scope.openForm=true;
            if (UserSesionService.RoleType == 'WAREHOUSEADMIN') {
                $scope.dd_partyType = "0";
                $scope.openForm=true;
            }
            $scope.select=true;
        }
         else if ($scope.TrnType == 'WH_CTR_F') {
            $scope.DRole = 'WAREHOUSEADMIN';
            //alert($scope.RoleType);
        }
        $scope.GetCounterUserList();
        $scope.GetWarehouseName();
        $scope.text = text;
        
    };
   

    $scope.back = function () {
        $scope.show = 'false';
        $scope.TransactionDate = new Date();
        $scope.StockTransfer.$setPristine();
    };

    $scope.display = function () {
        if ($scope.dd_applienceType != "0") {
            $scope.hide = 'false';
        } 
        else {
            $scope.hide = 'true';
        }
        
       // console.log($scope.hide);
       // console.log($scope.Available);

    };
   
    $scope.refresh = function (){
        $scope.dd_applienceType = "0";
        $scope.txt_comments = "";
        $scope.UsernameTo="0";
        $scope.dd_partyType = "0";
        $scope.TransactionDate = new Date();
        if (UserSesionService.RoleType == 'COUNTER') {
            $scope.dd_partyType = UserSesionService.Username;
        } else if (UserSesionService.RoleType == 'WAREHOUSEADMIN') {
            $scope.dd_partyType = UserSesionService.WarehouseName;
        }
        else{
            $scope.Uname='0';
        }
    };
    
   
    $scope.onKeydown= function ($event){
       if(event.keyCode==13){
             //alert('hii');
             event.preventDefault();
       }
       
    };
    
    
    // for search
     $scope.GetStateList = function () {
        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetStateList();

    $scope.GetDistrictList = function () {

        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
//                $scope.DistrictList = Data_;
//                if($scope.StateSearch=="0")
//                {
//                     $scope.DistrictList = $scope.DistrictList.filter(x => x.StateName == $scope.StateSearch);
//                }
                
                
                if ($scope.StateSearch!="0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.DistrictSearch="0";
                    $scope.Uname="0";
                    $scope.Wname="0";
                    $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
                    
                    //console.log( $scope.DistrictList);
                }
                else if($scope.StateSearch!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN') || (UserSesionService.RoleType == 'COUNTER'))){
                     $scope.DistrictList = Data_;
                }
                else{
                    $scope.District="0";
                    $scope.DistrictList=[];
                    $scope.Uname="0";
                    $scope.Wname="0";
                }
                $scope.GetUserList();
                $scope.FetchWarehouseList();
               
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetDistrictList();


    $scope.GetUserList = function () {
        //alert($scope.StateSearch);
        //alertalert($scope.DistrictSearch);
        $http.post('fetch-users', {StateName: $scope.StateSearch, RoleType: "0", DistrictName: $scope.DistrictSearch, WarehouseName: $scope.WarehouseName, Para:0}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
//                $scope.UList = Data_;
//                console.log($scope.UList);
                if(UserSesionService.RoleType == 'WAREHOUSEADMIN' || UserSesionService.RoleType == 'COUNTER'){
                  $scope.UList = Data_;
                }
                else{
                    $scope.Uname="0";
                    $scope.UList = Data_;
                }
            } else
            { 
                $scope.UList = [];
                $scope.ErrorMessage ='';
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetUserList();
    
    $scope.FetchWarehouseList = function () {
        $http.post('fetchWarehouseList',{ "State": $scope.StateSearch, "District": $scope.DistrictSearch}).then(function (response) {
            //console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            //console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                //$scope.WarehouseList = Data_;
                //console.log($scope.ApplianceList);
                
               if(UserSesionService.RoleType == 'WAREHOUSEADMIN' || UserSesionService.RoleType == 'COUNTER'){
                   $scope.WarehouseList = Data_;
                }
                else{
                    $scope.Wname="0";
                     $scope.WarehouseList = Data_;
                }
            } else
            {
                $scope.ErrorMessage = '';
                $scope.WarehouseList=[];
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.FetchWarehouseList();


    $scope.FetchBrandList = function () {
        //$scope.STOCKs;
        if ($scope.stockType != "FRESH")
        {
            $http.post('fetchBrandList', {"ApplianceName": $scope.applienceType}).then(function (response) {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == 1)
                {
                    var Data_ = Response_[0]["data"];
                    $scope.brandName = "0";
                    $scope.BrandList = Data_;
                } else
                {
                    $scope.ErrorMessage = Message_;
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        } else
        {
            $scope.brandName = "0";
            $scope.BrandList = [];
        }
    };
    //$scope.FetchBrandList();
    
    $scope.FetchStock = function(){
       $scope.stockType="0";
       $scope.brandName = "0";
       $scope.BrandList = [];
    };
});