app.controller('ExpenseController', function ($scope, $http, UserSesionService, $mdDialog, $timeout) {
//    Captcha();
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.IsExpenseType = "0";
    $scope.Unapprovecash = "0";
    $scope.Approvecash = "0";
    $scope.TotalAmount = "0";
    $scope.IsCashInHandPresent = true;
    $scope.StateList = [];
    $scope.DistrictList = [];
    $scope.UserList = [];
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    $scope.IsUserEnable =false;
    $scope.IsDistrictEnable =false;
    $scope.IsStateEnable =false;
    $scope.panel= true;
    $scope.noRecord= "";
    $scope.WarehouseName = UserSesionService.WarehouseName;
    $scope.RoleType = UserSesionService.RoleType;
    $scope.PrevNoofMonths = 2;
    $scope.PrevNoofDays = 3;
    $scope.add = 2;
    $scope.Status="0";
    $scope.ExpenseList=[];
    $scope.Username = UserSesionService.Username;
    $scope.Prevday=6;
    $scope.expnsType="0";
    if ($scope.RoleType == "COUNTER")
    {
        $scope.User = UserSesionService.Username.toUpperCase();
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
        $scope.IsUserEnable =true;
        //$scope.panel= false;
        
    } else if ($scope.RoleType == "WAREHOUSEADMIN")
    {
        $scope.User = UserSesionService.Username.toUpperCase();
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
        $scope.IsUserEnable =true;
        
    } else if ($scope.RoleType == "DISTRICTADMIN")
    {
        $scope.User = "0";
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
    } else if ($scope.RoleType == "STATEADMIN")
    {
        $scope.User = "0";
        $scope.IsStateEnable =true;
    }
    else
    {
        $scope.User="0";
    }

    $scope.txt_trnDate = new Date();
    $scope.minDate = new Date(
            $scope.txt_trnDate.getFullYear(),
            //$scope.txt_trnDate.getMonth() - $scope.PrevNoofMonths,
            $scope.txt_trnDate.getMonth(),
            $scope.txt_trnDate.getDate() - $scope.Prevday
            );
    $scope.maxDate = new Date(
            $scope.txt_trnDate.getFullYear(),
            $scope.txt_trnDate.getMonth(),
            $scope.txt_trnDate.getDate()
            );
    this.isOpen = false;


    $scope.column = 'Transaction Date';
    $scope.reverse = false;


    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };
    
    // date range for search
     $scope.to_Date =''; //tdays date
     $scope.from_Date = '';
     $scope.send="";
     
    $scope.getDate = function(){
         if($scope.from_Date !=''){
            //$scope.from_Date = new Date();
            $scope.from_Date = new Date(
                    $scope.from_Date.getFullYear(),
                    $scope.from_Date.getMonth(),
                    $scope.from_Date.getDate(),
                    0, 0, 0, 0
                    );
            $scope.from_Date = new Date($scope.from_Date.getTime() - ($scope.from_Date.getTimezoneOffset()) * 60000);
            
        }
        if($scope.to_Date !=''){
            $scope.send = new Date(
                    $scope.to_Date.getFullYear(),
                    $scope.to_Date.getMonth(),
                    $scope.to_Date.getDate(),
                    23, 59, 59, 0
                    );
            //convert to date with specific time
            $scope.send = new Date($scope.send.getTime() - ($scope.send.getTimezoneOffset()) * 60000);
        }
       
     };
    
     $scope.GetExpenseList = function () {
         
        if($scope.from_Date !='' && ($scope.send!='' && $scope.send < $scope.from_Date)){
            $scope.noRecord="Please select valid range.";
            $scope.ExpenseList=[];
        }
        else{
        $http.post('expense-get', {"State": $scope.State, "District": $scope.District, "User": $scope.User, 'Status':$scope.Status, 'ExpType': $scope.expnsType, "From": $scope.from_Date, "To":$scope.send}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
               // $scope.ExpenseList = Data_;
                if(Data_.length ==0){
                    $scope.noRecord="No details found for Expenses.";
                     $scope.ExpenseList=[];
                }
                else{
                    $scope.ExpenseList = Data_;
                    $scope.noRecord="";
                }
                

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    }
    };
    $scope.GetExpenseList();

    $scope.SaveExpenseDetails = function (isValid) {
        
        $scope.ErrorMessage = '';
        $scope.SuccessMessage = '';
//        $scope.showAlert();
        
        if (isValid) {
            $scope.txt_trnDate = new Date( $scope.txt_trnDate.getTime() -($scope.txt_trnDate.getTimezoneOffset())*60000);
            if ($scope.dd_expnsType != "0")
            {

                if ($scope.TotalAmount > 0 && $scope.TotalAmount >= $scope.txt_amountSpent)
                {
                    
                    
                    $http({
                        method: 'post',
                        url: 'expense-save',
                        data: {"csrf_token_name": $scope.token_, "TrnDate": $scope.txt_trnDate, "ExpenseType": $scope.dd_expnsType, "AmountSpent": $scope.txt_amountSpent, "Comments": $scope.txt_comments},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $mdDialog.cancel();
                            $scope.GetExpenseList();
                            $scope.FetchCashForUser();
                            $scope.ErrorMessage = '';
                            $scope.SuccessMessage = Message_;
                            $scope.txt_trnDate = new Date();
                            $scope.dd_expnsType = "0";
                            $scope.txt_amountSpent = "";
                            $scope.txt_comments = "";
                            $scope.txt_trnDate = new Date();
//                            $scope.captcha_value='';
                            $scope.myForm.$setPristine();
                            
                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else
                {
                    $scope.ErrorMessage = "Amount Spent can not be zero or  greater than Cash In Hand";
                }
            } else
            {
               
                 $scope.IsExpenseType = "1";
            }
        } else
        {
            $scope.ErrorMessage = "Please fill all details.";
        }
        
//         $mdDialog.cancel();
    };

    $scope.FetchCashForUser = function () {

        $http({
            method: 'post',
            url: 'fetchCashDetailsByUser',
            data: {"Username": ""},
            headers: {'Content-Type': 'application/json'}
        }).then(function (data)
        {
            var Response_ = data["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                if (Response_[0]["data"].length > 0)
                {
                    var FilteredData = Response_[0]["data"];
                    $scope.Unapprovecash = FilteredData[0]["Unapprovecash"];
                    $scope.Approvecash = FilteredData[0]["Approvecash"];
                    if (parseInt($scope.Unapprovecash) < parseInt($scope.Approvecash))
                    {
                        $scope.TotalAmount = $scope.Approvecash - $scope.Unapprovecash;
                    } else
                    {
                        $scope.TotalAmount = $scope.Unapprovecash - $scope.Approvecash;
                    }
                    $scope.IsCashInHandPresent = true;
                } else
                {
                    $scope.IsCashInHandPresent = false;
                    $scope.ErrorMessage = "Cash In Hand not present for specified user";
                }

            } else
            {
                $scope.IsCashInHandPresent = false;
                $scope.ErrorMessage = 'Cash In Hand not present for specified user';
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });

    };
    $scope.FetchCashForUser();

    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };

    $scope.GetStateList = function () {

        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        
    };
    $scope.GetStateList();

    $scope.GetDistrictList = function () {
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                //$scope.DistrictList = Data_;
//                if($scope.State!="0")
//                {
//                    alert('hii');
//                     $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
//                }
                
                if ($scope.State!="0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.District="0";
                    $scope.User="0";
                    $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
                    $scope.GetUserList();
                    //console.log( $scope.DistrictList);
                }
                else if($scope.State!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN') || (UserSesionService.RoleType == 'COUNTER'))){
                     $scope.DistrictList = Data_;
                     $scope.GetUserList();
                }
                else{
                    $scope.District="0";
                    $scope.DistrictList=[];
                    $scope.User="0";
                    $scope.GetUserList();
                }
                
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetDistrictList();


    $scope.GetUserList = function () {

        $http.post('fetch-users', {StateName: $scope.State, RoleType: $scope.userRole, DistrictName: $scope.District, WarehouseName: $scope.WarehouseName, Para:0}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                if(UserSesionService.RoleType == 'WAREHOUSEADMIN' || UserSesionService.RoleType == 'COUNTER'){
                  $scope.UserList = Data_;
                }
                else{
                    $scope.User="0";
                    $scope.UserList = Data_;
                }

            } else
            {
                $scope.ErrorMessage = '';
                $scope.User="0";
                $scope.UserList=[];
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
   // $scope.GetUserList();

    $scope.onKeydown = function ($event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }

    };
    
    
//   $scope.showAlert = function(ev) {
//    $mdDialog.show({
//      template: '<md-dialog id="plz_wait" style="box-shadow:none;background:transparent">' +
//        '<md-dialog-content layout="row" layout-margin layout-padding layout-align="center center" aria-label="wait">' +
//        '<md-progress-circular md-mode="indeterminate" md-diameter="40" ></md-progress-circular>' +
//        '</md-dialog-content>' +
//        '</md-dialog>',
//      parent: angular.element(document.body),
//      clickOutsideToClose: false,
//      fullscreen: false,
//      escapeToClose: false
//    });
//  };


    $scope.onKeydown= function ($event){
       if(event.keyCode==32){
             event.preventDefault();
       }
    };
    
    
//    $scope.validate = function(){
//     
////           var captch =  (document.getElementById('mainCaptcha').value).split(' ').join('');
//           var captch =  (document.getElementById('mainCaptcha').innerHTML).split(' ').join('');
//           var captch_val = $scope.captcha_value;
//           if(captch == captch_val){
//               alert('success');
//           }
//           else{
//               alert('fail');
//               Captcha();
//               $scope.captcha_value='';
//           }
//    }
    
});


