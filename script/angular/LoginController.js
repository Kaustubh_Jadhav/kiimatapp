app.controller('LoginController', function ($scope, $http) {
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.submitLoginForm = function () {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        if ($scope.user.UserName != "" && $scope.user.UserName != null && typeof $scope.user.UserName != undefined)
        {
            if ($scope.user.Password != "" && $scope.user.Password != null && typeof $scope.user.Password != undefined)
            {
                $http.post('authenticate-user', {student: $scope.user}).then(function (data)
                {
                    var Response_ = data["data"];
                    var Status_ = Response_["status"];
                    var Message_ = Response_["message"];
                    var Data_ = Response_["data"];
                    if (Status_ == "1")
                    {
                        var RoleType = Data_[0]['RoleType'];
                        $scope.SuccessMessage = Message_;
                        if(RoleType == 'COUNTER'){
                            location.href = 'mis-entry';
                        }
                        else if(RoleType == 'WAREHOUSEADMIN'){
                             location.href = 'stock-transfer';
                        }
                        else {
                             location.href = 'approval';
                        }
                        
                    } else
                    {
                        $scope.ErrorMessage = 'Invalid Credentials.';
                       // console.log("hiii");
                        //console.log($scope.ErrorMessage);
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
            } else
            {
                $scope.ErrorMessage = "Password required";
            }

        } else
        {
            $scope.ErrorMessage = "Username required";
        }
    };
});
