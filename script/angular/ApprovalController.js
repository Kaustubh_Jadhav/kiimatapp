app.controller('ApprovalController', function ($scope, $location, $anchorScroll, $window, $http, UserSesionService, fileUpload, $mdDialog, $timeout) {
    $scope.Statesearch = "0";
    $scope.Districtsearch = "0";
    $scope.Users = "0";
    $scope.StockApprovalList = "";
    $scope.IsDataForApproval = 0;
    $scope.IsDataShow = 0;
    $scope.IsCashDataShow = 0;
    $scope.IsExpDataShow = 0;
    $scope.SelectedData = [];
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.txt_remarks = {text: ""};
    $scope.column = 'TrnDate';
    $scope.reverse = false;
    $scope.IsFocus = false;
    $scope.show = false;
    $scope.noRecord="";
    $scope.ExpRecord="";
    $scope.stockRecord="";
    $scope.Uname = UserSesionService.Username;
    $scope.expshow= true;
    $scope.approveRejectMsg="";
    $scope.tabChange=function(){
         $scope.IsDataShow = 0;
         $scope.IsCashDataShow = 0;
         $scope.IsExpDataShow = 0;
         $scope.ErrorMessage = "";
         $scope.SuccessMessage = "";
    };
    $scope.panel=false;
     $scope.windowWidth = $window.innerWidth;
     var w = angular.element($window);
   
//    w.bind('resize', function () {
//         $scope.$apply(function() {
//                 $scope.windowWidth = $window.innerWidth;
//                 
//         });
//                 });
            
//   var w = angular.element($window);
//    w.bind('resize', function () {
//        $scope.windowWidth = $window.innerWidth;
//        alert( $scope.windowWidth);
//    });
//    $(window).resize(function() {
//     $scope.windowWidth = $( window ).width();
//   });
   // $scope.isCheck = false;
    //alert($scope.IsCashDataShow);
    // Pending
   /* if (UserSesionService.RoleType == 'COUNTER') {
        $scope.Username = UserSesionService.Username;
    } else if (UserSesionService.RoleType == 'WAREHOUSEADMIN') {
      $scope.Username = UserSesionService.WarehouseName;//change
     } else {
      $scope.Username = UserSesionService.Username;
     }*/
     
     
     if (UserSesionService.RoleType == "DISTRICTADMIN")
    {
        $scope.Districtsearch = UserSesionService.DistrictName;
        $scope.Statesearch = UserSesionService.StateName;
        $scope.Username = UserSesionService.Username;
        $scope.sdisabled=true;
        $scope.ddisabled=true;
        $scope.panel=true;
       
    } else if (UserSesionService.RoleType == "STATEADMIN")
    {
        $scope.Statesearch = UserSesionService.StateName;
        $scope.Username = UserSesionService.Username;
        $scope.sdisabled=true;
       
    } else if (UserSesionService.RoleType == 'WAREHOUSEADMIN')
    {
        $scope.Districtsearch = UserSesionService.DistrictName;
        $scope.Statesearch = UserSesionService.StateName;
        $scope.Username = UserSesionService.WarehouseName;
        $scope.ddisabled=true;
        $scope.sdisabled=true;
        $scope.panel=true;
        
    } else if (UserSesionService.RoleType == 'SUPERADMIN') {
        //$scope.Statesearch = "0";
        $scope.Statesearch = "0";
        $scope.Districtsearch = "0";
        $scope.Username = UserSesionService.Username;
      
       
    }else if (UserSesionService.RoleType == 'COUNTER') {
        $scope.Districtsearch = UserSesionService.DistrictName;
        $scope.Statesearch = UserSesionService.StateName;
        $scope.Username = UserSesionService.Username;
        $scope.sdisabled=true;
        $scope.ddisabled=true;
        $scope.panel=true;
    }

    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };

    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };

    

    //stock approval
    $scope.GetApprovalList = function () {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $http.post('fetch-approvalList', {"Username": $scope.Username, "Status": "Pending", "RoleType":UserSesionService.RoleType, "State":$scope.Statesearch, "District":$scope.Districtsearch}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StockApprovalList = Data_;
                
                if($scope.StockApprovalList.length ==0){
                    $scope.stockRecord="No details found for approvals";
                    $scope.StockApprovalList=[];
                }
                else{
                    $scope.stockRecord="";
                }
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApprovalList();

    $scope.OpneAction = function (Type, Id)
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataForApproval = Type;
        $scope.SelectedData = $scope.StockApprovalList[$scope.StockApprovalList.findIndex(x => x.Id == Id)];
        $scope.IsDataShow = 1;
        $scope.IsCashDataShow = 0;
        //$("body").scrollTop(0);
//        w.bind('resize', function () {
//            $scope.windowWidth = $window.innerWidth;
//        });
//
//        if ($scope.windowWidth < 992) {
//            $("body").scrollTop(1000);
//        }
//        else {
//            $("body").scrollTop(0);
//        }           

    };
    
    $scope.OpneCashAction = function (Type, Id)
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataForApproval = Type;
        $scope.SelectedData = $scope.CashApprovalList[$scope.CashApprovalList.findIndex(x => x.Id == Id)];
        $scope.IsCashDataShow = 1;
        $scope.IsDataShow = 0;
        //$("body").scrollTop(0);
//        w.bind('resize', function () {
//            $scope.windowWidth = $window.innerWidth;
//        });
//
//        if ($scope.windowWidth < 992) {
//            $("body").scrollTop(1000);
//        }
//        else {
//            $("body").scrollTop(0);
//        }
    };
    
    $scope.OpneExpenseAction = function (Type, Id)
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataForApproval = Type;
        $scope.SelectedData = $scope.ExpApprovalList[$scope.ExpApprovalList.findIndex(x => x.Id == Id)];
        $scope.IsExpDataShow = 1;
        $scope.IsDataShow = 0;
        $scope.IsCashDataShow = 0;
//        $scope.myFile = undefined;
//        w.bind('resize', function () {
//            $scope.windowWidth = $window.innerWidth;
//        });
//
//        // alert($scope.windowWidth);
//        if ($scope.windowWidth < 992) {
//            $("body").scrollTop(1000);
//            // alert($scope.width);
//        }
//        else {
//            $("body").scrollTop(0);
//        }
       
      // focus('Form_Div');
      
      //$window.scrollTo(0, angular.element(document.getElementById('Form_Div')).offset().top);  
      //$scope.setfocus();
         //$scope.save();
    };
    
     $scope.save=function(scrollLocation){
        // location.hash = "topLink";
         //$location.hash('topLink');
         //$window.scrollTo(angular.element(document.getElementById('topLink')).offset());  
//         
//         if(location.hash) {
//               $window.scrollTo(0,$('#topLink').offset().top);
//         }
         //$anchorScroll();
         $location.hash(scrollLocation);  
         $anchorScroll(); 
        // alert('hii');
     };

    $scope.SaveComments = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.showAlert();
        if ($scope.txt_remarks.text != "" && $scope.txt_remarks.text != null)
        {
//           if($scope.UploadStatus_ == 1){
                    var Status = "APPROVED";
                     $scope.approveRejectMsg="Stock Transfer Approved."
                    if ($scope.IsDataForApproval != 1)
                    {
                        Status = "REJECTED";
                         $scope.approveRejectMsg="Stock Transfer Rejected."
                    }
                    var Username_To = "";
                    if (UserSesionService.RoleType == 'COUNTER') {
                        Username_To = $scope.SelectedData["Username_To"];
                    } else if (UserSesionService.RoleType == 'WAREHOUSE') {
                        Username_To = UserSesionService.RefName;
                    } else {
                        Username_To = $scope.SelectedData["Username_To"];
                    }

                    $http({
                        method: 'post',
                        url: 'save-stockTransferApproval',
                        data: {"Id": $scope.SelectedData["Id"], "Status": Status, "ApproverRemarks": $scope.txt_remarks.text,
                            "TrnType": $scope.SelectedData["TrnType"], "Username_To": Username_To, "TrnQty": $scope.SelectedData["Qty"], "ApplianceName": $scope.SelectedData["ApplianceName"],
                            "StockType": $scope.SelectedData["StockType"], "Username_From": $scope.SelectedData["Username_From"]
                        },
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {

                            $scope.txt_remarks = {text: ""}
                            $scope.IsDataShow = 0;
                            $scope.GetApprovalList();
                            $scope.SuccessMessage =$scope.approveRejectMsg;

                        } else
                        {
                            $scope.ErrorMessage = [];
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });

//        } else
//             {
//                         $scope.ErrorMessage = "File is not uploaded.Please try again later ";
//              }    
        } else
        {
            $scope.ErrorMessage = "Please provide remarks to continue";
        }
        
         $mdDialog.cancel();
    };

    $scope.CancelPanel = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataShow = 0;
        $scope.txt_remarks = {text: ""};
        $("body").scrollTop(0);
    };
    
     $scope.CancelCashPanel = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsCashDataShow = 0;
        $("body").scrollTop(0);
        $scope.txt_remarks = {text: ""};
    };
    
     $scope.CancelExpPanel = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsExpDataShow = 0;
        $("body").scrollTop(0);
        $scope.txt_remarks = {text: ""};
    };
    
    $scope.ClearMsg = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
    };
    $scope.onKeydown = function ($event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }

    };
    
    
    
    
    // cash approval
    
     $scope.GetCashApprovalList = function () {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $http.post('fetch-cashapprovalList', {"Username": $scope.Uname, "Status": "Pending", "RoleType":UserSesionService.RoleType, "State":$scope.Statesearch, "District":$scope.Districtsearch}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.CashApprovalList = Data_;
                //alert($scope.CashApprovalList);
                
                if($scope.CashApprovalList.length ==0){
                    $scope.noRecord="No details found for approvals";
                    $scope.CashApprovalList =[];
                }
                else{
                    $scope.noRecord="";
                  
                }
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetCashApprovalList();
    
    
     $scope.SaveCashComments = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.showAlert();
        if ($scope.txt_remarks.text != "" && $scope.txt_remarks.text != null )
        {
//            if($scope.UploadStatus_ == 1){
                var Status = "APPROVED";
                $scope.approveRejectMsg="Cash Transfer Approved."
                if ($scope.IsDataForApproval != 1)
                {
                    Status = "REJECTED";
                    $scope.approveRejectMsg="Cash Transfer Rejected."
                }
                var Username_To = "";
                if (UserSesionService.RoleType == 'COUNTER') {
                    Username_To = $scope.SelectedData["Username_To"];
                } else if (UserSesionService.RoleType == 'WAREHOUSE') {
                    Username_To = UserSesionService.RefName;
                } else {
                    Username_To = $scope.SelectedData["Username_To"];
                }

                $http({
                    method: 'post',
                    url: 'save-cashTransferApproval',
                    data: {"Id": $scope.SelectedData["Id"], "Status": Status, "ApproverRemarks": $scope.txt_remarks.text,
                        "TrnAmount": $scope.SelectedData["TrnAmount"], "Username_To": Username_To, 
                        "Username_From": $scope.SelectedData["Username_From"]
                    },
                    headers: {'Content-Type': 'application/json'}
                }).then(function (response)
                {
                    var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == "1")
                    {

                        $scope.txt_remarks = {text: ""};
                        //alert($scope.SuccessMessage);
                        $scope.IsCashDataShow = 0;
                        $scope.GetCashApprovalList();
                        $scope.SuccessMessage = $scope.approveRejectMsg;

                    } else
                    {
                        $scope.ErrorMessage = Message_;
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
//                 } else
//             {
//                         $scope.ErrorMessage = "File is not uploaded.Please try again later ";
//              }    
           
        } else
        {
            $scope.ErrorMessage = "Please provide remarks to continue";
        }
        
         $mdDialog.cancel();
    };
    
    
    //Expense approval
    $scope.GetExpApprovalList = function () {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $http.post('fetch-ExpapprovalList', {"Username": $scope.Uname, "Status": "Pending", "RoleType":UserSesionService.RoleType, "State":$scope.Statesearch, "District":$scope.Districtsearch}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.ExpApprovalList = Data_;
                
                if($scope.ExpApprovalList.length ==0){
                    $scope.ExpRecord="No details found for approvals";
                    $scope.ExpApprovalList=[];
                }
                else{
                    $scope.ExpRecord="";
                }
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetExpApprovalList();
    
    
    $scope.SaveExpComments = function ()
    {
         $scope.ErrorMessage = "";
         $scope.SuccessMessage = "";
           $scope.showAlert();
        if ($scope.txt_remarks.text != "" && $scope.txt_remarks.text != null)
        {
//            if($scope.UploadStatus_ == 1){
         
                    var Status = "APPROVED";
                    $scope.approveRejectMsg = "Expense Approved."
                    if ($scope.IsDataForApproval != 1)
                    {
                        Status = "REJECTED";
                        $scope.approveRejectMsg = "Expense Rejected."
                    }
                   
                    $http({
                        method: 'post',
                        url: 'save-expTransferApproval',
                        data: {"Id": $scope.SelectedData["Id"], "Status": Status, "ApproverRemarks": $scope.txt_remarks.text,
                            "AmountSpent": $scope.SelectedData["AmountSpent"],
                            "Username": $scope.SelectedData["Username"]
                        },
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.txt_remarks = {text: ""};
                            //alert($scope.SuccessMessage);
                            $scope.IsExpDataShow = 0;
                            $scope.GetExpApprovalList();
                            $scope.SuccessMessage = $scope.approveRejectMsg;

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
//                } else
//             {
//                         $scope.ErrorMessage = "File is not uploaded.Please try again later ";
//              }    
           
        } else
        {
            $scope.ErrorMessage = "Please provide remarks to continue";
        }
         $mdDialog.cancel();
    };
    
    
    // func for stateList filter
    $scope.fetchStateList = function () {
        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";
        });
    };
    $scope.fetchStateList();
    
    
    $scope.fetchDistrictList = function () {
      
        //console.log($scope.State);
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.DistrictListsearch = Data_;
                 //alert($scope.Statesearch);
                
                if ($scope.Statesearch!="0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.Districtsearch="0";
                    $scope.DistrictListsearch = $scope.DistrictListsearch.filter(x => x.StateName == $scope.Statesearch);
                    //console.log( $scope.DistrictList);
                }
                else if($scope.Statesearch!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN') || (UserSesionService.RoleType == 'COUNTER'))){
                    $scope.DistrictListsearch = Data_;
                }
                 else{
                    $scope.Districtsearch="0";
                    $scope.DistrictListsearch=[];
                   
                }
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";
        });
    };
     $scope.fetchDistrictList();
     
     

    $scope.uploadExpFile = function(){
        if ($scope.myFile != undefined) {
                var file = $scope.myFile;
                var uploadUrl = "file-upload";
                fileUpload.uploadFileToUrl(file, uploadUrl).then(function (response) {
                    var Response_ = response["data"];
                    $scope.UploadStatus_ = Response_["status"];
                    var Upload_Data = Response_["data"];
//                    console.log($scope.UploadStatus_);
//                    console.log(Upload_Data);
                    $scope.SaveExpComments();
                });
            }
            else {
                $scope.UploadStatus_ = 1;
                   $scope.SaveExpComments();
            }
           
   };
   
//   $scope.uploadCashFile = function(){
//       
//        $scope.cashfile = document.getElementById('cashfile').files[0];
//       // alert($scope.cashfile);
//        if ($scope.cashfile != undefined) {
//                var file = $scope.cashfile;
//                var uploadUrl = "file-upload";
//                fileUpload.uploadFileToUrl(file, uploadUrl).then(function (response) {
//                    var Response_ = response["data"];
//                    $scope.UploadStatus_ = Response_["status"];
//                    var Upload_Data = Response_["data"];
//                    $scope.SaveCashComments();
//                });
//            }
//            else {
//                $scope.UploadStatus_ = 1;
//                   $scope.SaveCashComments();
//            }
//   };
   
   
//   $scope.uploadStockFile = function(){
//       
//        $scope.stockfile = document.getElementById('stockfile').files[0];
//       // alert($scope.cashfile);
//        if ($scope.stockfile != undefined) {
//                var file = $scope.stockfile;
//                var uploadUrl = "file-upload";
//                fileUpload.uploadFileToUrl(file, uploadUrl).then(function (response) {
//                    var Response_ = response["data"];
//                    $scope.UploadStatus_ = Response_["status"];
//                    var Upload_Data = Response_["data"];
//                    $scope.SaveComments();
//                });
//            }
//            else {
//                $scope.UploadStatus_ = 1;
//                   $scope.SaveComments();
//            }
//   };


$scope.showAlert = function(ev) {
    $mdDialog.show({
      template: '<md-dialog id="plz_wait" style="box-shadow:none;background:transparent">' +
        '<md-dialog-content layout="row" layout-margin layout-padding layout-align="center center" aria-label="wait">' +
        '<md-progress-circular md-mode="indeterminate" md-diameter="40" ></md-progress-circular>' +
        '</md-dialog-content>' +
        '</md-dialog>',
      parent: angular.element(document.body),
      clickOutsideToClose: false,
      fullscreen: false,
      escapeToClose: false
    });
  };
});