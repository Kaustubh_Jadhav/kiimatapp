app.controller('SaleController', function ($scope, $http,$filter, UserSesionService, uiGridGroupingConstants, uiGridConstants, $timeout, $element) {
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.StateList = [];
    $scope.DistrictList = [];
    $scope.UserList = [];
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    $scope.WarehouseName = UserSesionService.WarehouseName;
    $scope.RoleType = UserSesionService.RoleType; 
    $scope.Status='0';
    $scope.Appliance = '0';
    $scope.PrevNoofDays = 3;
    $scope.add = 2;
    $scope.noRecord = "";
    $scope.filterText = '';
    if ($scope.RoleType == "SUPERADMIN")
    {
        $scope.State = "0";
        $scope.District = "0";
    }

    $scope.UserList = [];
    // date for sale list filter
    
     $scope.txt_trnDate = new Date();
     $scope.to_Date = new Date();
     $scope.from_Date =  new Date(
            $scope.to_Date.getFullYear(),
            $scope.to_Date.getMonth(),
            $scope.to_Date.getDate()- $scope.PrevNoofDays
            );
    
    $scope.from_Date=new Date( $scope.from_Date.getTime()-($scope.from_Date.getTimezoneOffset())*60000);
    this.isOpen = false;
    
    
   

   // $scope.Username = UserSesionService.Username;
    $scope.gridOptions = {
    enableFiltering: false,
    excessRows:1500,
    treeRowHeaderAlwaysVisible: false,
    paginationPageSize: 1,
    enablePaginationControls: true,
    enableGridMenu: true,
    enableCellEdit: false,
    showGridFooter: true,
    groupingShowCounts: false,
    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
    enableVerticalScrollbar: 1,
    allowCellFocus: false,
    minimumColumnSize:120,
    columnDefs: [
      { displayName: 'Appliance', field: 'ApplianceName',  grouping: { groupPriority: 0 }, sort: {  direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { displayName: 'State', field: 'StateName',  grouping: { groupPriority: 1 }, sort: {  direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { displayName: 'District', field: 'DistrictName',grouping: { groupPriority: 2}, sort: {  direction: 'asc' }, cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { displayName: 'Counter', field: 'Username', grouping: { groupPriority: 3},width:150, sort: { direction: 'asc' },  cellTemplate: '<div><div {{row.entity[col.field].toUpperCase() ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'  },
      { displayName: 'Date', field: 'Date',  sort: { direction: 'desc' }},
      { displayName: 'Distributed', field: 'QtyDistributed', width:100, cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }},
      { displayName:'Replaced',field: 'QtyReplaced', cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }},
      { displayName:'BD', field: 'QtyBD',  cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }}
      ],
      gridMenuShowHideColumns:false,
      exporterMenuVisibleData:false,
      exporterMenuCsv: false,
      exporterMenuPdf: false,
      onRegisterApi: function (gridApi) {
      $scope.gridApi = gridApi;
       $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
       $timeout(function () {
            var $viewport = angular.element('.ui-grid-render-container');
            ['touchstart', 'touchmove', 'touchend','keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
                $viewport.unbind(eventName);
            });
       });
       $scope.gridApi.grid.registerDataChangeCallback(function() {
               //$scope.gridApi.treeBase.expandAllRows();
              
             });
      }
  };
  
  
  
//  $scope.filterGrid = function(value) {
////    console.log(value);
////    for (var i=0; i< $scope.gridOptions.data.length; i++) {
////        $scope.gridApi.grid.columns[i].filters[0].term = value;
////   }
// $scope.gridApi.grid.columns[4].filters[0].term = value;
//  };
  
    $scope.FetchSaleSummary = function () {
        $scope.send =  new Date(
            $scope.to_Date.getFullYear(),
            $scope.to_Date.getMonth(),
            $scope.to_Date.getDate(),
            23, 59, 59, 0
            );
        //convert to date with specific time
        $scope.send=new Date( $scope.send.getTime()-($scope.send.getTimezoneOffset())*60000);
        $http.post('available-sale', {"State": $scope.State, "District": $scope.District, "Appliance":$scope.Appliance, "From": $scope.from_Date, "To":$scope.send}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                //console.log(Status_);
                var data = Response_[0]["data"];
                
                //$scope.gridOptions.data = data;
                //console.log(data.length);
                //alert('hii');
                if(data.length == 0){
                       $scope.noRecord ="No record found";
                       $scope.gridOptions.data=[];
                }
                else{
                     $scope.gridOptions.data = data;
                     for ( var i = 0; i < data.length; i++ ){
                          data[i].Username =  data[i].Username.toUpperCase();
                      }
                    // console.log($scope.gridOptions.data.length);
                     $scope.noRecord="";
                }

            } else
            {
                //console.log(Status_);
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.FetchSaleSummary();
    
       $scope.GetApplianceList = function () {
        $http.get('fetchApplianceList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.ApplianceList = Data_;
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApplianceList();
    
    $scope.filterText = '';
    $scope.filterValue = '';
//    $scope.filterBtn = function () {
//        $scope.filterText = 'MAHARASHTRA';
//        $scope.gridApi.grid.refresh();
//    }

    $scope.filter = function () {
        $scope.filterText = $scope.filterValue;
        $scope.gridApi.grid.refresh();
    };

    $scope.singleFilter = function (renderableRows) {
//        alert($scope.filterText);
        var matcher = new RegExp($scope.filterText.toLowerCase());
        
        renderableRows.forEach(function (row) {
            var match = false;
            ['ApplianceName','StateName', 'DistrictName','Username'].forEach(function (field) {
                if (row.entity[field].toLowerCase().match(matcher)) {
                    match = true;
//                    alert(true);
                }
            });
            if (!match) {
                    row.visible = false;
//                  alert(false);
//                    $scope.gridOptions.data.length=0;
//                    $scope.noRecord ="No record found";
            }
        });
        return renderableRows;
    };
});

