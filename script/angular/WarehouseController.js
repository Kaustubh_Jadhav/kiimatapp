app.controller('WarehouseController', function ($scope, $http, UserSesionService) {

    $scope.ShowForm = 0;
    $scope.SelectedData = "";
    $scope.Id='';
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.column = 'TrnDate';
    $scope.District = "0";
    $scope.State = "0";
    $scope.wname = '';
    $scope.txt_comments = '';
    $scope.buttonShow = true;
    $scope.disabled= false;
    $scope.ddisabled=false;
    $scope.sdisabled=false;
    $scope.panel=true;
    $scope.noRecord= "";
    //search
    $scope.Statesearch = '0';
    $scope.Districtsearch = '0';
    $scope.RoleType = UserSesionService.RoleType;
   // $scope.UserRoles = UserRoles[0];
   // alert($scope.UserRoles);
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };


    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };


    $scope.OpneAction = function ()
    {
        $scope.ShowForm = 1;
        $scope.EditForm = 0;
        $scope.SuccessMessage = "";
        $scope.ErrorMessage = "";
        $scope.District = "0";
        $scope.State = "0";
        $scope.wname = '';
        $scope.disabled = false;
        $scope.model = {
            isDisabled: false
        };
        $scope.txt_comments= '';
        $scope.DistrictList=[];
        $scope.text="Add";
        $scope.SelectedData = "";
        $scope.myForm.$setPristine();
    };


    $scope.FetchWarehouseList = function () {
        $http.post('fetchWarehouseList',{ "State": $scope.Statesearch, "District": $scope.Districtsearch}).then(function (response) {
            //console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            //console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                
                if(Data_.length ==0){
                    $scope.noRecord="No details found .";
                    $scope.WarehouseList=[];
                }
                else{
                    $scope.WarehouseList = Data_;
                    $scope.noRecord="";
                }
                //console.log($scope.ApplianceList);
                
            } else
            {
                $scope.noRecord="No details found .";
                $scope.WarehouseList=[];
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    //$scope.FetchWarehouseList();


    $scope.GetStateList = function () {

        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetStateList();

    $scope.GetDistrictList = function () {
        //console.log($scope.State);
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                $scope.ErrorMessage = "";
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                if ($scope.State != "0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.District = "0";
                    $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
                }
                else if($scope.Statesearch!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN'))){
                     $scope.DistrictList = Data_;
                }
                else{
                    $scope.DistrictList = [];
                }

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };

    //$scope.GetDistrictList();
    $scope.fetchDistrictList = function () {

        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                $scope.ErrorMessage = "";
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                var Data_ = Data_.map(function (item) {
                    return {"Name_":item.Name_.toUpperCase(),"StateName":item.StateName};
                });

                //console.log(Data_);
                $scope.DistrictList = Data_;
                //console.log($scope.Districtdata);
                if ($scope.State != '0') {
                    $scope.District = '0';
                    $scope.DistrictList = $scope.DistrictList.filter(x => x.StateName == $scope.State);
                    //console.log($scope.Districtdata);
                } else if ($scope.SelectedData != '') {
                    $scope.State = $scope.DistrictList[$scope.DistrictList.findIndex(x => x.Name_.toUpperCase() == $scope.District.toUpperCase())]["StateName"];
                }
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
   

    $scope.SaveWarehouseDetails = function (isValid) {
        if (isValid) {
            
        if($scope.State!="0" && $scope.District!="0"){
           // console.log($scope.SelectedData);
           if ($scope.SelectedData == '') {
               $scope.Id ='';
           }
           else{
               $scope.Id = $scope.SelectedData["Id"];
           }
                $http({
                    method: 'post',
                    url: 'save-warehouse',
                    data: {"Id":$scope.Id, "DistrictName": $scope.District, "Name_": $scope.wname.toUpperCase(), "Description": $scope.txt_comments},
                    headers: {'Content-Type': 'application/json'}
                }).then(function (response)
                {
                    var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == "1")
                    {
                        $scope.SuccessMessage = Message_;
                        $scope.ErrorMessage = "";
                        $scope.District = "0";
                        $scope.State = "0";
                        $scope.wname = "";
                        $scope.txt_comments = '';
                        $scope.myForm.$setPristine();
                        $scope.FetchWarehouseList();
                        $scope.backToButton();

                    } else
                    {
                        $scope.ErrorMessage = Message_;
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";
                });
            
        } else {
            $scope.ErrorMessage = "Please fill all details. ";
        }
        
    }
    else{
         $scope.ErrorMessage = "Please fill all details. ";
    }
    };

    $scope.backToButton = function () {
        $scope.ShowForm = 0;
    };

    $scope.EditAction = function (Id)
    {
        $scope.SuccessMessage = '';
        $scope.ErrorMessage = '';
       // $scope.EditForm = 1;
        $scope.ShowForm = 1;
        $scope.SelectedData = $scope.WarehouseList[$scope.WarehouseList.findIndex(x => x.Id == Id)];
        $scope.District = $scope.SelectedData['DistrictName'].toUpperCase();
        if ($scope.District != '') {
            $scope.fetchDistrictList();
            //console.log($scope.eDistrict);
        }
        $scope.State = '0';
        $scope.wname = $scope.SelectedData['Name_'];
        $scope.txt_comments = $scope.SelectedData['Description'];
        $scope.disabled = true;
        
        $scope.model = {
            isDisabled: true
        };
        $scope.text="Edit";
        $scope.myForm.$setPristine();
    };

    $scope.clearScreen = function () {
        $scope.EditForm = 0;
    };
    
    $scope.onKeydown= function ($event){
       if(event.keyCode==32){
             event.preventDefault();
       }
    };
    
    
    // search filter
    if ($scope.RoleType == "DISTRICTADMIN")
    {
        $scope.Districtsearch = UserSesionService.DistrictName.toUpperCase();
        $scope.Statesearch = UserSesionService.StateName;
        $scope.sdisabled=true;
        $scope.ddisabled=true;
        $scope.panel=false;
        $scope.FetchWarehouseList();
    } else if ($scope.RoleType == "STATEADMIN")
    {
        $scope.Statesearch = UserSesionService.StateName;
        $scope.sdisabled=true;
        $scope.FetchWarehouseList();
       
    } else if ($scope.RoleType == 'WAREHOUSEADMIN')
    {
        $scope.Districtsearch = UserSesionService.DistrictName.toUpperCase();
        $scope.Statesearch = UserSesionService.StateName;
        $scope.WareHousesearch = UserSesionService.WarehouseName;
        $scope.ddisabled=true;
        $scope.sdisabled=true;
        $scope.panel=false;
        $scope.FetchWarehouseList();
        
    } else if ($scope.RoleType == 'SUPERADMIN') {
        $scope.Statesearch = "0";
        $scope.Districtsearch = "0";
        $scope.FetchWarehouseList();
    }
   

   $scope.fetchDistrictListforSearch = function () {
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                $scope.ErrorMessage = "";
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                var Data_ = Data_.map(function (item) {
                    return {"Name_":item.Name_.toUpperCase(),"StateName":item.StateName};
                });

                $scope.DistrictListSearch = Data_;
                 if ($scope.Statesearch!="0" && ($scope.RoleType == 'STATEADMIN') || ($scope.RoleType == 'SUPERADMIN'))
                {
                    $scope.Districtsearch="0";
                    $scope.DistrictListSearch = $scope.DistrictListSearch.filter(x => x.StateName == $scope.Statesearch);
                    //console.log( $scope.DistrictList);
                }else{
                    $scope.DistrictListforsearch = Data_;
                }
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
     $scope.fetchDistrictListforSearch();
    
});


