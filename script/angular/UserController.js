app.controller('UserController', function ($scope, $http, UserSesionService) {
    $scope.State = UserSesionService.StateName;
    $scope.District =UserSesionService.DistrictName;
    $scope.Users = "0";
    $scope.WareHouse="0";
    //fetching users
    $scope.userRole = "";
    
    //search
    $scope.Statesearch = "0";
    $scope.Districtsearch = "0";
    $scope.WareHousesearch="0";
    $scope.ddisabled=false;
    $scope.sdisabled=false;
    $scope.wdisabled=false;
    $scope.udisabled=false;
    
    $scope.IsDataShow = 1;
    $scope.ShowForm = 0;
    $scope.SelectedData = "";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.column = 'TrnDate';
    $scope.reverse = false;
    $scope.Role = UserSesionService.RoleType;
  
    $scope.noRecord = "";
    $scope.StateDisable= false;
    $scope.DistrictDisable = false;
    
    
    
    
    //Pagination
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };


    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };
    
    
    // function for fetching UserRole
    $scope.GetRoleList = function () {
        //console.log($scope.Username);
        $http.get('roles').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.RoleList = Data_;
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetRoleList();
    
   //funct to display User List
    $scope.GetUserList = function () {
        if($scope.WareHousesearch !="0" && $scope.userRole !=""){
            // $scope.ErrorMessage = "Please Select Only one User";
             $scope.noRecord = "Please Select Only one Between Role and Warehouse.";
             $scope.UserList = [];
        }
        else{
            $http.post('fetch-users', {'RoleType': $scope.userRole, "StateName": $scope.Statesearch, "DistrictName": $scope.Districtsearch, "WarehouseName": $scope.WareHousesearch, Para:1}).then(function (response) {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == 1)
                {
                    var Data_ = Response_[0]["data"];
                   // console.log(Data_.length);
                    if (Data_.length == 0) {
                        $scope.noRecord = "No details found for Users.";
                        $scope.UserList = [];
                    }
                    else {
                        $scope.UserList = Data_;
                        $scope.noRecord = "";
                    }
                }
                else
                {
                    $scope.UserList = [];
                    $scope.noRecord = "No details found for Users.";
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        }
        
        
    };
  
  // func to fetch state List for form
    $scope.GetStateList = function () {
        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetStateList();
    
    // func to fetch District List for form
    $scope.GetDistrictList = function () {
        //console.log($scope.State);
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                //$scope.DistrictList = Data_;
                //console.log($scope.DistrictList);
                if ($scope.State!="0"  && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.District="0";
                    $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
                    //console.log( $scope.DistrictList);
                }
                else if($scope.State!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN'))){
                     $scope.DistrictList = Data_;
                }
                else{
                    $scope.District="0";
                    $scope.WareHouse="0";
                    $scope.DistrictList = [];
                    $scope.TmpList = [];
                }
                $scope.getwhList();
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
   // $scope.GetDistrictList();

    // func to fetch WH List for form
    $scope.getwhList = function () {
            //console.log('hii');
            $http.post('fetchWarehouseList', {"State": $scope.State, "District": $scope.District}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
               
                if($scope.District!="0"){
                   $scope.WareHouse="0";
                   $scope.TmpList = Data_;
//                    $scope.WareHouse="0";
//                    $scope.TmpList = Data_.filter(p => p.DistrictName == $scope.District.toUpperCase());
                    }
                else{
                      $scope.WareHouse="0";
                      $scope.TmpList = [];
                }
                
            } else
            {
               
                $scope.WareHouse="0";
                $scope.TmpList = [];
                $scope.ErrorMessage =[];
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        
    };
    
    //$scope.getwhList();
  
    $scope.OpneAction = function ()
    {
        $scope.IsDataShow = 1;
        $scope.ShowForm =0;
        $scope.EditForm = 0;
        $scope.myForm.$setPristine();
    };
    
    
    
    

    $scope.SaveUserDetails = function (isValid) {
        if (isValid) {
            
            //alert($scope.State +"abc");
            if ($scope.RoleType == "WAREHOUSEADMIN") {
                if ($scope.Name != "" && $scope.Uname != "" && $scope.State != '0' && $scope.WareHouse != '0') {
                    $http({
                        method: 'post',
                        url: 'create-user',
                        data: {"Name_": $scope.Name, "Username": $scope.Uname, "StateName": $scope.State, "DistrictName": $scope.District, "WarehouseName": $scope.WareHouse, "RoleType": $scope.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.SuccessMessage = Message_;
                            $scope.ErrorMessage = "";
                            $scope.Name = "";
                            $scope.Uname = "";
                            $scope.State = "0";
                            $scope.District = "0";
                            $scope.myForm.$setPristine();
                            $scope.GetUserList();
                            $scope.backToButton();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else {
                    $scope.ErrorMessage = "Please fill all fields";
                }

            } else if ($scope.RoleType == "COUNTER") {
                if ($scope.Name != "" && $scope.Uname != "" && $scope.State != '0' && $scope.District != '0') {
                    $http({
                        method: 'post',
                        url: 'create-user',
                        data: {"Name_": $scope.Name, "Username": $scope.Uname, "StateName": $scope.State, "DistrictName": $scope.District, "WarehouseName":'', "RoleType": $scope.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.SuccessMessage = Message_;
                            $scope.ErrorMessage = "";
                            $scope.Name = "";
                            $scope.Uname = "";
                           // $scope.State = "0";
                           // $scope.District = "0";
                            $scope.myForm.$setPristine();
                            $scope.GetUserList();
                            $scope.backToButton();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else {
                    $scope.ErrorMessage = "Please fill all fields";
                }

            } else if ($scope.RoleType == "DISTRICTADMIN") {
               // alert("hii");
                if ($scope.Name != "" && $scope.Uname != "" && $scope.State != '0' && $scope.District != '0') {
                    $http({
                        method: 'post',
                        url: 'create-user',
                        data: {"Name_": $scope.Name, "Username": $scope.Uname, "StateName": $scope.State,"DistrictName": $scope.District, "WarehouseName":'', "RoleType": $scope.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.SuccessMessage = Message_;
                            $scope.ErrorMessage = "";
                            $scope.Name = "";
                            $scope.Uname = "";
                           // $scope.State = "0";
                            $scope.District = "0";
                            $scope.myForm.$setPristine();
                            $scope.GetUserList();
                            $scope.backToButton();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else {
                    $scope.ErrorMessage = "Please fill all fields";
                }
            } else if ($scope.RoleType == "STATEADMIN") {

                if ($scope.Name != "" && $scope.Uname != "" && $scope.State != '0') {
                    $http({
                        method: 'post',
                        url: 'create-user',
                        data: {"Name_": $scope.Name, "Username": $scope.Uname, "StateName": $scope.State, "DistrictName": '', "WarehouseName":'',  "RoleType": $scope.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.SuccessMessage = Message_;
                            $scope.ErrorMessage = "";
                            $scope.Name = "";
                            $scope.Uname = "";
                            $scope.State = "0";
                            $scope.District = "0";
                            $scope.myForm.$setPristine();
                            $scope.GetUserList();
                            $scope.backToButton();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                } else {
                    $scope.ErrorMessage = "Please fill all fields";
                }
                
            } else if ($scope.RoleType == "SUPERADMIN") {
                $scope.State = '';
                if ($scope.Name != "" && $scope.Uname != "") {
                    $http({
                        method: 'post',
                        url: 'create-user',
                        data: {"Name_": $scope.Name, "Username": $scope.Uname, "StateName": '', "DistrictName": '', "WarehouseName":'', "RoleType": $scope.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function (response)
                    {
                        var Response_ = response["data"];
                        var Status_ = Response_[0]["status"];
                        var Message_ = Response_[0]["message"];
                        if (Status_ == "1")
                        {
                            $scope.SuccessMessage = Message_;
                            $scope.ErrorMessage = "";
                            $scope.Name = "";
                            $scope.Uname = "";
                            $scope.State = "0";
                            $scope.District = "0";
                            $scope.myForm.$setPristine();
                            $scope.GetUserList();
                            $scope.backToButton();

                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }
                    }, function (response) {
                        $scope.ErrorMessage = "Unknown error occured. Please try again later";

                    });
                }
                else {
                    $scope.ErrorMessage = "Please fill all fields";
                }
            }
        } else
        {
            $scope.ErrorMessage = "Please fill all details.";
        }
    };
    
    
    
    // Func To edit User
    $scope.EditUserDetails = function ()
    {
        if ($scope.password != "" )
        {
           
                $http({
                    method: 'post',
                    url: 'edit-user',
                    data: { "Username": $scope.SelectedData["Username"], "pass":$scope.password
                    },
                    headers: {'Content-Type': 'application/json'}
                }).then(function (response)
                {
                    var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == "1")
                    {
                        $scope.SuccessMessage = Message_;
                        $scope.myForm.$setPristine();
                        $scope.GetUserList();
                        $scope.clearScreen();

                    } else
                    {
                        $scope.ErrorMessage = Message_;
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
          
        } else
        {
            if ($scope.password == '' || $scope.password == null) {
                $scope.ErrorMessage = "Enter Password";
            } 
           
        }
    };


    //Button Click
    $scope.GetUser=function(type, para){
        
        if($scope.Role == 'SUPERADMIN'){
            $scope.State = "0";
            $scope.District = "0";
            $scope.WareHouse="0";
        }
        if($scope.Role== "STATEADMIN"){
             $scope.District = "0";
             $scope.WareHouse="0";
        }
        $scope.Name = "";
        $scope.Uname = "";
        
        //$scope.State = "0";
       // $scope.District = "0";
        $scope.SuccessMessage = '';
        $scope.ErrorMessage = '';
        $scope.ShowForm = 1;
        $scope.IsDataShow = 0;
        
       
        if(type == 'COUNTER'){
            $scope.text="CREATE COUNTER USER";
            $scope.RoleType="COUNTER";
            $scope.label='District';
        }
        else if(type =='DISTRICTADMIN' ){
              $scope.text="CREATE DISTRICTADMIN";
              $scope.RoleType="DISTRICTADMIN";
              $scope.label='District';
        }
        else if(type == 'STATEADMIN'){
             $scope.text="CREATE STATEADMIN";
              $scope.RoleType="STATEADMIN";
               $scope.label='State';
        }
        else if(type == 'SUPERADMIN'){
              $scope.text="CREATE SUPERADMIN";
              $scope.RoleType="SUPERADMIN";
        }
         else if(type == 'WAREHOUSEADMIN'){
             $scope.text="CREATE WAREHOUSEADMIN";
             $scope.RoleType="WAREHOUSEADMIN";
             $scope.label='Warehouse';
            
        }
        
       // alert($scope.RoleType);
       
    };
    
    
    $scope.backToButton=function(){
        $scope.ShowForm = 0;
        $scope.IsDataShow =1;
        
    };
    
     $scope.EditAction = function (Id)
    {   
        $scope.SuccessMessage = '';
        $scope.ErrorMessage = '';
        $scope.password='';
        $scope.cnfpassword='';
        $scope.IsDataShow = 0;
        $scope.ShowForm = 0;
        $scope.EditForm = 1;
        $scope.SelectedData = $scope.UserList[$scope.UserList.findIndex(x => x.Id == Id)];
    };
    
    $scope.clearScreen=function(){
         $scope.IsDataShow = 1;
         $scope.EditForm = 0;
    };
    
    
    // for search 
    
    if ( $scope.Role == "DISTRICTADMIN")
    {
        $scope.Districtsearch = UserSesionService.DistrictName;
        //alert( $scope.Districtsearch);
        $scope.Statesearch = UserSesionService.StateName;
        $scope.sdisabled=true;
        $scope.ddisabled=true;
        $scope.GetUserList();
        $scope.StateDisable=true;
        $scope.DistrictDisable = true;
        $scope.GetDistrictList();
        //alert($scope.District);
    } else if ( $scope.Role == "STATEADMIN")
    {
        $scope.Statesearch = UserSesionService.StateName;
        //$scope.State = UserSesionService.StateName;
        $scope.District = "0";
        $scope.sdisabled=true;
        $scope.GetUserList();
        $scope.StateDisable=true;
        $scope.GetDistrictList();
        //alert($scope.Statesearch);
       // $scope.GetStateList();
    } else if ( $scope.Role == 'WAREHOUSEADMIN')
    {
        //alert('hii');
        $scope.Districtsearch = UserSesionService.DistrictName;
        $scope.Statesearch = UserSesionService.StateName;
        $scope.WareHousesearch = UserSesionService.WarehouseName;
        $scope.ddisabled=true;
        $scope.wdisabled=true;
        $scope.sdisabled=true;
        $scope.GetUserList();
        $scope.GetDistrictList();
    } 
    else if ( $scope.Role == 'SUPERADMIN') {
        $scope.Statesearch = "0";
        $scope.Districtsearch = "0";
        $scope.WareHousesearch = "0";
        $scope.GetUserList();
        $scope.state="0";
        $scope.District ="0";
        $scope.GetDistrictList();
        //alert('fgf');
    }
    
    // func for stateList filter
    $scope.fetchStateList = function () {
        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateListforsearch = Data_;
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";
        });
    };
    $scope.fetchStateList();
    
     // func for DistrictList filter
    $scope.fetchDistrictList = function () {
      
        //console.log($scope.State);
        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.DistrictListforsearch = Data_;
                
                if ($scope.Statesearch!="0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.Districtsearch="0";
                    $scope.WareHousesearch="0"
                    $scope.DistrictListforsearch = $scope.DistrictListforsearch.filter(x => x.StateName == $scope.Statesearch);
                    //console.log( $scope.DistrictList);
                }
                else if($scope.Statesearch!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN') || (UserSesionService.RoleType == 'COUNTER'))){
                     $scope.DistrictListforsearch = Data_;
                }
                else{
                    $scope.Districtsearch="0";
                    $scope.WareHousesearch="0";
                    //$scope.fetchwhList();
                    $scope.DistrictListforsearch = [];
                }
                $scope.fetchwhList();
                
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";
        });
        
        //console.log($scope.Statesearch);
    };
   $scope.fetchDistrictList();

   //function to fetch Warehouse
    $scope.fetchwhList = function () {
        
            //console.log('hii');
            $http.post('fetchWarehouseList',{"State": $scope.Statesearch, "District": $scope.Districtsearch}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //$scope.TmpLists = Data_;
                
                if(UserSesionService.RoleType == 'WAREHOUSEADMIN' || UserSesionService.RoleType == 'COUNTER'){
                  $scope.TmpLists = Data_;
                }
                else{
                   $scope.WareHousesearch="0";
                   $scope.TmpLists = Data_;
                }
                 
            } else
            {
                $scope.WareHousesearch="0";
                $scope.TmpLists=[];
                $scope.ErrorMessage = "";
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        
    };
    $scope.fetchwhList();
});


