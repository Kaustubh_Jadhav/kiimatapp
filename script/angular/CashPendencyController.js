app.controller('CashPendencyController', function ($scope, $http, UserSesionService, uiGridGroupingConstants, uiGridConstants, $timeout, $element) {
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.StateList = [];
    $scope.DistrictList = [];
    $scope.UserList = [];
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    $scope.WarehouseName = UserSesionService.WarehouseName;
    $scope.RoleType = UserSesionService.RoleType; 
    $scope.Status='0';
    $scope.Appliance = '0';
    $scope.noRecord = "";
    $scope.panel=true;
    if ($scope.RoleType == "SUPERADMIN")
    {
        $scope.State = "0";
        $scope.District = "0";
        $scope.Username="0";
    }
    else if($scope.RoleType == "COUNTER" || $scope.RoleType == "WAREEHOUSEADMIN"){
         $scope.Username=  UserSesionService.Username;
         $scope.panel=false;
    }
    else{
        $scope.Username="0";
    }
   
   

    $scope.UserList = [];
    // date for sale list filter
   
   // $scope.Username = UserSesionService.Username;
    $scope.gridOptions = {
    enableFiltering: false,
    treeRowHeaderAlwaysVisible: false,
    paginationPageSize: 1,
    enablePaginationControls: true,
    enableGridMenu: true,
    excessRows:1500,
    enableCellEdit: false,
    showGridFooter: true,
    groupingShowCounts: false,
    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
    enableVerticalScrollbar: 1,
    allowCellFocus: false,
    minimumColumnSize:110,
//    gridFooterTemplate: '<div style="background:#f3f3f3;padding:20px;" ></div>',
    columnDefs: [
      { displayName: 'State', field: 'StateName',  grouping: { groupPriority: 0 }, sort: {  direction: 'asc' }, cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { displayName: 'District', field: 'DistrictName', grouping: { groupPriority: 1 }, sort: {  direction: 'asc' }, cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>' },
      { displayName: 'Counter', field: 'Username', sort: { direction: 'asc' },  cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'  },
      { displayName: 'Cash In Hand',  field: 'Available',cellFilter : 'number', headerCellTemplate:'<div><div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="Test">Cash In Hand</span> (<i class=" rupe fas fa-rupee-sign " style="font-size:10px" ></i>)</div><div>',  cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }},
      { displayName:'UnApproved Cash',cellFilter : 'number', field: 'Pending',headerCellTemplate:'<div><div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="Test">UnApproved Cash</span> (<i class=" rupe fas fa-rupee-sign " style="font-size:10px" ></i>)</div><div>',  cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }},
      { displayName:'Total Available Cash', cellFilter : 'number', field: 'Total', headerCellTemplate:'<div><div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="Test">Total Available Cash</span>(<i class=" rupe fas fa-rupee-sign " style="font-size:10px" ></i>)</div><div>', cellTemplate: '<div class="rightAlign"><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>', treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
        aggregation.rendered = aggregation.value;
      }}
      ],
      gridMenuShowHideColumns:false,
      exporterMenuVisibleData:false,
      exporterMenuPdf: false,
      exporterMenuCsv: false,
      onRegisterApi: function (gridApi) {
      $scope.gridApi = gridApi;
       $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
       
       $timeout(function () {
            var $viewport = angular.element('.ui-grid-render-container');
            ['touchstart', 'touchmove', 'touchend','keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
                $viewport.unbind(eventName);
            });
       });
       
        if($scope.RoleType == "COUNTER" || $scope.RoleType == "WAREEHOUSEADMIN"){
            $scope.gridApi.grid.registerDataChangeCallback(function() {
               $scope.gridApi.treeBase.expandAllRows();
               $scope.changeGrouping();
             });
      }
    }  
  };
  
            $scope.changeGrouping = function() {
                $scope.gridApi.grouping.clearGrouping();
              };
  
  
  
         function number(input) {
            return function (input) {
                if (input) {
                    return moment(input).format('{0:0,00}');
                }
            };
        };
  
  
  
   
    $scope.FetchCash = function () {
        //$http.get('fetchStockTransfer').then(function (response) {
        $http.post('available-cash', {"State": $scope.State, "District": $scope.District, "Username":$scope.Username}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var data = Response_[0]["data"];
               
                if(data.length == 0){
                       $scope.noRecord ="No record found";
                       $scope.gridOptions.data=[];
                }
                else{
                     $scope.gridOptions.data = data;
                     
                      for ( var i = 0; i < data.length; i++ ){
                          data[i].Username =  data[i].Username.toUpperCase();
                      }
                      
                     $scope.noRecord="";
                }
                

            } else
            {
                //console.log(Status_);
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.FetchCash();
    
    
    
    $scope.filterText = '';
    $scope.filterValue = '';
//    $scope.filterBtn = function () {
//        $scope.filterText = 'MAHARASHTRA';
//        $scope.gridApi.grid.refresh();
//    }

    $scope.filter = function () {
        $scope.filterText = $scope.filterValue;
        $scope.gridApi.grid.refresh();
    };

    $scope.singleFilter = function (renderableRows) {
//        alert($scope.filterText);
        var matcher = new RegExp($scope.filterText.toLowerCase());
        
        renderableRows.forEach(function (row) {
            var match = false;
            ['StateName', 'DistrictName','Username'].forEach(function (field) {
                if (row.entity[field].toLowerCase().match(matcher)) {
                    match = true;
//                    alert(true);
                }
            });
            if (!match) {
                    row.visible = false;
//                  alert(false);
//                    $scope.gridOptions.data.length=0;
//                    $scope.noRecord ="No record found";
            }
        });
        return renderableRows;
    };
    
    
});