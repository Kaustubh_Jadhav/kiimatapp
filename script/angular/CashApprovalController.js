app.controller('CashApprovalController', function ($scope, $http, UserSesionService) {
    $scope.State = "0";
    $scope.District = "0";
    $scope.Users = "0";
    $scope.StockApprovalList = "";
    $scope.IsDataForApproval = 0;
    $scope.IsDataShow = 0;
    $scope.SelectedData = [];
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.txt_remarks = {text: ""};
    $scope.column = 'TrnDate';
    $scope.reverse = false;
    $scope.IsFocus = false;
    $scope.show = false;

   // if (UserSesionService.RoleType == 'COUNTER') {
        $scope.Username = UserSesionService.Username;
    //} else if (UserSesionService.RoleType == 'WAREHOUSE') {
     //   $scope.Username = UserSesionService.RefName;
    //} else {
     //   $scope.Username = UserSesionService.Username;
    //}

    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };

    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };

    $scope.GetApprovalList = function () {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $http.post('fetch-cashapprovalList', {"Username": $scope.Username, "Status": "Pending", "RoleType":UserSesionService.RoleType}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StockApprovalList = Data_;
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetApprovalList();

    $scope.OpneAction = function (Type, Id)
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataForApproval = Type;
        $scope.SelectedData = $scope.StockApprovalList[$scope.StockApprovalList.findIndex(x => x.Id == Id)];
        $scope.IsDataShow = 1;
        $("body").scrollTop(0);
    };

    $scope.SaveComments = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        if ($scope.txt_remarks.text != "" && $scope.txt_remarks.text != null)
        {
            var Status = "APPROVED";
            if ($scope.IsDataForApproval != 1)
            {
                Status = "REJECTED";
            }
            var Username_To = "";
            if (UserSesionService.RoleType == 'COUNTER') {
                Username_To = $scope.SelectedData["Username_To"];
            } else if (UserSesionService.RoleType == 'WAREHOUSE') {
                Username_To = UserSesionService.RefName;
            } else {
                Username_To = $scope.SelectedData["Username_To"];
            }

            $http({
                method: 'post',
                url: 'save-cashTransferApproval',
                data: {"Id": $scope.SelectedData["Id"], "Status": Status, "ApproverRemarks": $scope.txt_remarks.text,
                    "TrnAmount": $scope.SelectedData["TrnAmount"], "Username_To": Username_To, 
                    "Username_From": $scope.SelectedData["Username_From"]
                },
                headers: {'Content-Type': 'application/json'}
            }).then(function (response)
            {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == "1")
                {
                    
                    
                    $scope.txt_remarks = {text: ""};
                    $scope.IsDataShow = 0;
                    $scope.SuccessMessage = Message_;
                    $scope.GetApprovalList();

                } else
                {
                    $scope.ErrorMessage = Message_;
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        } else
        {
            $scope.ErrorMessage = "Please provide remarks to continue";
        }
    };

    $scope.CancelPanel = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
        $scope.IsDataShow = 0;
        $("body").scrollTop(0);
    };

    $scope.ClearMsg = function ()
    {
        $scope.ErrorMessage = "";
        $scope.SuccessMessage = "";
    };
    $scope.onKeydown = function ($event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }

    };

});