app.controller('ChangePassword', function ($scope, $http, UserSesionService, fileUpload) {
 
  
  $scope.Username= UserSesionService.Username;
  $scope.password='';
  $scope.cnfpassword='';
  $scope.SuccessMessage="";
  $scope.ErrorMessage="";
    $scope.EditUserDetails = function ()
    {
        if ($scope.password != "" && $scope.cnfpassword!="" )
        {
            if($scope.password ==$scope.cnfpassword){
                 $http({
                    method: 'post',
                    url: 'edit-user',
                    data: { "Username":UserSesionService.Username, "pass":$scope.password
                    },
                    headers: {'Content-Type': 'application/json'}
                }).then(function (response)
                {
                    var Response_ = response["data"];
                    var Status_ = Response_[0]["status"];
                    var Message_ = Response_[0]["message"];
                    if (Status_ == "1")
                    {
                        $scope.SuccessMessage = Message_;
                        $scope.password='';
                        $scope.cnfpassword='';
                        $scope.ErrorMessage="";
                        $scope.myForm.$setPristine();

                    } else
                    {
                        $scope.ErrorMessage = Message_;
                        $scope.SuccessMessage="";
                    }
                }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";
                     $scope.SuccessMessage="";
                });
            }
            else{
                $scope.ErrorMessage = "New Password and confirm Password are not match.";
                $scope.SuccessMessage="";
            }
               
          
        } else
        {
            if ($scope.password == '' || $scope.password == null) {
                $scope.ErrorMessage = "Enter Password";
            }
            else if($scope.cnfpassword == '' || $scope.cnfpassword == null) {
                $scope.ErrorMessage = "Enter Confirm Password";
            }  
             $scope.SuccessMessage="";
           
        }
    };


    
//    $scope.uploadFile = function(){
//        
//        if($scope.myFile != undefined){
//            var file = $scope.myFile;
//             var uploadUrl = "file-upload";
//           //fileUpload.uploadFileToUrl(file, uploadUrl);
//            fileUpload.uploadFileToUrl(file, uploadUrl).then(function(response) {
//              // console.log(response); // data comes under data property in then 
//                var Response_ = response["data"];
//                var Status_ = Response_["status"];
//                var Message_ = Response_["data"];
//                //console.log(Status_);
//            });
//        }
//       
//   };
   
});


