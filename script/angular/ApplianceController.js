app.controller('ApplianceController', function ($scope, $http, UserSesionService) {
   
    $scope.ShowForm = 0;
    $scope.SelectedData = "";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.txt_remarks = {text: ""};
    $scope.column = 'TrnDate';
    $scope.EditForm = 0;
    $scope.name = '';
    $scope.rate = '';
    $scope.buttonShow=true; 
    $scope.noRecord ="";
    $scope.Id='';
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };


    $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };
    
     
    $scope.OpneAction = function ()
    {
        $scope.ShowForm =1;
        $scope.EditForm =0;
        $scope.SuccessMessage = "";
        $scope.ErrorMessage = "";
        $scope.name = "";
        $scope.rate = "";
         $scope.model = {
            isDisabled: false
        };
         $scope.text="Add";
         $scope.SelectedData="";
         $scope.myForm.$setPristine();
    };


   // $scope.GetDistrictList();
    $scope.getApplianceList = function () {
            //console.log('hii');
            $http.get('fetchApplianceList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                $scope.TmpList = Data_;
                if($scope.TmpList.length >= 10){
                    $scope.buttonShow = false;
                     $scope.noRecord="";
                }
                else{
                     if($scope.TmpList.length ==0){
                             $scope.noRecord="Appliance not available.";
                        }
                         $scope.noRecord="";
                }
                
            } else
            {
                $scope.noRecord="Appliance not available.";
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
        
    };
    $scope.getApplianceList();
    
     //alert($scope.Id);
    
    $scope.SaveApplianceDetails = function (isValid) {
        
        
        if ($scope.name != "" && $scope.rate != "") {
           if ($scope.SelectedData == '') {
               $scope.Id ='';
           }
           else{
               $scope.Id = $scope.SelectedData["Id"];
           }
           //alert($scope.Id);
           //console.log($scope.Id);
            $http({
                method: 'post',
                url: 'save-appliance',
                data: {"Id": $scope.Id, "Name_": $scope.name.toUpperCase(), "RatePerPc": $scope.rate},
                headers: {'Content-Type': 'application/json'}
            }).then(function (response)
            {
                var Response_ = response["data"];
                var Status_ = Response_[0]["status"];
                var Message_ = Response_[0]["message"];
                if (Status_ == "1")
                {
                    $scope.SuccessMessage = Message_;
                    $scope.ErrorMessage = "";
                    $scope.name = "";
                    $scope.rate = "";
                    $scope.myForm.$setPristine();
                    $scope.getApplianceList();
                    $scope.backToButton();

                } else
                {
                    $scope.ErrorMessage = Message_;
                }
            }, function (response) {
                $scope.ErrorMessage = "Unknown error occured. Please try again later";

            });
        } else {
                $scope.ErrorMessage = "Please fill all details. ";
        }
    };
    

    $scope.backToButton=function(){
        $scope.ShowForm = 0;
    };
    
     $scope.EditAction = function (Id)
    {   
        $scope.SuccessMessage = '';
        $scope.ErrorMessage = '';
        $scope.password='';
        $scope.cnfpassword='';
       // $scope.EditForm =1;
        $scope.ShowForm = 1;
        $scope.SelectedData = $scope.TmpList[$scope.TmpList.findIndex(x => x.Id == Id)];
        //console.log($scope.SelectedData.length);
        $scope.name = $scope.SelectedData['Name_'];
        $scope.rate = $scope.SelectedData['RatePerPc'] |0;
        $scope.model = {
            isDisabled: true
        };
         $scope.model = {
            isDisabled: true
        };
        $scope.text="Edit";
         $scope.myForm.$setPristine();
    };
    
    $scope.clearScreen=function(){
        $scope.EditForm =0;
    };

});


