app.controller('CashController', function ($scope, $http , UserSesionService) {
    $scope.dd_expnsType = "0";
    $scope.ErrorMessage = "";
    $scope.SuccessMessage = "";
    $scope.IsExpenseType = "0";
    $scope.Approvecash = "0";
    $scope.TotalAmount = "0";
    $scope.dd_userType = "";
    $scope.Username = UserSesionService.Username;
    $scope.noRecord="";
    $scope.txt_trnDate = new Date();
    $scope.PrevNoofMonths =2;
    $scope.PrevNoofDays = 3;
    //$scope.add = 2;
    $scope.Status="0";
    $scope.RecipientType = "0";
    $scope.CashTransferList=[];
    $scope.Prevday=6;
    $scope.minDate = new Date(
            $scope.txt_trnDate.getFullYear(),
            //$scope.txt_trnDate.getMonth() - $scope.PrevNoofMonths,
            $scope.txt_trnDate.getMonth(),
            $scope.txt_trnDate.getDate()- $scope.Prevday
            );
    $scope.maxDate = new Date(
            $scope.txt_trnDate.getFullYear(),
            $scope.txt_trnDate.getMonth(),
            $scope.txt_trnDate.getDate()
            );
    this.isOpen = false;
    $scope.UserList = [];
    $scope.State = UserSesionService.StateName;
    $scope.District = UserSesionService.DistrictName;
    $scope.panel= true;
    $scope.Trn_To="0";
    $scope.Username = UserSesionService.Username;
    $scope.IsEnable = false;
    // uname use for filter and Username for cashtransfer
    if (UserSesionService.RoleType == 'COUNTER' || UserSesionService.RoleType == 'WAREHOUSEADMIN') 
    {
       // $scope.Username = UserSesionService.Username;
        $scope.Uname =UserSesionService.Username.toUpperCase();
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
        $scope.IsUserEnable =true;
       // $scope.panel= false;
    } 
   else  if (UserSesionService.RoleType == "DISTRICTADMIN")
    {
       // $scope.Username = UserSesionService.Username;
        $scope.Uname ="0";
        $scope.IsStateEnable =true;
        $scope.IsDistrictEnable =true;
    } 
    else if(UserSesionService.RoleType == "STATEADMIN")
    {
        //$scope.Username = UserSesionService.Username;
        $scope.Uname ="0";
        $scope.IsStateEnable =true;
    }
    else if(UserSesionService.RoleType == "SUPERADMIN")
    {
        //$scope.Username = UserSesionService.Username;
        $scope.Uname ="0";
        $scope.State = "0";
        $scope.District = "0";
        $scope.IsEnable = true;
    }
    $scope.show = 'false';
//Function use to fetch Available cash
    $scope.GetCashList = function () {
              $http({
                    method: 'POST',
                    url: 'fetchCounterCash',
                    data: {"Username": $scope.Username},
                    headers: {'Content-Type': 'application/json'}
                }).then(function(response){
                    //console.log(response);
                    var Response_ = response["data"];
                    // console.log(response);
                    var Status_ = Response_[0]["status"];
                    //console.log(Status_);
                    var Message_ = Response_[0]["message"];
                    if (Status_ == 1)
                    {  
                        $scope.getstatus=Status_;
                        var Data_ = Response_[0]["data"];
                        //console.log(Data_);
                                if (Data_ == "") {
                            $scope.Available = "";
                            $scope.RatePerPc = "";
                            $scope.ErrorMessage = "Cash is not available";
                        } else {
                            //$scope.CashInHand = Data_[0]['CashInHand'];
                            $scope.Available = Data_[0]['CashAvailable'];
                        }
                        
                        //console.log($scope.Available);

                    } else
                    {   
                        $scope.Available = 0;
                        $scope.ErrorMessage = 'Cash is not available';
                    }
                    
                }, function (response) {
                  $scope.ErrorMessage = "Unknown error occured. Please try again later";
                });
    };
    $scope.GetCashList();
    
    
    // date for filter logic 
    // date for filter logic 
     $scope.to_Date =''; //tdays date
     $scope.from_Date = '';
     $scope.send="";
     
    $scope.getDate = function(){
         if($scope.from_Date !=''){
            //$scope.from_Date = new Date();
            $scope.from_Date = new Date(
                    $scope.from_Date.getFullYear(),
                    $scope.from_Date.getMonth(),
                    $scope.from_Date.getDate(),
                    0, 0, 0, 0
                    );
            $scope.from_Date = new Date($scope.from_Date.getTime() - ($scope.from_Date.getTimezoneOffset()) * 60000);
            
        }
        if($scope.to_Date !=''){
            $scope.send = new Date(
                    $scope.to_Date.getFullYear(),
                    $scope.to_Date.getMonth(),
                    $scope.to_Date.getDate(),
                    23, 59, 59, 0
                    );
            //convert to date with specific time
            $scope.send = new Date($scope.send.getTime() - ($scope.send.getTimezoneOffset()) * 60000);
        }
       
     };
     
    
    // function to display cash transfer list 
    $scope.GetCashTransferList = function () {
       
        if($scope.from_Date !='' && ($scope.send!='' && $scope.send < $scope.from_Date)){
            $scope.noRecord="Please select valid range.";
            $scope.CashTransferList=[];
        }
        else{
        $http({
            method: 'POST',
            url: 'fetchCashTransfer',
            data: {"Username": $scope.Uname, "State": $scope.State, "District": $scope.District,'RecipientType':$scope.RecipientType, 'Status':$scope.Status, "TransferTo": "0", "From": $scope.from_Date, "To":$scope.send},
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            // console.log(response);
            var Response_ = response["data"];
            //console.log(response);
            var Status_ = Response_[0]["status"];
            // console.log(Status_);
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
                //console.log(Data_);
                //$scope.CashTransferList = Data_;
                
                if(Data_.length ==0){
                    $scope.noRecord="No details found for cash transfer.";
                    $scope.CashTransferList=[];
                }
                else{
                    $scope.CashTransferList = Data_;
                    $scope.noRecord="";
                }
                
               // console.log($scope.CashTransferList);

            } else
            {
                $scope.ErrorMessage = Message_;
            }

        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    }
    };
    $scope.GetCashTransferList();


//function to display Available user for acc type iternals

//$scope.displayusers = function () {
//          if($scope.dd_expnsType !="0"){
//              $scope.SuccessMessage ="";
//              $scope.ErrorMessage = "";
//              $scope.show = 'true';
//              if($scope.dd_expnsType=="INTERNAL_USERS"){
//                  $http({
//                        method: 'POST',
//                        url: 'internal-userlist',
//                        data: {StateName: "0", RoleType: "0", DistrictName: "0", WarehouseName: "0", Para:0},
//                        headers: {'Content-Type': 'application/json'}
//                    }).then(function(response){
//                       // console.log(response);
//                        var Response_ = response["data"];
//                        // console.log(response);
//                        var Status_ = Response_[0]["status"];
//                        //console.log(Status_);
//                        var Message_ = Response_[0]["message"];
//                        if (Status_ == 1)
//                        {  
//                            $scope.getstatus=Status_;
//                            var Data_ = Response_[0]["data"];
//                            // $scope.UserList = Data_;
//                             $scope.TempList = Data_;
////                             $scope.TempList = $scope.TempList.filter(x => x.RoleType != 'COUNTER');
//                             $scope.TempList = $scope.TempList.filter(x => x.RoleType != 'COUNTER');
//
//                        } else
//                        {
//                            $scope.ErrorMessage = Message_;
//                        }
//
//                    }, function (response) {
//                    $scope.ErrorMessage = "Unknown error occured. Please try again later";
//
//                });
//              }
//              else{
//                 // $scope.UserList=[];
//              }
//      }
//          
//   };

//function to display Available user for acc type iternals
$scope.displayusers = function () {
          if($scope.dd_expnsType !="0"){
              $scope.SuccessMessage ="";
              $scope.ErrorMessage = "";
              $scope.show = 'true';
              if($scope.dd_expnsType=="INTERNAL_USERS"){
                  $http({
                        method: 'POST',
                        url: 'internal-userlist',
                        data: {Username:UserSesionService.Username, StateName: $scope.State, DistrictName:$scope.District, RoleType: UserSesionService.RoleType},
                        headers: {'Content-Type': 'application/json'}
                    }).then(function(response){
                       // console.log(response);
                        var Response_ = response["data"];
                        // console.log(response);
                        var Status_ = Response_[0]["status"];
                        //console.log(Status_);
                        var Message_ = Response_[0]["message"];
                        if (Status_ == 1)
                        {  
                            $scope.getstatus=Status_;
                            var Data_ = Response_[0]["data"];
                            if(UserSesionService.RoleType == "SUPERADMIN"){
                                $scope.TempList=[];
                            }
                            else{
                                  $scope.TempList = Data_.filter(x => x.RoleType != 'COUNTER');
                            }
                        } else
                        {
                            $scope.ErrorMessage = Message_;
                        }

                    }, function (response) {
                    $scope.ErrorMessage = "Unknown error occured. Please try again later";

                });
              }
              else{
                 // $scope.UserList=[];
              }
      }
          
   };
   
   // func to save cash transfer
    $scope.SaveCashTransfer = function (isValid) {
     
        if (isValid) {
             $scope.txt_trnDate = new Date( $scope.txt_trnDate.getTime() -($scope.txt_trnDate.getTimezoneOffset())*60000);
            if ($scope.dd_expnsType != "0")
            {
                if ($scope.txt_amountSpent > 0 && $scope.Available >= $scope.txt_amountSpent) {

                    if ($scope.dd_expnsType == 'INTERNAL_USERS' && $scope.dd_userType==null) {
                        $scope.ErrorMessage = "Please Select User";
                        $scope.myForm.$setPristine();
                    } else {
                        $http({
                            method: 'post',
                            url: 'saveCashTransfer',
                            data: {"Username": $scope.Username, "TrnDate": $scope.txt_trnDate, "RecipientAccountType": $scope.dd_expnsType, "Username_To": $scope.dd_userType, "TrnAmount": $scope.txt_amountSpent, "Comments": $scope.txt_comments},
                            headers: {'Content-Type': 'application/json'}
                        }).then(function (response)
                        {
                            var Response_ = response["data"];
                            //console.log(response);
                            var Status_ = Response_[0]["status"];
                            // console.log(Status_);
                            var Message_ = Response_[0]["message"];
                            if (Status_ == "1")
                            {
                                
                                $scope.SuccessMessage = Message_;
                                $scope.dd_expnsType = "0";
                                $scope.txt_amountSpent = "";
                                $scope.txt_comments = "";
                                $scope.ErrorMessage = "";
                                $scope.dd_userType  = "";
                                $scope.txt_trnDate= new Date();
                                $scope.myForm.$setPristine();
                                $scope.show = 'false';
                                $scope.GetCashTransferList();
                                $scope.GetCashList();
                            } else
                            {
                                $scope.ErrorMessage = Message_;
                            }
                        }, function (response) {
                            $scope.ErrorMessage = "Unknown error occured. Please try again later";

                        });
                    }
                } else {
                    $scope.ErrorMessage = "Amount send cannot be zero or greater than Available amount";
                }

            } else
            {
                $scope.IsExpenseType = "1";
            }
        } else
        {
            $scope.ErrorMessage = "Please fill all details.";
        }
    };
    
    // Pagination
    
    //$scope.column = 'Transaction Date';
    $scope.reverse = false;
    
    $scope.sortColumn = function (col) {
        $scope.column = col;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    };
    
     $scope.indexCount = function (newPageNumber) {
        $scope.serial = newPageNumber * 10 - 10;
    };
    
    
      
      
      
    $scope.backToCash = function () {
        $scope.SuccessMessage = "";
        $scope.dd_expnsType = "0";
        $scope.txt_amountSpent = "";
        $scope.txt_comments = "";
        $scope.ErrorMessage = "";
        $scope.dd_userType  = "";
        $scope.myForm.$setPristine();
        $scope.show = 'false';
        $scope.txt_trnDate= new Date();
    };
    
    
     // for search
     $scope.GetStateList = function () {

        $http.post('fetchStateList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                $scope.StateList = Data_;

            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetStateList();

    $scope.GetDistrictList = function () {

        $http.post('fetchDistrictList').then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
//                $scope.DistrictList = Data_;
//                if($scope.State!="0")
//                {
//                     $scope.DistrictList = $scope.DistrictList.filter(x => x.StateName == $scope.State);
//                }
                
                if ($scope.State!="0" && ((UserSesionService.RoleType == 'STATEADMIN') || (UserSesionService.RoleType == 'SUPERADMIN')))
                {
                    $scope.District="0";
                    $scope.Uname="0";
                    $scope.DistrictList = Data_.filter(x => x.StateName == $scope.State);
                   
                    //console.log( $scope.DistrictList);
                }
                else if($scope.State!="0" && ((UserSesionService.RoleType == 'DISTRICTADMIN') || (UserSesionService.RoleType == 'WAREHOUSEADMIN') || (UserSesionService.RoleType == 'COUNTER'))){
                     $scope.DistrictList = Data_;
                    
                }
                else{
                    $scope.District="0";
                    $scope.DistrictList=[];
                    $scope.Uname="0";
                   
                }
                $scope.GetUserList();
            } else
            {
                $scope.ErrorMessage = Message_;
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.GetDistrictList();


    $scope.GetUserList = function () {
        $http.post('fetch-users', {StateName: $scope.State, RoleType: "0", DistrictName: $scope.District, WarehouseName: "0" , Para:0}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == "1")
            {
                var Data_ = Response_[0]["data"];
                //$scope.UserList = Data_;
                
                if(UserSesionService.RoleType == 'WAREHOUSEADMIN' || UserSesionService.RoleType == 'COUNTER'){
                    $scope.UserList = Data_;
                }
                else{
                     $scope.Uname="0";
                     $scope.UserList = Data_;
                }

            } else
            { 
                $scope.Uname="0";
                $scope.UserList = [];
                $scope.ErrorMessage ='';
            }
        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
   $scope.GetUserList();
    
    
    $scope.transferTo = function () {
            $http.post('internal-userlist', {Username:UserSesionService.Username, StateName: $scope.State, DistrictName: $scope.District, RoleType: UserSesionService.RoleType}).then(function (response) {
            var Response_ = response["data"];
            var Status_ = Response_[0]["status"];
            var Message_ = Response_[0]["message"];
            if (Status_ == 1)
            {
                var Data_ = Response_[0]["data"];
//                if (UserSesionService.RoleType == "SUPERADMIN") {
//                    $scope.ToList = [];
//                }
//                else {
                    //$scope.ToList = Data_.filter(x => x.RoleType != 'COUNTER');
//                }

                if (UserSesionService.RoleType == "SUPERADMIN") {
                    $scope.ToList = Data_ ;
                }
                else {
                    $scope.ToList = Data_.filter(x => x.RoleType != 'COUNTER');
                }
               
            } else
            {
                $scope.ErrorMessage = Message_;
            }

        }, function (response) {
            $scope.ErrorMessage = "Unknown error occured. Please try again later";

        });
    };
    $scope.transferTo();

});


